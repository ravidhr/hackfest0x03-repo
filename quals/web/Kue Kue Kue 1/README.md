# Ekstrak Kulit Manggis

# Deskripsi

    memanipulasi beberapa nilai pada cookie

# Solusi

challenge ini peserta mengharuskan memanipulasi beberapa nilai pada cookie yang sudah diset default oleh server, ada 2 bagian nilai cookie yaitu user, role yang dimana peserta hanya mengubah nilai role menjadi admin dan melakukan encode ke base64 dan mengubah nilainya di localstorage browser

    {"user":"bebas_apa_aja","role":"admin"}

flag : hackfest{privilege_escalation_with_cookie_poisoning}