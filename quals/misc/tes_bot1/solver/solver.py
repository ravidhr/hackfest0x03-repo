from pwn import *
import gmpy2

host = 'ccug.my.id'
port = 39005

r = remote(host, port)

for i in range(100):
	a = r.recv(2048)
	b = a.decode().split('\n')
	#print(b)
	angka1 = int(b[0].split(' ')[2])
	angka2 = int(b[1].split(' ')[2])
	print(angka1, angka2)

	fpb = gmpy2.gcd(angka1, angka2)
	r.sendline(str(fpb))
	print(r.recvline())
	
print(r.recvline())
