#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//gcc chall.c -o chall -m64 -fstack-protector-all -pie -g -O0 -Wl,-z,relro,-z,now
char list_name[0x200][40] = {};

void init(float *berat_badan) {
	setvbuf(stdout, 0 , 2 , 0);
	setvbuf(stdin, 0 , 2 , 0);
	for(int i = 0;i<0x100;i++) {
		berat_badan[i] = 0;
	}
}

void menu() {
	puts("\n-------------------------------------------");
	puts("\t GYM 1337 DATA BERAT BADAN\t");
	puts("-------------------------------------------");
  puts("\t[1] Tambah Data Berat Badan");
  puts("\t[2] Tampilkan Data Berat Badan");
	puts("\t[3] Reset Database");
  puts("\t[4] Keluar");
	puts("-------------------------------------------");
  printf("[?] Pilihan : ");
}

int search_nama(char nama_lengkap[40]) {
	int result;
	for(int i = 0;i < 0x200;i++) {
		if(strcmp(nama_lengkap,list_name[i]) == 0) {
			result = i;
			return result;
		}
	}
	return -1;
}

void tambah_data(float *berat_badan, int pos) {
	char nama_lengkap[40];
	int check;
		printf("\n[?] Nama Lengkap : ");
		fgets(nama_lengkap,32,stdin);
		strtok(nama_lengkap,"\n");
		check = search_nama(nama_lengkap);
		if(check == -1) {
			strncpy(list_name[pos],nama_lengkap,32);
			printf("[?] Berat Badan (kg) : ");
			scanf("%f",&berat_badan[pos]);
			getchar();
		} else {
			puts("[!] Nama sudah terdaftar.");
		}
}

void lihat_data(float *berat_badan) {
	char nama_lengkap[40];
	int pos;
	printf("\n[?] Nama Lengkap : ");
	fgets(nama_lengkap,32,stdin);
	strtok(nama_lengkap,"\n");
	pos = search_nama(nama_lengkap);
	if(pos != -1) {
		printf("[+] Berat Badan (kg) : %f\n",berat_badan[pos]);
	} else {
		puts("[!] Nama tidak ditemukan.");
	}
}

void reset_Data(float *berat_badan) {
	for(int i = 0;i < 0x200; i++) {
		if(i < 0x100) {
			berat_badan[i] = 0;
		}
		memset(list_name[i],0,40);
	}

}

int main(void) {
	int choice;
	int idx = 0;
	int finish = 0;
	char choicebuf[8];
	float berat_badan[0x100];
	init(berat_badan);
	while(finish == 0) {
		menu();
		fgets(choicebuf,4,stdin);
		strtok(choicebuf,"\n");
		choice = atoi(choicebuf);
		switch(choice) {
			case 1:
			tambah_data(berat_badan,idx);
			idx++;
			break;
			case 2:
			lihat_data(berat_badan);
			break;
			case 3:
			reset_Data(berat_badan);
			idx = 0;
			puts("[+] Data berhasil direset");
			break;
			case 4:
			finish = 1;
			break;
		}
	}
}
