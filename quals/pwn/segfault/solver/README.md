# Segfault

    can you sigsev me?

## Solusi

sudah jelas dari deskripsi, challenge ini memiliki kerentanan buffer overflow, untuk mendapatkan flag
peserta hanya perlu melakukan input yang banyak hingga program crash

    $ python -c "print 'a' * 800" | ./segfault

FLAG: hackfest{ini_namanya_segmentation_fault_Dalam komputasi, kesalahan segmentasi atau pelanggaran akses adalah kesalahan, atau kondisi kegagalan, yang ditimbulkan oleh perangkat keras dengan perlindungan memori, memberitahukan sistem operasi perangkat lunak telah berusaha untuk mengakses area memori yang terbatas. --wikipedia}