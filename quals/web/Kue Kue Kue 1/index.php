<?php

    if(!isset($_COOKIE['auth'])) {
        setcookie('auth', base64_encode(json_encode(['user' => 'guest', 'role' => 'guest'])));
    }

?>
<html>
<body>

    <h2>Flag Controller 1!</h2>
    <p><a href="index.php">Home</a> | <a href="flag.php">Flag</a></p>
    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Impedit blanditiis et iste velit, maxime eos!</p>
    
</body>
</html>