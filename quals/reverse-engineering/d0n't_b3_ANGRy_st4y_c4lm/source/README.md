# d0n't b3 ANGRy st4y c4lm

## Solution

Pada soal berikut ini kita diberikan sebuah file 'rev', pada program tersebut kita diminta untuk memasukan password yang tepat. 
```
[0x00401090]> afl
0x00401090    1 46           entry0
0x004010d0    4 33   -> 31   sym.deregister_tm_clones
0x00401100    4 49           sym.register_tm_clones
0x00401140    3 33   -> 32   sym.__do_global_dtors_aux
0x00401170    1 6            entry.init0
0x00404e00    1 5            sym.__libc_csu_fini
0x00402925    5 67           sym.func_87
0x0040289b    5 69           sym.func_85
0x004029ab    5 67           sym.func_89
0x0040278d    5 67           sym.func_81
0x00402813    5 67           sym.func_83
0x00401bef    5 67           sym.func_37
0x00403195    5 67           sym.func_119
0x00403bf8    5 71           sym.func_154
...
...
```

Seperti yang kita lihat terdapat banya sekali fungsi pada program tersebut, berikut ini adalah pseudocode main dari program tersebut.
```
[0x00401090]> pdg @main

undefined8 main(void)
{
    int32_t iVar1;
    int64_t in_RCX;
    undefined8 placeholder_2;
    undefined8 placeholder_2_00;
    undefined8 placeholder_2_01;
    undefined8 placeholder_2_02;
    undefined8 placeholder_1;
    char *s;
    
    sym.imp.printf("[?] Password: ");
    placeholder_1 = 0xff;
    sym.imp.fgets(&s, 0xff, _reloc.stdin);
    iVar1 = sym.func_0((char *)&s);
    if (((((((((((iVar1 != 0) && (iVar1 = sym.func_1((int64_t)&s), iVar1 != 0)) &&
               (iVar1 = sym.func_2((char *)&s), iVar1 != 0)) &&
              ((iVar1 = sym.func_3((int64_t)&s), iVar1 != 0 && (iVar1 = sym.func_4((char *)&s), iVar1 != 0)))) &&
             (iVar1 = sym.func_5((int64_t)&s), iVar1 != 0)) &&
            ((iVar1 = sym.func_6((char *)&s), iVar1 != 0 && (iVar1 = sym.func_7((int64_t)&s), iVar1 != 0)))) &&
           ((iVar1 = sym.func_8((char *)&s), iVar1 != 0 &&
           			...
           			...
           			...
                      (((iVar1 = sym.func_156((int64_t)&s), iVar1 != 0 &&
                        (iVar1 = sym.func_157((int64_t)&s), iVar1 != 0)) &&
                       (((iVar1 = sym.func_158((int64_t)&s), iVar1 != 0 &&
                         ((iVar1 = sym.func_159((int64_t)&s), iVar1 != 0 &&
                          (iVar1 = sym.func_160((int64_t)&s), iVar1 != 0)))) &&
                        (iVar1 = sym.func_161((int64_t)&s), iVar1 != 0)))))) &&
                     (iVar1 = sym.func_162((int64_t)&s), iVar1 != 0)))))))))))) {
        sym.imp.puts("Correct!...");
        return 0;
    }
    sym.imp.puts("Incorrect!...");
    return 0;
}
```

Pada main program terdapat 'if', yang mana if tersebut melakukan pengecekan terhadap inputan kita. Dan terdapat sebanyak 163 fungsi.
berikut ini adalah bebrapa pseudocode dari func_0 & func_1.

```
[0x00401090]> pdg@sym.func_0

undefined8 sym.func_0(char *arg1)
{
    undefined8 uVar1;
    char *var_8h;
    
    if ((*arg1 < arg1[0x7b]) && (*arg1 == 'E')) {
        uVar1 = 1;
    } else {
        uVar1 = 0;
    }
    return uVar1;
}
[0x00401090]> pdg@sym.func_1

undefined8 sym.func_1(int64_t arg1)
{
    undefined8 uVar1;
    int64_t var_8h;
    
    if ((*(char *)(arg1 + 1) == 'n') && (*(char *)(arg1 + 0x5c) < *(char *)(arg1 + 1))) {
        uVar1 = 1;
    } else {
        uVar1 = 0;
    }
    return uVar1;
}
```

Setelah beberapa menit melakukan static analysis, saya berkesimpulan bahwa untuk menyelesaikan tantangan ini secara manual akan memakan waktu yang sangat banyak.

Jika kalian perhatikan pada judul soal terdapat sebuah clue [*"ANGR"*](https://angr.io/). jika kamu tidak tahu apa itu 'Angr', angr adalah toolkit analisis biner multi-arsitektur, dengan kemampuan untuk melakukan eksekusi simbolik dinamis. [*docs*](http://docs.angr.io/).
Kita dapat menggunakan angr untuk memudahkan kita menyelesaikan tantangan tersebut.

Berikut ini adalah solvernya
```
#!/usr/bin/env python3
import angr

proj = angr.Project('./rev')

try:
    state = proj.factory.entry_state()
    simgr = proj.factory.simulation_manager(state)
    simgr.explore(find=lambda s: b"Correct!..." in s.posix.dumps(1))
    s = simgr.found[0]
    print(s.posix.dumps(1))
    result = s.posix.dumps(0)
    print(result)
except IndexError:
    print("Unsat")
```

PoC
```
❯ time python3 solver.py
b'[?] Password: Correct!...\n'
b"Entah bagaimana anda menyelesaikannya tapi ini adalah FL4G: 'TIFEST{4ngr_s1mul4710n_M4n4g3rs_s4ng4t_b3rgun4}'. buat kamu yang sudah menyelesaikan tantangan ini :).\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
python3 solver.py  401,09s user 3,85s system 99% cpu 6:46,18 total
```