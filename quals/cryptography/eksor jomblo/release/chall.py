from secret import flag, key

cipher = ''

for i in flag:
	cipher += chr(ord(i) ^ key)

r = open('cipher.txt', 'w')

r.write(cipher)
r.close()

