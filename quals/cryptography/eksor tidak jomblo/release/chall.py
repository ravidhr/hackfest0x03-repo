from secret import flag
key = 'TheQuickBrownFox'

cipher = ''
for i in range(len(flag)):
	cipher += chr(ord(flag[i]) ^ ord(key[i % len(key)]))

a = open('cipher.txt', 'w')

a.write(cipher)

a.close()