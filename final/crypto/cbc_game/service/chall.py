from __future__ import print_function
from Crypto.Cipher import AES
from Crypto import Random
from secret import *
import sys
import base64
import string
KEY = Random.new().read(16)

class Unbuffered(object):
    def __init__(self, stream):
        self.stream = stream
    def write(self, data):
        self.stream.write(data)
        self.stream.flush()
    def writelines(self, datas):
        self.stream.writelines(datas)
        self.stream.flush()
    def __getattr__(self, attr):
        return getattr(self.stream, attr)

sys.stdout = Unbuffered(sys.stdout)

def decrypt(data):
    iv = data[:16]
    cipher = data[16:]
    aes_obj = AES.new(KEY,AES.MODE_CBC,iv)
    plain = aes_obj.decrypt(cipher)
    return plain

def check_attack(data):
    for char in data:
        if char not in string.printable:
            return True
    return False

def main():
    while True:
        try:
            print("""
        ---[ HackFest CBC Game ]---
        """)
            print("[?] Ciphertext : ",end='')
            cipher = raw_input()
            cipher = base64.b64decode(cipher)
            if len(cipher) % 16 == 0:
                plain = decrypt(cipher)
                if 'Majulah Gunadarma Majulah Putra Indonesia Tuntut Ilmu Bersemangat Baja Tuk Mengemban Tugas Mulia Majulah Gunadarma Mujulah Putra Indonesia Kibarkan Panji Panji Cita Tonggak Membangun Negara Hiduplah Gunadarma Hidup Siswa dan Pendidiknya Mengabdi Pada Negara Bangsa Nan Jaya' in plain:
                    if check_attack(plain) == False:
                        print("[+] Congratulations : {}".format(flag))
                        break
                    else:
                        print("[x] Attack Detected")
                        break
                else:
                    print("[+] Plaintext : {}".format(plain.encode('hex')))
            else:
                print("[x] Invalid length of ciphertext")
                break
        except:
            break

if __name__ == "__main__":
    main()
