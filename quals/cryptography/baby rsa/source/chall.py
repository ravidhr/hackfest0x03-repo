from Crypto.Util.number import *
from libnum import *
#n = 266965481915457805187702917726550329693157
e = 65537
p = 8464149782874043593254414191179506861158311266932799636000173971661904149225893113311
q = 8464149782874043593254414191179506861158311266932799636000173971661904149225893113387
n = p*q

flag = 'HackFest{jUsT_a_BaSiC_r5a_cH4ll3nGe}'

m = ''
for i in flag:
	tmp = str(ord(i))
	if len(tmp) == 2:
		tmp = '0' + tmp
	m += tmp 
#print m
m = int(m)

c = pow(m, e, n)

print 'n :', n
print 'e :', e
print 'c :', c

