#!/usr/bin/python
import random, sys, os

class Unbuffered(object):
    def __init__(self, stream):
        self.stream = stream
    def write(self, data):
        self.stream.write(data)
        self.stream.flush()
    def writelines(self, datas):
        self.stream.writelines(datas)
        self.stream.flush()
    def __getattr__(self, attr):
        return getattr(self.stream, attr)

sys.stdout = Unbuffered(sys.stdout)

row, col = 50, 50
def get_map():
    mapnya = []
    for x in range(row):
        mapnya.append(['#' for n in range(col)])

    cur, cur_max, cur2, cur_max2 = 0, 4, col-1, col-1-4
    a,b = 0,col
    for x in range(row):
        if x<row:
            if a<col-1:
                if (x<=row/2 and a<col/2) or x>row/2:
                    a = random.randrange(cur, cur_max)
            if b>=0:
                if (x<=row/2 and b>=col/2) or x>row/2:
                    b = random.randrange(cur2, cur_max2, -1)
            mapnya[x][a] = ' '
            mapnya[x][b] = ' '
            cur = a
            if cur+4>col-1:
                cur_max = col
            else:
                cur_max = cur+4
            cur2 = b
            if cur2-4<0:
                cur_max2 = -1
            else:
                cur_max2 = cur2-4

    for x in range(len(mapnya)/2):
        if x==0:
            a1 = mapnya[x].index(' ')+1
            for y in range(mapnya[x].index(' ')):
                mapnya[x][y] =  ' '
            tmp = mapnya[x][::-1]
            b1 = tmp.index(' ')+1
            for y in range(tmp.index(' ')):
                tmp[y] = ' '
            mapnya[x] = tmp[::-1]
        elif x<col-1:
            a = mapnya[x].index(' ')
            for y in range(a1-1, a):
                mapnya[x][y] = ' '
            a1 = a+1
            tmp = mapnya[x][::-1]
            b = tmp.index(' ')
            for y in range(b1-1, b):
                tmp[y] = ' '
            mapnya[x] = tmp[::-1]
            b1 = b+1

    mapnya_tmp = mapnya[::-1]
    for x in range(len(mapnya_tmp)/2):
        if x==0:
            a1 = mapnya_tmp[x].index(' ')+1
            for y in range(mapnya_tmp[x].index(' ')):
                mapnya_tmp[x][y] =  ' '
            tmp = mapnya_tmp[x][::-1]
            b1 = tmp.index(' ')+1
            for y in range(tmp.index(' ')):
                tmp[y] = ' '
            mapnya_tmp[x] = tmp[::-1]
        elif x<col-1:
            a = mapnya_tmp[x].index(' ')
            for y in range(a1-1, a):
                mapnya_tmp[x][y] = ' '
            a1 = a+1
            tmp = mapnya_tmp[x][::-1]
            b = tmp.index(' ')
            for y in range(b1-1, b):
                tmp[y] = ' '
            mapnya_tmp[x] = tmp[::-1]
            b1 = b+1

    mapnya = mapnya_tmp[::-1]

    o = random.randrange(0, 8)
    cnt = 0
    for x in range(2):
        for y in range(4):
            if cnt == o:
                mapnya[(row/2)+x][(col/2)-2+y] = 'O'
                ox = (row/2)+x
                oy = (col/2)-2+y
            else:
                mapnya[(row/2)+x][(col/2)-2+y] = ' '
            cnt += 1

    bintang = random.randrange(0, 4)
    if bintang == 0:
        mapnya[0][0] = 'X'
        bx = 0; by = 0
    elif bintang == 1:
        mapnya[0][col-1] = 'X'
        bx = 0; by = col-1
    elif bintang == 2:
        mapnya[row-1][0] = 'X'
        bx = row-1; by = 0
    elif bintang == 3:
        mapnya[row-1][col-1] = 'X'
        bx = row-1; by = col-1
    return mapnya, ox, oy, bx, by

print("""
\t\t---[ MAZE GAME ]---\n
Pindahkan O ke titik X, maka kamu akan mendapatkan score +1.
Flag akan ditampilkan jika score kamu sudah mencapai 1337

Catatan : Gunakan w, a, s, d untuk memindahkan O
""")

raw_input()

score = 0
mapnya, ox, oy, bx, by = get_map()
while True:
    print ("\n" * 100)
    if score == 1337:
        print('HackFest{kamu_manual_apa_otomatis_nih_ngerjainnya_:D}')
        break
    maps = '\n'.join([''.join(n) for n in mapnya])
    print(maps)
    move = raw_input('[?] Langkah : ')
    try:
        if move == 'w' or move == 'W':
            if mapnya[ox-1][oy] == ' ' or mapnya[ox-1][oy] == 'X':
                mapnya[ox][oy] = ' '
                mapnya[ox-1][oy] = 'O'
                ox -= 1
        elif move == 'a' or move == 'A':
            if mapnya[ox][oy-1] == ' ' or mapnya[ox][oy-1] == 'X':
                mapnya[ox][oy] = ' '
                mapnya[ox][oy-1] = 'O'
                oy -= 1
        elif move == 's' or move == 'S':
            if mapnya[ox+1][oy] == ' ' or mapnya[ox+1][oy] == 'X':
                mapnya[ox][oy] = ' '
                mapnya[ox+1][oy] = 'O'
                ox += 1
        elif move == 'd' or move == 'D':
            if mapnya[ox][oy+1] == ' ' or mapnya[ox][oy+1] == 'X':
                mapnya[ox][oy] = ' '
                mapnya[ox][oy+1] = 'O'
                oy += 1
    except IndexError:
        continue
    if ox == bx and oy == by:
        mapnya, ox, oy, bx, by = get_map()
        score += 1
        print('score : ' + str(score))
