#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#define SZ_FLAG 100

u_int *(buf);

void win(){
	char buf[SZ_FLAG];
	FILE *f = fopen("flag.txt","r");
	if (f == NULL){
		puts("[!] error code 901");
		exit(0);
	}
	fgets(buf,SZ_FLAG,f);
	puts(buf);
}

void welcome(){

puts("	            ------               _____");
puts("	           /      \\ ___\\     ___/    ___");
puts("	        --/-  ___  /    \\/  /  /    /   \\");
puts("	       /     /           \\__     //_     \\");
puts("	      /                     \\   / ___     |");
puts("	      |           ___       \\/+--/        /");
puts("	       \\__           \\       \\           /");
puts("	          \\__                 |          /");
puts("	         \\     /____      /  /       |   /");
puts("	          _____/         ___       \\/  \\/\\");
puts("	               \__      /      /    |    |");
puts("	             /    \____/   \       /   //");
puts("	         // / / // / /\    /-_-/\//-__-");
puts("	          /  /  // /   \__// / / /  //");
puts("	         //   / /   //   /  // / // /");
puts("	          /// // / /   /  //  / //");
puts("	       //   //       //  /  // / /");
puts("	         / / / / /     /  /    /");
puts("	      ///  / / /  //  // /  // //");
puts("	         ///    /    /    / / / /");
puts("	    ///  /    // / /  // / / /  /");
puts("	       // ///   /      /// / /");
puts("	      /        /    // ///  /");

puts(" Hackfest 2020        author: arsalan (tripoloski) ");
}

void menu(){
	puts("+---------------------+");
	puts("|         Menu        |");
	puts("+---------------------+");
	puts("| 1. allocate memory  |");
	puts("| 2. freeing memory   |");
	puts("| 3. exit             |");
	puts("+---------------------+");
	printf("| select [1-3] : ");
}

void init(){
	setvbuf(stdout, 0 , 2 , 0);
	setvbuf(stdin, 0 , 2 , 0);
}

void create_memory(){
	int size;
	printf("[?] size : ");
	//read(0,size , sizeof(size));
	scanf("%d" , &size);
	printf("[?] data : ");
	if (size <= 0xffffff){
		buf = malloc(size);
		read(0,buf , size);
    puts("[+] memory allocated!");
	}else{
    puts("\n[!] something error");
  }
}

void release_memory(){
	free(buf);
}

void main(){
	init();
	char buf[4];
	welcome();
	while(1){
		menu();
		read(0 , buf , sizeof(buf));
		switch(atoi(buf)){
			case 1:
				create_memory();
				break;
			case 2:
				release_memory();
				break;
			case 3:
				puts("[+] exiting...");
				exit(0);``
				break;
			default:
				puts("[!] waduh ngapain gan");
				break;
		}
	}
}
