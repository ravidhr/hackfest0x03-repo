# eksor tidak jomblo Versi 2.0

	c'mon... you know the key, so it should be easy, right? *insert confused face emoji*

## solusi

Challenge ini agak sedikit tricky, karena multibyte xor biasa dan kita tahu keynya, tetapi setiap kali keynya dipakai, karakter2 di key tersebut di acak... Untungnya adalah, setiap karakter di key tersebut berbeda jadi kita bisa melacak perpindahan tiap karkater keynya

Maksudnya adalah, kita butuh lebih dari satu cipher untuk bisa melacak perpindahan setiap karakter keynya, semakin banyak cipher yang kita punya maka akan semakin akurat kita melacak perpindahan karakter keynya

Disini solver ini saya menggunakan 9 cipher

	import base64

	xor = lambda x, y: ''.join([chr(ord(x[i]) ^ ord(y[i % len(y)])) for i in range(len(x))])
	key = 'Th3KeySucz'

	cip1 = 'HDYVGURaDBMIOiQOSDoRFAl0AhkFFhA/RRMYJDxAOgoJEEMOXFkpJhAnHHQiF1ZVCkYOEw4HexoURTchHTYTEjFIHSoVHxMyRR0QBj0WOFlBHDMAF1kjHxc2HWVNektIDRomFTBaEVwQWT4iDRETASRZHAwJPzsfVRcccyJdGyACHkEYDnM6ETwBBiAXCjhzDFoKBh1FIwAHHEhaIEMtFRYXWRw4MlRPSAMYFlgNBgkgHjo3FmcGOglnABQAU0YmHTUTYDciFB1VSRE8fzwdegsp'
	cip1 = base64.b64decode(cip1)

	cip2 = 'MhwWGR90OkggDVwOVQw/TyB0AhYXJBYNVQoFJDxAATEJVmsgFVkBB0YcLlU9DlVzBxYnRUQ7SQcUWhE+ES5zIw1ZCxtFAFlSQx0NPSIQBnQRE1Q8J2sdDRccZUsdRldVIwwmFgFzESQWExQcOh4TJztFDiQPFRoNRRQDaxwGIBdSMyYyXmsjAR8TFg0HWhlDRloVPS9IDRwRLhMcCXQqByYaRS0ZG1RDdCsbFg4lVhs/AjoLN04GFBZKRjwqAz4MCxseZzpyKh5VZC8mOTwEWVAH'
	cip2 = base64.b64decode(cip2)

	cip3 = 'Aw0nX39DE1UOFxUkVQxHbBBINRUUFUAcRSI+JBoQOgEJH3QcFlUpQRANH0M6AR9INj4nQzwWSlwfRTAALwYTEhBIOxgMMVo1VTQcBloQOEMXPVQ7HGsSBgsQVFd6TUtzRBo+BAFzShUhQyYcFwxrJztaVQwVBAoCSBcqVVoLEA0qMwgCBUVzPwIQICAbPQRDdEUSXR5VPAAhVmsBCVUADEYmQx8EKjRfWj01FiAjBkANAQEMFlclbCdJWyYcYz46CxgMW2w0CzRKSTdsBwcSUgYp'
	cip3 = base64.b64decode(cip3)

	cip4 = 'LQA4GUQTEEM4JTsfWiIBXhBFUj9eGhANSCI5CjwWEAoZAGsXXHMYJgAOLkMBEXhzUQwBQxIVZAdEWTc+HlZVFDZFABg9MUNSSDQcGxM+J0UIDDQLR1kjMQcNHX1bRk1rEhUhFTBzAxoQQyYQXQxDPzwTMhUEFQoCRT4aawEUBkcYMxoCGGslJw1aCiAxCjhZWmspFBETIxwXMUhaCUMMPAwREy0EGzNfcz0UBiA/MRsXSBMMNwcGPApJVioqexAMC1IMRiYVNyx7WDA8NhZUYgYI'
	cip4 = base64.b64decode(cip4)

	cip5 = 'NAA4BFVzWloeJTwORVocUglDKjheJAYNdAwXGAwgIhgJEEMNPEhRJhZHH3MMHHhVKQwXRR8kRRYjc1cPHQ1rDVZ0OxQVAGsbcxIGBloQCnQhEwQjERMdDQsxelsdRn1rDgoPDwF0AAwAEz4TBi9ZPzxIEwoPFjtEWiYqWQoLBiAJVBE1F0ULRyQBBg0xPV9VXEgkFx5rIxAIMRMKIEgcFg8RSC0/GBIJRRwUCxIlMUAOHjoUAFE9bApJeCoRZA8MDQlFUCoKJjNVRVcmLzwdWSgp'
	cip5 = base64.b64decode(cip5)

	cip6 = 'HAYJP1ZZWlU/BgckdApHQgZrGxU5CiABSBMOO1wKOgoJVlU/DHQbCCY/HEMBIFYTFxAgExI8WBoNWS8WVwB0FBBzEhg9DVUEcx5WFAodOHQHWh0jEUMAMQs2eh1LVFdrFDwdGQFZEVwmWiY9GwdFFzxILRoVXzsNawgtSBxdIA4YBCYUXlk6FyQMOBwKEAkTSnQiJR5zCx8mHHMcOEhKCiEaQwNfFB5xczIyFgMjHxANSD0UFgclDBBJSgscWEYlHTUPWzwKDAJkAx0qJDwESygY'
	cip6 = base64.b64decode(cip6)

	cip7 = 'Ii44Px9aCkUSBjwORSIgUhsTAhYYJCBHWRMFEwonEAo/MUUBXEMYORAgLloQAU9IMUZHWRI7UxokWi8dDBBrDVZZOzUTBhMbax4QDT0QG3MRHFQjHEUSNgsxZR16fUtDHxUMGS9VLQcmExcKCx1ZFzwTLRUZOAofWSYRdDpdFhcUHREyOWs1DRJaCRwpHF9zRUUpFwd0ETEHVkU6CUgyDAwmVRw/UgJZSAMyFhISVhAOHiI3BkEUPEBgeDcAZEYMPCoeVjw7Kh5KRR1sBwoEYjcV'
	cip7 = base64.b64decode(cip7)

	cip8 = 'AxAEX0lDEGsRPRU8WRwncxsTBA8UPEAcWgoYFSQnDBs/EHQOXGsKEQAnVmsQIE9aBx0OEx88RTsOawcQVxB0JAZrDQkPH0NSVR0AOjo+G1kLDDQSAWsLDSZWVE1LfWVIAjtGFRFZGgo+cxc9XQxIDQpzLVwWGQwjdD4aRSIXBhxSHRo1PhM1ETwKCQ02E19DWkg0PS9ZPDYLEEUBEGsDXEYRSDIJGB1xVRsCKA4zHCAgSAElUH8LJRtgZzwMSUYLERsVeDo0DA9VY1clBwcESDcI'
	cip8 = base64.b64decode(cip8)

	cip9 = 'HDEWFU9VAXMOXTsNSAoRXgYTKj8IFhAcaz0XQzwGARg4NlU/FhMHEQ8/EEUBJx9DGyFHSBIaYwwjWh0+MDZFHwYTHRs9HFk1VR1WPQo+FkghCjMjAUUSHEENVH1GTWVZRDsQGQdFSgcMcxgiFDBrFwdzMhUVXwoCSAgtVSIXCRcyVAsJXkMUPw0cFicbPRZzV0UiG1drADYIEEMQQHQcJAwhawUWUjNPRSAUAAM8LhYNL1oMUE0bCyBbUDoDexYmIxsiADcSOhJFWB0MKQwzelAH'
	cip9 = base64.b64decode(cip9)

Setelah kita kumpulkan ke 9 ciphernya kita bikin suatu list untuk menampung index-index acakan yang ada dan juga kita masukan character2 yang ada di key ke dalam sebuah list

	index_acakan = []
	listkey = list(key)

Lalu kita buat perulangan sebanyak panjang karakter salah satu cipher, dan buat list kosong untuk masing2 cipher di dalam perulangan tersebut untuk menampung nilai index ketika dixor dengan karakter2 pada key

	for x in range(len(cip1)):
		listhasil1 = []
		listhasil2 = []
		listhasil3 = []
		listhasil4 = []
		listhasil5 = []
		listhasil6 = []
		listhasil7 = []
		listhasil8 = []
		listhasil9 = []

Lalu kita buat lagi sebuah perulangan di dalamnya untuk membrute xor setiap karakter key dengan karakter2 yang ada di tiap2 cipher untuk dimasukkan ke masing2 list yang sudah kita buat sebelumnya

		for i in range(len(key)):
			listhasil1.append(ord(cip1[x]) ^ ord(listkey[i]))
			listhasil2.append(ord(cip2[x]) ^ ord(listkey[i]))
			listhasil3.append(ord(cip3[x]) ^ ord(listkey[i]))
			listhasil4.append(ord(cip4[x]) ^ ord(listkey[i]))
			listhasil5.append(ord(cip5[x]) ^ ord(listkey[i]))
			listhasil6.append(ord(cip6[x]) ^ ord(listkey[i]))
			listhasil7.append(ord(cip7[x]) ^ ord(listkey[i]))
			listhasil8.append(ord(cip8[x]) ^ ord(listkey[i]))
			listhasil9.append(ord(cip9[x]) ^ ord(listkey[i]))

Tinggal bikin satu list kosong untuk menampung nilai index dari salah satu cipher yang mana pada index cipher tersebut mengandung karakter yang sama dengan karakter yang ada di cipher lainnya setelah dixor

		sama = []

		for i in listhasil1:
			if i in listhasil2:
				if i in listhasil3:
					if i in listhasil4:
						if i in listhasil5:
							if i in listhasil6:
								if i in listhasil7:
									if i in listhasil8:
										if i in listhasil9:
											sama.append(listhasil1.index(i))

Di sinilah gunanya kita memiliki banyak cipher, jika kita hanya menggunakan 2 cipher maka akan ada kemungkinan yang sangat besar 'index sama' yang ditemukan lebih dari satu, semakin banyak ciphernya semakin sedikit 'index sama' yang ditemukan... Jika 'index sama' yang ditemukan hanya satu maka dapat dipastikan di posisi index itulah karakter keynya ditukar untuk cipher yang menjadi perbandingan (untuk solver ini cipher yang menjadi perbandingan adalah cipher1 seperti yang ada pada percabangan di atas)

Jika index yg ditemukan tepat 1 index, maka tinggal diappend ke list index_acakan yg sebelumnya sudah dibuat di awal program


		if len(sama) > 1:
			print "ada yg lebih dari 1 index"
			exit()
		else:
			index_acakan.append(sama[0])

Setelah itu kita tinggal buat key yang baru berdasarkan index acak yang kita punya

	list_new_key = []
	for i in index_acakan:
		list_new_key.append(listkey[int(i)])

	newkey = ''.join(list_new_key)


jika benar, maka harusnya panjang key yang baru sama dengan panjang cipher, tinggal dixor dan dapatlah flagnya


	flag = ''
	for i in range(0, len(cip1)):
		flag += chr(ord(cip1[i]) ^ ord(newkey[i]))

	print flag



FLAG : HackFest{i_c4n_s33_y0u_hav3_A_g00d_Log1c}
