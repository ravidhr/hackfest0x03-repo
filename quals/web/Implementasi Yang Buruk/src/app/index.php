<?php

    if(array_key_exists('submit', $_POST)) {
        $text = $_POST['text'];
        $function = $_POST['function'];
        echo 'Hasil: ' . $function($text);
    }

?>
<form action="" method="POST">
    <textarea name="text" id="" cols="30" rows="10"></textarea>
    <select name="function" id="">
        <option value="base64_encode">Base64</option>
        <option value="base32_encode">Base 32</option>
        <option value="str_rot13">ROT13</option>
        <option value="md5">MD5</option>
    </select>
    <button type="submit" name="submit">Send</button>
</form>