#!/bin/bash
reldir=`dirname $0`
cd $reldir

time_limit="${3}"

if /usr/bin/curl -sSf --max-time "${time_limit}" "${1}:${2}" --insecure 2>/dev/null >/dev/null; then
    true
else
    /usr/bin/python3.6 app.py
    false
fi
