# Ekstrak Kulit Manggis

# Deskripsi

    menyalahgunakan fungsi php extract()

# Solusi

pada source code tersebut peserta diminta untuk menganalisa bagian mana yang terdapat celah yang dapat dimanfaatkan, letak celahnya ada difungsi extract() yang dimana php akan mengimport nilai array menjadi variabel

    index.php?username&password&userl33t&passl33t

flag : hackfest{something_wrong_i_can_feel_it}