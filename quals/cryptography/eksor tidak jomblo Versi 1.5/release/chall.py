from secret import msg, flag
import random

huhuhu = flag + msg

key = ''
for i in range(8):
	key += chr(random.randrange(0, 256))

cipher = ''
for i in range(0, len(huhuhu)):
	cipher += chr(ord(huhuhu[i]) ^ ord(key[i%len(key)]))

w = open('cipher.txt', 'w')
w.write(cipher)
w.close()