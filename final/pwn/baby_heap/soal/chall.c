#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
struct item{
	char *msg ;
};

struct item ptr[0x90] = {0};

int num;
void banner() {
  puts("\t ____   __   ____  _  _    _  _  ____   __   ____");
  puts("\t(  _ \\ / _\\ (  _ \\( \\/ )  / )( \\(  __) / _\\ (  _ \\");
  puts("\t) _ ( /    \\ ) _ ( )  /   ) __ ( ) _) /    \\ ) __/");
  puts("\t(____/\\_/\\_/(____/(__/    \\_)(_/(____)\\_/\\_/(__)  ");
	printf("\n\t\t\t[ ^_^ ]\t");
}

void choicemenu() {
  printf("\n[1] create\n[2] show\n[3] delete\n[4] exit\n> ");
}

void init()
{
    setvbuf(stdin, 0, 2, 0);
    setvbuf(stdout, 0, 2, 0);
}

int read_int()
{
    char c[16];
    read(0, c, 16);
    return atoi(c);
}

void read_str(int n, char* str)
{
    int c;
    c = read(0, str, n+1);
}


int create() {
  int length;
  int size;
  if(num < 0x90) {
    printf("\n[?] size : ");
    length = read_int();
    if(length <= 0 || length > 0x3e0) {
      puts("[x] invalid size");
      return 0;
    } else {
      for(int i = 0;i < 0x90; i++) {
        if(!ptr[i].msg) {
          ptr[i].msg = (char*)malloc(length);
          printf("[?] msg : ");
					read_str(length,ptr[i].msg);
          num++;
          puts("[+] success");
          break;
        }
      }
    }
  } else {
    puts("[!] resource full");
  }
}

int show() {
  int index;
  if(!num) {
    puts("[x] empty");
    return 0;
  } else {
    printf("\n[?] idx : ");
    index = read_int();
    if(ptr[index].msg) {
      printf("[+] msg : %s\n",ptr[index].msg);
    } else {
      printf("[x] data not found\n");
    }
  }
}

int delete() {
  int index;
  if(!num) {
    puts("[x] empty");
    return 0;
  } else {
    printf("\n[?] idx : ");
    index = read_int();
    if(ptr[index].msg) {
      free(ptr[index].msg);
      ptr[index].msg = 0;
      printf("[+] delete success\n");
    } else {
      printf("[x] data not found\n");
    }
  }
}

void main() {
  int choice;
	init();
  banner();
  while(1) {
    choicemenu();
    choice = read_int();
    switch(choice) {
      case 1:
      create();
      break;
      case 2:
      show();
      break;
      case 3:
      delete();
      break;
      case 4:
      exit(0);
      break;
      default:
      puts("[!] try again");
      break;
    }
  }
}
