# Ekstrak Kulit Manggis

# Deskripsi

    memanipulasi beberapa nilai pada cookie dan pengetahuan sedikit tentang hash crack

# Solusi

challenge ini peserta mengharuskan memanipulasi beberapa nilai pada cookie yang sudah diset default oleh server, ada 3 bagian nilai cookie yaitu user, role dan hash yang dimana peserta hanya mengubah nilai role dan mengubah nilai hash, yang dimana hasil nilai hash dari role itu sendiri, dan melakukan encode ke base64 dan mengubah nilainya di localstorage browser

    {"user":"guest","role":"admin","hash":"21232f297a57a5a743894a0e4a801fc3"}

flag : hackfest{sama_kaya_sebelumnya_hanya_aja_ditambah_verifikasi_dari_role}