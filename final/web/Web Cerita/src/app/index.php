<?php

    require 'config/config.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cyber Community Universitas Gunadarma</title>
    <?php if(!empty($_GET['id'])): ?>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <?php else: ?>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <?php endif; ?>
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">CCUG</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <?php if(array_key_exists('fullname', $_SESSION)): ?>
                    <?php if(!empty($_GET['id'])): ?>
                    <li class="nav-item active">
                        <a class="nav-link" href="../beranda">Beranda<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../cerita">Cerita</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../kontak">Kontak</a>
                    </li>
                    <?php else: ?>
                        <li class="nav-item active">
                        <a class="nav-link" href="beranda">Beranda<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="cerita">Cerita</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="kontak">Kontak</a>
                    </li>
                    <?php endif; ?>
                </ul>
                <?php if(!empty($_GET['id'])): ?>
                <span class="navbar-text">
                    <a href="../keluar">Keluar</a>
                </span>
                <?php else: ?>
                    <span class="navbar-text">
                    <a href="keluar">Keluar</a>
                </span>
                <?php endif; ?>
                <?php endif; ?>
        </div>
    </nav>

    <?php

        if(array_key_exists('page', $_GET) && file_exists($_GET['page'])) {
            include $_GET['page'];
        } else {
            header('Location: masuk');
        }

    ?>

</body>
</html>