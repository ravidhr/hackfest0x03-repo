from Crypto.Hash import MD5
import random

md5 = lambda x: MD5.new(x).digest()

flag = 'HackFest{tH3_r3al_AESchallenge_i5_0n_it5_w4yyyy}'
key = md5(str(random.randrange(0, 9999999999)))