from Crypto.Cipher import AES
from Crypto.Util import Counter
from secret import flag, key, iv
import sys
import base64
import random
import binascii

class Unbuffered(object):
    def __init__(self, stream):
        self.stream = stream
    def write(self, data):
        self.stream.write(data)
        self.stream.flush()
    def writelines(self, datas):
        self.stream.writelines(datas)
        self.stream.flush()
    def __getattr__(self, attr):
        return getattr(self.stream, attr)

sys.stdout = Unbuffered(sys.stdout)


huhu = random.randint(0, 100)

def padding(s):
	padding_len = 16 - (len(s) & 0xf)
	return chr(padding_len) * padding_len

def ofb_enc(p):
	print('Encrypt using OFB mode')
	plaintext = p + padding(p)
	aes_obj = AES.new(key, AES.MODE_OFB, iv)
	cipher = aes_obj.encrypt(plaintext)
	b64_cipher = base64.b64encode(cipher)
	return b64_cipher

def ctr_enc(p):
	print('Encrypt using CTR mode')
	ctr = Counter.new(128, initial_value=int(binascii.hexlify(iv), 16))
	aes_obj = AES.new(key, AES.MODE_CTR, counter=ctr)
	cipher = aes_obj.encrypt(p)
	b64_cipher = base64.b64encode(cipher)
	return b64_cipher

def enc_aes(r, p):
	if r%2 == 0:
		return ofb_enc(p)
	else:
		return ctr_enc(p)

def main():

	print("""
    ----------------------------------
    Welcome to AES Encryption Service
    ----------------------------------

    1. Encrypt A Message
    2. Encrypt Flag
    
		""")
	pilihan = raw_input('Your Choice? ')
	if pilihan == '1':
		m = raw_input('Type your message: ')
		hasil = enc_aes(huhu, m)
		print('Your encrypted message is: ', hasil)
	elif pilihan == '2':
		hasil = enc_aes(huhu, flag)
		print('Encrypted flag : ', hasil)
	else:
		print("Bye bye")
		exit()

while True:
	main()

