# DOCX to PDF

## Deskripsi
Terdapat sebuah web untuk melakukan konversi DOCX to PDF secara online.
Website ini dibuild dengan menggunakan Laravel.

## Solusi

Pada dasarnya sebuah DOCX adalah file zip yang berisi XML.
Terdapat langkah-langkah yang harus dilakukan peserta untuk mendapatkan flag.

1. Memasukan payload XXE Injection ke .docx yang diupload untuk mendapatkan file sensitif dari Laravel yaitu .env
2. Setelah mendapatkan .env, peserta melakukan decrypt pada laravel_session dan terlihat bahwa
   Laravel yang berjalan vulnerable terhadap CVE-2018-15133
3. Terdapat dua kesulitan lain untuk peserta yaitu peserta harus melakukan modifikasi CVE terlebih dahulu
   untuk mendapatkan Remote Code Execution. Karena terdapat 2 file penting dari Laravel ini yang dirubah yaitu
   Encrypter.php dan PendingBroadcast.php.
   Yang diganti pada Encrypter.php adalah fungsi hash menjadi seperti berikut
        return hash_hmac('sha256', hash('sha256',$value.$iv), $this->key);
   Yang diganti pada PendingBroadcast.php adalah 2 variabel penting menjadi seperti berikut
        protected $pending_events;
        protected $pending_event;
   Untuk lebih detilnya silahkan diff PendingBroadcast.php dan Encrypter.php milik challenge ini dengan milik Laravel asli.
4. Setelah itu, peserta sudah bisa menyusun payload RCE menggunakan PHPGGC https://github.com/ambionics/phpggc
5. Sesuaikan nama variabelnya ketika menggenerate POP chain
6. Setelah itu, kirim exploit dan peserta sudah mendapatkan RCE.
