cipher = open('cipher.txt', 'r').read()


formatflag = 'HackFest'

key = ''
for i in range(8):
	key += chr(ord(formatflag[i]) ^ ord(cipher[i]))


flag = ''
for i in range(0, len(cipher)):
	flag += chr(ord(cipher[i]) ^ ord(key[i%len(key)]))

print flag