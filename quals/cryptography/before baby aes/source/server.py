#!usr/bin/python2
from secret import flag, key
from Crypto.Cipher import AES
import sys

class Unbuffered(object):
    def __init__(self, stream):
        self.stream = stream
    def write(self, data):
        self.stream.write(data)
        self.stream.flush()
    def writelines(self, datas):
        self.stream.writelines(datas)
        self.stream.flush()
    def __getattr__(self, attr):
        return getattr(self.stream, attr)

sys.stdout = Unbuffered(sys.stdout)


def encrypt_flag():
    cipher = AES.new(key, AES.MODE_ECB)
    encrypted = cipher.encrypt(flag)
    ciphertext = key.encode('hex') + encrypted.encode('hex')

    return {"ciphertext": ciphertext}

def decrypt(KEY, ciphertext):
    try:
        ciphertext = ciphertext.decode('hex')
        KEY = KEY.decode('hex')
        cipher = AES.new(KEY, AES.MODE_ECB)
        try:
            decrypted = cipher.decrypt(ciphertext)
        except ValueError as e:
            return {"error": str(e)}
    except:
        return {"error": "ada yg salah, gan"}
    return {"plaintext": decrypted.encode('hex')}


def main():
    print '''
=========================================
        Menu Utama
    1. Encrypted Flag
    2. Decrypt Something
=========================================
    '''

    pilihan = raw_input("Masukan Pilihan: ")
    print '-----------------------------------------'
    if pilihan == '1':
        print encrypt_flag()
        print '-----------------------------------------'
        exit()
    elif pilihan == '2':
        k = raw_input('Masukkan key (hex): ')
        c = raw_input('Masukkan cipher (hex): ')
        print ''
        print decrypt(k, c)
        print '-----------------------------------------'
        exit()
    else:
        print ">:("
        exit()

while True:
    try:
        main()
    except:
        print  '''
-----------------------------
       bye bye.
-----------------------------
        '''
        exit()
