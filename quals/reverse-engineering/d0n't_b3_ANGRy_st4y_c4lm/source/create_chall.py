#!/usr/bin/env python3
from os import system
from random import randint, choice

flag = "Entah bagaimana anda menyelesaikannya tapi ini adalah FL4G: 'TIFEST{4ngr_s1mul4710n_M4n4g3rs_s4ng4t_b3rgun4}'. buat kamu yang sudah menyelesaikan tantangan ini :)."


body = """#include <stdio.h>
#include <stdlib.h>
#include <string.h>

%s

int main(){
    char s[0xff];

    printf("[?] Password: ");
    fgets(s, 0xff, stdin);

    if (%s){
        puts("Correct!...");
    } else {
        puts("Incorrect!...");
    }
    return 0;
}
"""

func = """int func_%d(char *a1){
    %s
}\n"""

one = ''

for i in range(len(flag)):
    case = choice([1,2,3])
    g = randint(0, len(flag)-1)
    v1 = randint(5,15)
    v2 = randint(15,25)
    v3 = randint(25,55)

    if case == 1:
        res = (ord(flag[i]) + v3) - v2
        almost = f"return ((*(a1+{i}) + {hex(v3)}) - {v2} == {hex(res)} && *(a1+{i}) > *(a1+{g}));"

    elif case == 2:
        res = (ord(flag[i]) - v1) + v3
        almost = f"return (*(a1+{i}) < *(a1+{g}) && (*(a1+{i}) - {v1}) + {hex(v3)} == {hex(res)});"

    elif case == 3:
        res = (ord(flag[g]) * v3) % v2
        almost = f"return ((*(a1+{g}) * {hex(v3)}) % {v2} == {hex(res)} && *(a1+{i}) == *(a1+{g}));"

    final = func%(i, almost)
    one += final

look = "func_%d(s)"
check = ''
for i in range(len(flag)):
    if i != len(flag)-1:
        check += look%i+" && "
    else:
        check += look%i

full = body%(one, check)

#with open('rev.c', 'w') as out:
with open('rev_new.c', 'w') as out:
    out.write(full)
    out.close()

#compile_rev = "gcc rev.c -o rev -fno-stack-protector -no-pie -m64"
compile_rev = "gcc rev_new.c -o rev_new -fno-stack-protector -no-pie -m64"
system(compile_rev)
