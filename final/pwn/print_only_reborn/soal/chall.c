#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

char buf[0x200];

void init()
{
	setvbuf(stdin,0,2,0);
	setvbuf(stdout,0,2,0);
	setvbuf(stderr,0,2,0);
}

void vuln() {
  read(0,buf,0x128);
  printf(buf);
}

void main() {
  init();
  while(1) {
    vuln();
  }
}
