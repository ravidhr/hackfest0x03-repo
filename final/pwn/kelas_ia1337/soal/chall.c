//gcc chall.c -o chall -m64 -no-pie -fstack-protector-all -g -O0 -Wl,-z,relro,-z,now
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
struct item{
	int size ;
	char *name ;
};

struct item mahasiswa[100] = {0};

int num;

void menu(){
	puts("\n----------------------------------------");
	puts("\tMahasiswa IA-1337\t");
	puts("----------------------------------------");
	puts("\t[1] Tambah Data Mahasiswa");
	puts("\t[2] Tampilkan Data Mahasiswa");
	puts("\t[3] Ubah Data Mahasiswa");
	puts("\t[4] Hapus Data Mahasiswa");
	puts("\t[5] Keluar");
	puts("----------------------------------------");
	printf("Pilihan : ");
}


void lihat_mahasiswa(){
	int i ;
	if(!num){
		puts("[x] Database masih kosong");
	}else{
		printf("\n-----------[ Daftar Nama Mahasiswa ]---------------\n");
		for(i = 0 ; i < 100; i++){
			if(mahasiswa[i].name){
				printf("[%d] %s\n",i,mahasiswa[i].name);
			}
		}
		puts("");
	}
}

int tambah_mahasiswa(){

	char sizebuf[8] ;
	int length ;
	int i ;
	int size ;
	if(num < 100){
		printf("\n[?] Panjang Nama Mahasiswa: ");
		read(0,sizebuf,8);
		length = atoi(sizebuf);
		if(length == 0){
			puts("[x] Panjang nama mahasiswa tidak valid");
			return 0;
		}
		for(i = 0 ; i < 100 ; i++){
			if(!mahasiswa[i].name){
				mahasiswa[i].size = length ;
				mahasiswa[i].name = (char*)malloc(length);
				printf("[?] Nama Mahasiswa: ");
				read(0,mahasiswa[i].name,length);
				num++;
        puts("[+] Data mahasiswa berhasil ditambahkan");
				break;
			}
		}

	}else{
		puts("[x] Database penuh");
	}
	return 0;
}



void edit_mahasiswa(){

	char indexbuf[8] ;
	char lengthbuf[8];
	int length ;
	int index ;

	if(!num){
		puts("[x] Database masih kosong");
	}else{
		printf("[?] Index : ");
		read(0,indexbuf,8);
		index = atoi(indexbuf);
		if(mahasiswa[index].name){
			printf("[+] Panjang Nama Mahasiswa: ");
			read(0,lengthbuf,8);
			length = atoi(lengthbuf);
			printf("[+] Nama Mahasiswa: ");
			read(0,mahasiswa[index].name,length);
		}else{
			puts("[x] Data Mahasiswa Tidak Ditemukan");
		}

	}

}

void hapus_mahasiswa(){
	char indexbuf[8] ;
	int index ;

	if(!num){
		puts("[x] Data Mahasiswa Tidak Ditemukan");
	}else{
		printf("[?] Index : ");
		read(0,indexbuf,8);
		index = atoi(indexbuf);
		if(mahasiswa[index].name){
			free(mahasiswa[index].name);
			mahasiswa[index].name = 0 ;
			mahasiswa[index].size = 0 ;
			puts("[+] Hapus Data Mahasiswa Berhasil");
			num-- ;
		}else{
			puts("[x] Data Mahasiswa Tidak Ditemukan");
		}
	}
}

int main(){

	char choicebuf[8];
	int choice;
	setvbuf(stdout,0,2,0);
	setvbuf(stdin,0,2,0);

	while(1){
		menu();
		read(0,choicebuf,8);
		choice = atoi(choicebuf);
		switch(choice){
			case 1:
        tambah_mahasiswa();
				break;
			case 2:
        lihat_mahasiswa();
				break;
			case 3:
				edit_mahasiswa();
				break;
			case 4:
				hapus_mahasiswa();
				break;
			case 5:
				exit(0);
				break;
			default:
				puts("[x] Pilihan tidak ditemukan");
				break;
		}
	}

	return 0 ;
}
