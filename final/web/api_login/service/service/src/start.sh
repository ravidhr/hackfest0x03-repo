#!/bin/bash

cd /app

chown -R root:root /app
chmod -R 555 /app

runuser -l ctf -c 'cd /app; /usr/bin/python3 /app/app.py' &

(crontab -l ; echo "* * * * * /bin/bash -c '/sanity.sh 127.0.0.1 8000 10'") | crontab
service cron start
