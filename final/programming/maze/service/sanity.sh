#!/bin/bash
reldir=`dirname $0`
cd $reldir

time_limit="${3}"

if timeout $time_limit nc -z ${1} ${2} 2>/dev/null >/dev/null; then
    true
else
    /usr/bin/socat -s -d -d -d TCP4-LISTEN:${2},reuseaddr,fork,su=nobody EXEC:"/usr/bin/python chall.py"
    false
fi
