from Crypto import Random
import random
import string
import requests

def generate_random_string(length):
    res = ""
    achar = string.lowercase + string.uppercase + string.digits
    for x in range(length):
        res += random.choice(achar)
    return res
def encode_payload(str):
    res = ""
    for x in range(len(str)):
        num = int(Random.new().read(1).encode('hex'),16)
        tmp_chr = ord(str[x])
        #if num % 3 == 0:
        #    tmp = bin(tmp_chr)[2:]
        #    res += "chr(int('{}',2))".format(tmp)
        if num % 2 == 0:
            #print(hex(tmp_chr))
            tmp = hex(tmp_chr)
            res += "chr({})".format(tmp)
        else:
            tmp = tmp_chr
            res += "chr({})".format(tmp)
        if x + 1 < len(str):
            res += "+"
    #print(res)
    final_res = "python -c 'open(\"/tmp/{}\",\"wb\").write({})'".format(generate_random_string(3),res)
    return final_res

def main():
    #print(len(flag))
    flag = "HackFest{ciee_kamu_nyariin_aku_wkwkwk}"
    for x in range(20):
        if x == 15:
            tmp = flag
        else:
            tmp = generate_random_string(38)
        headers = {'User-Agent':'Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0'}
        param = {'cmd':encode_payload(tmp)}
        r = requests.get("http://10.10.10.143/tmpbeept.php",params=param,headers=headers)
        print(tmp)



if __name__ == "__main__":
    main()
