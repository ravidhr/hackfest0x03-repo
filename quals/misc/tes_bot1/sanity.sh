#!/bin/bash
cd /home/ctf/hackfest0x03-repo/quals/misc/tes_bot1

time_limit="${3}"

if timeout $time_limit nc -z ${1} ${2} 2>/dev/null >/dev/null; then
    true
else
    /usr/bin/socat -s -d -d -d TCP4-LISTEN:${2},reuseaddr,fork,su=twistbil EXEC:"/usr/bin/python3 tesbot.py"
    false
fi
