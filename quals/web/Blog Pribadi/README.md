# Blog Pribadi

# Deskripsi

    aldo baru saja merilis blog artikel tetapi aldo tidak mengetahui apakah web yang baru saja dibuat aman atau tidak, bisakah kamu membantu aldo untuk menguji keamanan webnya?

# Solusi

pada tantangan ini peserta diminta untuk melakukan ekploitasi kerentanan sql injection union based untuk mendapatkan flagnya yang terletak ditable flag_here, ada dua parameter yang rentan akan sql injection yaitu ?search dan ?id

    ' UNION SELECT 1,flag FROM flag_here--

flag : hackfest{twice_lebih_WOW_daripada_blackpink_h3h3h3}