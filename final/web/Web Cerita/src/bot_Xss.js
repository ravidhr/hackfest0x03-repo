var page = require('webpage').create();
page.settings.userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A';
page.settings.javascriptEnabled = true;
page.settings.webSecurityEnabled = false;
page.settings.localToRemoteUrlAccessEnabled = true;
page.settings.navigationLocked = false;
phantom.cookiesEnabled = true;
phantom.javascriptEnabled = true;
var timeout = 3000;
var url = '';

phantom.addCookie({
    'name': 'PHPSESSID',
    'value': '',
    'domain': '',
    'path': '/',
    'httponly': true
});

page.onNavigationRequested = function(url, type, willNavigate, main) {
    console.log("[URL] URL=" + url);
};

page.settings.resourceTimeout = timeout;
page.onResourceTimeout = function(e) {
    setTimeout(function() {
        console.log("[INFO] Timeout");
        phantom.exit();
    }, 3000);
};

page.open(url, function(status) {
    console.log("[INFO] rendered page");
        setTimeout(function() {
        phantom.exit();
    }, 3000);
});