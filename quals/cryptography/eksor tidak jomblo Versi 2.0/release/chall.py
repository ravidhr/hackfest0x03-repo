#!/usr/bin/python
from flag import flag
import random, sys
import base64

class Unbuffered(object):
    def __init__(self, stream):
        self.stream = stream
    def write(self, data):
        self.stream.write(data)
        self.stream.flush()
    def writelines(self, datas):
        self.stream.writelines(datas)
        self.stream.flush()
    def __getattr__(self, attr):
        return getattr(self.stream, attr)

sys.stdout = Unbuffered(sys.stdout)

key = 'Th3KeySucz'
xor = lambda x, y: ''.join([chr(ord(x[i]) ^ ord(y[i % len(y)])) for i in range(len(x))])

assert len(flag) % len(key) == 0

def encrypt(flag, key):
	cipher = ''
	for i in range(0, len(flag), len(key)):
		key = ''.join(random.sample(key, len(key)))
		cipher += xor(flag[i:i+len(key)], key)
	return 'ciphertext: %s' % (base64.b64encode(cipher))

print encrypt(flag, key)
