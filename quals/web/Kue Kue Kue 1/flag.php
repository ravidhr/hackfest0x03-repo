<?php

    error_reporting(0);
    if(isset($_COOKIE['auth'])) {
        $cookie = json_decode(base64_decode($_COOKIE['auth']), TRUE);
        if(isset($cookie['user']) && $cookie['role'] === 'admin') {
            die('hackfest{privilege_escalation_with_cookie_poisoning}');
        } else {
            die('Role selain admin dilarang akses!');
        }
    } else {
        http_response_code(406);
    }