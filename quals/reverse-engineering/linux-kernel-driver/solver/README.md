# LINUX KERNEL DRIVER

# Deskripsi
    
    flagnya sembunyi di kernel space 

# Solusi 
pada soal ini peserta dapat mendapatkan flag dengan melakukan
reverse engineering terhadap kernel driver ini, atau bisa juga
dengan meload kernel tersebut ke linux secara langsung

    sudo insmod driver.ko
    dmesg | tail -c 650

FLAG : hackfest{hello from the kernel}  


