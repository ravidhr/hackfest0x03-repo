<?php

    if(array_key_exists('submit', $_POST)) {
        $fullname = $_SESSION['fullname'];
        $judul = addslashes($_POST['judul']);
        $cerita = addslashes($_POST['cerita']);
        if($conn->query("INSERT INTO tbl_story VALUES ('', '{$fullname}', '{$judul}', '{$cerita}')") OR die($conn->error)) {
            echo '<script>alert("Berhasil menambahkan cerita");</script>';
        } else {
            echo '<script>alert("Gagal menambahkan cerita");</script>';
        }
    }

?>
<h3>Hallo <?= $_SESSION['fullname']; ?></h3>
<hr>
<h3>Tambah Cerita</h3>
<form action="" method="POST">
    <label for="judul">Judul</label>
    <input type="text" name="judul" id="judul"><br>
    <label for="cerita">Cerita</label>
    <textarea name="cerita" id="" cols="30" rows="10"></textarea>
    <button type="submit" class="btn btn-primary" name="submit">Kirim</button>
</form>
<?php

    if(array_key_exists('role', $_SESSION) && $_SESSION['role'] === 'admin') {
        $data = $conn->query('SELECT message FROM tbl_contact');
        echo $data->fetch_assoc()['message'];
        $conn->query('DELETE FROM tbl_contact LIMIT 1');
    }

?>