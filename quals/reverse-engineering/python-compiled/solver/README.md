# Python Compiled

    tolong decompile file saya, flag ada pada file tersebut, dan agak terenkripsi

## Solusi

untuk dapat membaca file source.pyc dengan lebih mudah, kita dapat menggunakan tools
`uncompyle6` dapat di install [disini](https://pypi.org/project/uncompyle6/) gunakan
perintah `uncompyle6 source.pyc` lalu akan terlihat source code tersebut file tersebut

    def encrypt(data , key):
        c = ''
        for i in data:
            c += chr((ord(i) ^ key) % 0x256)
        print c
        return c

    def main():
        msg = "elnfkh~yvhlaat-hl~t-d~c*y-dy-2p"
        inp = raw_input("masukan flag : ")
        key = int(raw_input('key : '))
        x = encrypt(inp , key)
        if x == msg:
            print("yeahh that's the flag mate ! ")
        else:
            print("stupid gunadarma student")

    if __name__ == '__main__':
        main()


selanjutnya, kita hanya tinggal membalikan flag yang terenkripsi, sesuaikan dengan logic
pada file source.pyc flag di enkripsi dengan kunci rahasia, lakukan bruteforce untuk menemukan 
kunci yang sesuai

    a = "elnfkh~yvhlaat-hl~t-d~c*y-dy-2p"
    for k in range(0xff):
        x = ''
        for i in a: 
            x +=  chr((ord(i) ^ k) % 0xff)
        print x


FLAG : hackfest{eally easy isn't it ?}