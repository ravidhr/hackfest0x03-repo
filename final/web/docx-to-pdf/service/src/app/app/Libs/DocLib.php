<?php
namespace App\Libs;

class DocLib {
  private static function rrmdir($dir){
    if (is_dir($dir)){
      $objects = scandir($dir);
      foreach ($objects as $object) {
        if ($object != '.' && $object != '..') {
          if (filetype($dir.'/'.$object) == 'dir') {
            self::rrmdir($dir.'/'.$object);
          } else {
            unlink($dir.'/'.$object);
          }
        }
      }
      reset($objects);
      rmdir($dir);
    }
  }

  public static function docx_to_html($docx_path) {
    $zip = new \ZipArchive();
    $res = $zip->open($docx_path);
    $dir = '/tmp/doc_extract/'.md5($docx_path.time());
    if ($res === TRUE) {
        $zip->extractTo($dir);
        $zip->close();
      } else {
        return false;
    }
    $xmlfile = file_get_contents($dir.'/word/document.xml');
    $dom = new \DOMDocument();
    $dom->loadXML($xmlfile, LIBXML_NOENT | LIBXML_DTDLOAD);
    $xml = simplexml_import_dom($dom);
    $c = "";
    foreach($xml->children('w',true)->body->p as $data) {
      $type = $data->pPr->pStyle['val'];
      $content = htmlspecialchars($data->r->t,ENT_QUOTES);
      switch($type) {
        case "Title":
          $tag = 'h1';
          break;
        case "Subtitle":
          $tag = 'h2';
          break;
        case "Heading1":
          $tag = 'h1';
          break;
        case "Heading2":
          $tag = 'h2';
          break;
        case "Heading3":
          $tag = 'h3';
          break;
        default:
          $tag = 'p';
          break;
    }
    $c .= '<'.$tag.'>'.$content.'</'.$tag.'>';
  }
  $html = '<!DOCTYPE html><html lang="en" dir="ltr"><head><meta charset="utf-8"><title></title><style type="text/css">.break-word{word-wrap: break-word;}</style></head><body><div class="break-word">'.$c.'</div></body></html>';
  self::rrmdir($dir);
  return $html;
  }
}
?>
