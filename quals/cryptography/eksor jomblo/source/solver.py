r = open('cipher.txt', 'r')
cip = r.read()
r.close()


flag = ''
for i in range(256):
	for j in cip:
		flag += chr(i ^ ord(j))
	if 'HackFest' in flag:
		print flag
		exit()
	flag = ''
