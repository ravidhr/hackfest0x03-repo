<?php

    if(array_key_exists('submit', $_POST)) {
        $username = addslashes($_POST['username']);
        $password = md5($_POST['password']  .  'Salt_Terkuat_Sepanjang_Sejarah_Peradaban_Dunia_Hacker');
        $full_name = addslashes($_POST['full_name']);
        $result = $conn->query("SELECT 1 FROM tbl_users WHERE username = '{$username}'");
        if($result->num_rows === 0) {
            $result = $conn->query("INSERT INTO tbl_users VALUES('', '{$full_name}', '{$username}', '{$password}', 'user')");
            if($result) {
                echo '<script>alert("Daftar Akun Berhasil");document.location.href="masuk"</script>';
            } else {
                echo '<script>alert("Daftar Akun Gagal");history.back();</script>';
            }
        } else {
            echo '<script>alert("Username Telah Terdaftar");history.back();</script>';
        }
    }

?>
<div class="card mt-5 mx-auto" style="width: 38rem;">
    <div class="card-header text-center">
        <h3>Daftar</h3>
    </div>
    <div class="card-body">
        <form action="" method="POST">
            <div class="form-group">
                <label for="full_name">Nama Lengkap</label>
                <input type="text" class="form-control" name="full.name" id="full_name">
            </div>
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" name="username" id="username">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" id="password">
            </div>
            <button type="submit" name="submit" class="btn btn-primary btn-lg btn-block">Daftar</button>
        </form>
        <p class="mt-3">Sudah mempunyai akun ? <a href="masuk">Masuk</a></p>
    </div>
</div>