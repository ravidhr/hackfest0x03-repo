# Gore

    Serem gan

# Solusi

ketika dicoba input, dengan nilai tertentu, terlihat kunci akan di kurang dengan `2`, namun ketika di cek pseudo codenya
terlihat seperti berikut pada function `main_main`:

     fmt_Fprint(a1, a2, (__int64)&v30, (__int64)&unk_4ACA40, v2, v3, (__int64)&go_itab__os_File_io_Writer, os_Stdout);
    *(_QWORD *)&v29 = &unk_4A6D20;
    *((_QWORD *)&v29 + 1) = input_kita;
    fmt_Fscanf(
      a1,
      a2,
      (__int64)&go_itab__os_File_io_Reader,
      (__int64)&v29,
      v4,
      v5,
      (__int64)&go_itab__os_File_io_Reader,
      os_Stdin,
      (__int64)&unk_4D08A9,
      2LL);
    v6 = *input_kita - 2LL;

dapat terlihat input kita benar di kurang dengan `2` lalu dimasukan ke variabel `v6`, selanjutnya lihat pada bagian 

    while ( result < 35 )
     {
       v13 = v11;
       v14 = result;
       v15 = v12;
       v16 = *input_kita + flag_encrypted[result];
       *(_QWORD *)&v17 = ((signed __int64)(v16
                                         + ((unsigned __int128)(v16 * (signed __int128)(signed __int64)0x8080808080808081LL) >> 64)) >> 7)
                       - (v16 >> 63);
       *((_QWORD *)&v17 + 1) = v17;
       *(_QWORD *)&v17 = 255 * v17;

terlihat variable `input_kita` yang malah digunakan untuk di tambah dengan variable `flag_encrypted` bukan variabel `v6`, untuk menyelesaikan soal ini peserta hanya perlu melakukan brute untuk mendapatkan flag, kunci yang benar adalah: `24`

    kunci : 24
    kunci fix:  22
    h
    ha
    hac
    hack
    hackf
    hackfe
    hackfes
    hackfest
    hackfest{
    hackfest{r
    hackfest{re
    hackfest{rev
    hackfest{reve
    hackfest{rever
    hackfest{revers
    hackfest{reverse
    hackfest{reverse_
    hackfest{reverse_e
    hackfest{reverse_en
    hackfest{reverse_eng
    hackfest{reverse_engi
    hackfest{reverse_engin
    hackfest{reverse_engine
    hackfest{reverse_enginee
    hackfest{reverse_engineer
    hackfest{reverse_engineeri
    hackfest{reverse_engineerin
    hackfest{reverse_engineering
    hackfest{reverse_engineering_
    hackfest{reverse_engineering_9
    hackfest{reverse_engineering_9l
    hackfest{reverse_engineering_9la
    hackfest{reverse_engineering_9lan
    hackfest{reverse_engineering_9lang
    hackfest{reverse_engineering_9lang}

FLAG: hackfest{reverse_engineering_9lang}