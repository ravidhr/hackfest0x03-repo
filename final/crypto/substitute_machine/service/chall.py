from __future__ import print_function
from flag import flag
import sys
import string
import random

class Unbuffered(object):
    def __init__(self, stream):
        self.stream = stream
    def write(self, data):
        self.stream.write(data)
        self.stream.flush()
    def writelines(self, datas):
        self.stream.writelines(datas)
        self.stream.flush()
    def __getattr__(self, attr):
        return getattr(self.stream, attr)

sys.stdout = Unbuffered(sys.stdout)

class Encryption:
    def __init__(self):
        self.karakter = list(string.digits + string.ascii_letters + string.punctuation)
        self.acak = list(string.digits + string.ascii_letters + string.punctuation)
        self.block_acak = range(8)
        self.key = ""
        while len(self.key) != 8:
            char = random.choice(self.karakter)
            if char not in self.key:
                self.key += char
        random.shuffle(self.acak)
        random.shuffle(self.block_acak)

    def padding(self,str):
        res = str
        while len(res) % 8 != 0:
            res += "+"
        return res

    def encrypt(self,msg):
        msg = self.padding(msg)
        res = ""
        num = 0
        for x in range(0,len(msg),8):
            block = msg[x:x+8]
            tmp = ""
            for y in range(len(block)):
                pos = ( self.karakter.index(block[y]) + self.karakter.index(self.key[y]) + num) % len(self.karakter)
                tmp += self.acak[pos]
            block_enc = ""
            for z in range(len(self.block_acak)):
                pos = self.block_acak[z]
                block_enc += tmp[pos]
            res += block_enc
            num += 1
        return res

def main():

    soal = Encryption()
    try:
        while True:
            print("""
        ------------------------------
        Mesin Substitusi HackFest 2020
        ------------------------------
        [e] Enkripsi pesan
        [g] Enkripsi flag
        [q] Keluar
        """)
            print("[?] Pilih menu : ",end='')
            op = raw_input()
            print("")
            if op == "e":
                is_ok = True
                print("[?] Pesan : ",end='')
                pesan = raw_input()
                for x in range(len(pesan)):
                    if pesan[x] not in (string.digits + string.ascii_letters + string.punctuation):
                        is_ok = False
                        break
                if is_ok:
                    enc = soal.encrypt(pesan)
                    print("[+] Hasil Enkripsi : {}\n".format(enc))
                else:
                    print("[x] Pesan hanya boleh mengandung {}\n".format(string.digits + string.ascii_letters + string.punctuation))
            elif op == "g":
                enc = soal.encrypt(flag)
                print("[+] Flag Terenkripsi : {}\n".format(enc))
            elif op == "q":
                break
            else:
                print("[x] Pilihan tidak ditemukan\n")
    except:
        exit()

if __name__ == "__main__":
    main()
