/* Chall	: kaisar_rev
 * Author	: insomn14
 * Compile	: gcc kaisar_rev.c -Wall -o kaisar_rev -s
 * Flag		: TIFEST{7im3_2_C43s4R_sH1f7_Sub7i7ut10n}
 */

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int enkripsi_c(char* a1, int a2){
	int i;
	int v4 = strlen(a1);
	for (i=0; i < v4; i++){
		if (*(a1+i) > 47 && *(a1+i) <= 57)
			*(a1+i) += a2;
	}
	return *a1;
}

unsigned int enkripsi_k(char *a1, int a2)
{
	for(int i=0; i <= strlen(a1) ; i+=3)
	{
		if (*(a1+i) > 96 && *(a1+i) <= 122)	// a-z
			*(a1+i) = (*(a1+i) + 84 - a2) % 26 + 97;
		if (*(a1+i) > 64 && *(a1+i) <= 90) // A-Z
			*(a1+i) = (*(a1+i) + 52 - a2) % 26 + 65;
	}
	return *a1;
}

unsigned int enkripsi(char *a1, int a2)
{
	char dest[0x64];
	int v4 = strlen(a1);
	strncpy(dest, a1, 0x64);
	for(int i=0; i < v4; ++i)
		*(a1+i) = dest[(i+a2)%v4];
	return enkripsi_k(a1, a2);
}

void checkEnc(char *a1, char *a2, char *a3)
{
	if (!strcmp(a1, a2))
		printf("Correct!\nFLAG : TIFEST{%s}\n", a3);
	else
		puts("Incorrect!..\n");
}


int main()
{
	int v2, v6;
	char src[0x64], dest[0x64];
	FILE *stream;
	size_t n = 0x64;
	char input[0x64];
	char tmp[32], M[5];
	time_t current_time;

	setbuf(stdout, 0);
	current_time = time(NULL);
	struct tm *tm = localtime(&current_time);
	strftime(tmp, sizeof(tmp), "[ %d-%m-%Y | %H:%M:%S%Z ]", tm);
	strftime(M, 5, "%M", tm);
	v2 = atoi(M);
	v6 = v2 % 6 + 3;
	stream = fopen("flag.txt", "r");
	if (!stream)
	{
		printf("[x] File flag.txt dosen't exist\n");
		exit(1);
	}
	fgets(src, n, stream);
	fclose(stream);

	for (int i=0; src[i] != '\x00'; i++)
		if (src[i] == '\n')
			src[i] = '\x00';

	strncpy(dest, src, 0x64);
	enkripsi(dest, v6);
	enkripsi_c(dest, v6);
	
	printf("%s\n", tmp);
	printf("Encryption message : %s\n", dest);
	printf("Decryption message : ");
	scanf("%s", input);

	for (int i=0; input[i] != '\x00'; i++)
		if (input[i] == '\n')
			input[i] = '\x00';

	enkripsi(input, v6);
	enkripsi_c(input, v6);
	checkEnc(input, dest, src);

	return 0;
}
