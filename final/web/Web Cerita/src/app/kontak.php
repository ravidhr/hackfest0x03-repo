<?php

    if(array_key_exists('submit', $_POST)) {
        $cerita = addslashes($_POST['cerita']);
        // Meriksa apakan payload yang dikirim berbentuk tag script atau tag biasa yang berisi event handler
        if(preg_match('/on*/i', $cerita) || preg_match('/<\/?script>/i', $cerita)) {
            $ip = $_SERVER['REMOTE_ADDR'];
            if(!($conn->query("INSERT INTO tbl_contact VALUES('', '{$cerita}')") && $conn->query("INSERT INTO tbl_contact_bkp VALUES('', '{$ip}', '{$cerita}')"))) {
                echo '<script>alert("Pesan Gagal Dikirim");history.back();</script>';
                exit;
            }
        }
        echo '<script>alert("Pesan Berhasil Dikirim");history.back();</script>';
    }

?>
<h3>Kontak</h3>
<p>Mengalami kendala? silahkan beri tahu admin!</p>
<form action="" method="post">
    <label for="cerita">Pesan</label>
    <textarea name="cerita" id="cerita" cols="30" rows="10"></textarea>
    <button type="submit" name="submit" class="btn btn-primary">Kirim</button>
</form>