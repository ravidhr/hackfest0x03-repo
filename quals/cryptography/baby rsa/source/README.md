# baby rsa

	ezet lah ini

## solusi

biasa, nyari p dan q dulu, setelah itu nyari phi, lalu nyari d.. Setelah didapatkan, tinggal decrypt, angka hasil decryptnya bukan didecode secara hex atau yg serupa tetapi di chr-kan setiap 3 digit.. Oh iya, p dan q nya bisa didapatkan di factordb.com atau bisa juga menggunakan faktorisasi fermat karena berhubung nilai p dan q berdekatan

	from Crypto.Util.number import inverse

	n = 71641831546926719303369645296528546480083425905458247405279061196214424558100678947996271179659761521775290973790597533683668081173314940392098256721488468660504161994357
	e = 65537
	c = 49708841476258024096100361372702768007066231545086745820764515223838040926751509788788858534225656445293607314337278260448196826224407742137958482234450460871977850778468


	p = 8464149782874043593254414191179506861158311266932799636000173971661904149225893113311
	q = 8464149782874043593254414191179506861158311266932799636000173971661904149225893113387

	phi = (p-1) * (q-1)
	d = inverse(e, phi)

	m = pow(c, d, n)
	m = '0' + str(m)

	flag = ''
	for i in range(0, len(m), 3):
		flag += chr(int(m[i:i+3]))
	print flag

FLAG : HackFest{jUsT_a_BaSiC_r5a_cH4ll3nGe}