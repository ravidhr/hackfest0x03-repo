from __future__ import print_function
from Crypto import Random
from Crypto.Util.number import *
from flag import flag
import time
import sys

class Unbuffered(object):
    def __init__(self, stream):
        self.stream = stream
    def write(self, data):
        self.stream.write(data)
        self.stream.flush()
    def writelines(self, datas):
        self.stream.writelines(datas)
        self.stream.flush()
    def __getattr__(self, attr):
        return getattr(self.stream, attr)

sys.stdout = Unbuffered(sys.stdout)

def main():
    p = getStrongPrime(1024)
    q = getStrongPrime(1024)
    #r = int(Random.new().read(4).encode('hex'),16)
    a = int(Random.new().read(4).encode('hex'),16)
    b = int(Random.new().read(4).encode('hex'),16)
    n = p * q
    #s = (p + r) * (q + r)
    s = (p + a) * (q + b)
    e = 65537
    m = int(flag.encode('hex'),16)
    c = pow(m,e,n)
    print("""
    ---------------------
    Welcome to Random RSA
    ---------------------
    m = int(flag.encode('hex'),16)
    n = p * q
    s = (p + {}) * (q + {})
    e = {}
    c = pow(m,e,n)
    """.format(hex(a),hex(b),hex(e)))
    time.sleep(2)
    print("""
    ------------------------------
    Find m, the m value is the flag
    ------------------------------
    n = {}
    s = {}
    c = {}
    """.format(hex(n),hex(s),hex(c)))


if __name__ == "__main__":
    main()
