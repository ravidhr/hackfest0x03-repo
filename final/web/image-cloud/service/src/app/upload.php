<?php
if(isset($_POST['submit'])) {
  $check = getimagesize($_FILES['file']['tmp_name']);
  if($check) {
    $ext = pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION);
    if($ext === 'jpg' || $ext === 'png') {
      $target_dir = 'uploads/';
      $target_file = $target_dir.sha1($_FILES['file']['name'].$_FILES['file']['tmp_name']).'.'.$ext;
      if(move_uploaded_file($_FILES['file']['tmp_name'],$target_file)) {
        $filename = sha1($_FILES['file']['name'].$_FILES['file']['tmp_name']).'.'.$ext;
        $_SESSION['filename'] = $filename;
        header('Location: index.php?section=image');
        exit;
      } else {
        $error = 'Something went wrong';
      }
    } else {
      $error = 'Only PNG and JPG are allowed';
    }
  } else {
    $error = 'Only PNG and JPG are allowed';
  }
}
?>
<h1 class="display-5">Upload Image</h1>
  <p class="lead">What are you waiting for ? Lets store your image in our service.</p>
  <hr class="my-4">
  <form action="" method="post" enctype="multipart/form-data">
    <div class="form-group">
      <input type="file" class="form-control-file" name="file">
    </div>
  <input type="submit" name="submit" value="Upload" class="btn btn-success btn-lg btn-block">
  </form>
  <?php
  if(isset($error)) {
    ?>
    <br>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <strong>Error!</strong>
      <?php echo $error;?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php
  }
  ?>
