<?php

    error_reporting(0);
    if(isset($_COOKIE['auth'])) {
        $cookie = json_decode(base64_decode($_COOKIE['auth']), TRUE);
        if(isset($cookie['user']) && $cookie['role'] === 'admin') {
            if($cookie['hash'] === md5('admin')) {
                die('hackfest{sama_kaya_sebelumnya_hanya_aja_ditambah_verifikasi_dari_role}');
            } else {
                die('Signature hash dengan role tidak sama');
            }
        } else {
            die('Role selain admin dilarang akses!');
        }
    } else {
        http_response_code(406);
    }