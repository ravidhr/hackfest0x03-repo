<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libs\DocLib;
use PDF;

class DocxConverter extends Controller
{
    public function index() {
      return view('welcome',['show_error'=>false]);
    }

    public function process(Request $request) {
      $request->validate([
        'file' => 'required|mimetypes:application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.wordprocessingml.documentapplication/vnd.openxmlformats-officedocument.wordprocessingml.document',
      ]);
      $file = $request->file('file');
      $dest_dir = '/tmp/docx';
      if($file->getClientOriginalExtension() === 'docx') {
        $filename = md5($file->getClientOriginalName().time());
        $file->move($dest_dir,$filename);
        $docx_path = '/tmp/docx/'.$filename;
        $html = DocLib::docx_to_html($docx_path);
        $pdf = \App::make('snappy.pdf.wrapper');
        $pdf->loadHTML($html)->setPaper('a4');
        //$pdf = \App::make('dompdf.wrapper');
        unlink($docx_path);
        //$pdf->loadHTML($html);
        //return $pdf->stream();
        return $pdf->inline();
      } else {
        return view('welcome',['show_error'=>true]);
      }
    }
}
