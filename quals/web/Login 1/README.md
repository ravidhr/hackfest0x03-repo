# Login 1

# Deskripsi

    Kalau kamu tidak mengetahui kunci bagaimana caranya kamu masuk?

# Solusi

Challenge ini inteded solutionnya adalah Bypass Login dengan Payload SQL Injection karena query yang berada di server tidak difilter maka ketika peserta mengirimkan payload apa saja asalkan mengembalikan nilai TRUE dan nilai baris hasil querynya sebanyak 1 maka flag akan muncul

    ' OR 1=1 LIMIT 1# 

flag : hackfest{dobrakk_paksa_adalah_cara_masuknya}
