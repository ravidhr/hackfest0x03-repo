from flask import Flask, request, jsonify, make_response, render_template
import sqlite3 as sql
import bcrypt

app = Flask(__name__)

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/api/login',methods=['POST'])
def login():
    if not request.json:
        result = {'message':'Please fill username and password argument'}
        return make_response(jsonify(result),403)
    data = request.json
    if 'username' not in data or 'password' not in data:
        result = {'message':'Please fill username and password argument'}
        return make_response(jsonify(result),403)
    username = data['username']
    password = data['password'].encode('utf-8')
    query = 'SELECT password FROM user WHERE username = "{}"'.format(username)
    con = sql.connect("database.db")
    con.row_factory = sql.Row
    cur = con.cursor()
    cur.execute(query)
    row = cur.fetchone()
    if row:
        hash_password = row['password'].encode('utf-8')
        if bcrypt.checkpw(password,hash_password):
            result = {'message':'Logged in','status_code':200}
            return make_response(jsonify(result),200)
        else:
            result = {'message':'Incorrect password','status_code':403}
            return make_response(jsonify(result),403)
    else:
        result = {'message':'Username not found','status_code':403}
        return make_response(jsonify(result),403)

if __name__ == "__main__":
    app.run(host='0.0.0.0',port=39200)
