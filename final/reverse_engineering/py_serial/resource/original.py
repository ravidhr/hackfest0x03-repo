import sys

used = []
key = '\xd0\xe3\x7f\xb3\xa8=\xf9\xcdR\x16"W\x1b4\x0b<'
def decrypt(str):
    res = ""
    for x in range(len(str)):
        res += chr(ord(str[x]) ^ ord(key[x % len(key)]))
    return res

def print_(s,dec=True):
    if dec:
        sys.stdout.write(decrypt(s))
    else:
        sys.stdout.write(s)
    sys.stdout.flush()

#def print_(s):
#    print_(s)

def check_serial(serial):
    a = [chr(x) for x in range(48,58)]
    b = [chr(x) for x in range(97,123)]
    c = [chr(x) for x in range(65,91)]
    d = a + b + c
    e = []
    f = 0
    g = 0
    h = 0
    if "-" not in serial:
        return False
    raw = serial.split("-")
    if len(raw) != 5:
        return False
    for rawx in raw:
        if len(rawx) != 5:
            return False
    for x in range(len(raw)):
        for y in range(len(raw[x])):
            if raw[x][y] in e:
                return False
            else:
                e.append(raw[x][y])
    for x in range(len(raw)):
        for y in range(len(raw[x])):
            if raw[x][y] not in d:
                return False
            else:
                if raw[x][y] in a:
                    f += 1
                elif raw[x][y] in b:
                    g += 1
                elif raw[x][y] in c:
                    h += 1
    if f < 5 or g < 6 or h < 7:
        return False

    return True

def main():
    print_("\x83\x86\x13\xd2\xc5\x5c\x8d\xed\x36\x77\x56\x36\x75\x53\x01")
    print_("\x9d\x82\x0c\xc6\xc3\x5c\x97\xed\x21\x73\x50\x3e\x7a\x58\x2b\x45\xb1\x8d\x18\x93\xca\x58\x97\xac\x20\x36\x51\x32\x79\x55\x65\x45\xb1\x88\x5f\x81\x98\x1d\x92\xac\x3e\x7f\x02\x22\x75\x40\x7e\x57\xf0\x8e\x1a\xdd\xcc\x5c\x89\xac\x26\x7d\x43\x39\x3b\x52\x67\x5d\xb7\xe9\x75")
    #exit()
    for x in range(20):
        step = decrypt("\x8b\xdc\x22\x93\xfb\x58\x8b\xa4\x33\x7a\x02\x3c\x7e\x14\x26\x1c\xab\x9e\x5f\x89\x88")
        print_(step.format(x+1),False)
        serial = raw_input("")
        if check_serial(serial) == True:
            if serial not in used:
                print_("\x8b\xc8\x22\x93\xfb\x58\x8b\xa4\x33\x7a\x02\x39\x6e\x59\x69\x59\xa2\xc3\x1d\xd6\xc6\x5c\x8b\xc7")
                used.append(serial)
                continue
            else:
                print_("\x8b\x9b\x22\x93\xfb\x58\x8b\xa4\x33\x7a\x02\x39\x6e\x59\x69\x59\xa2\xc3\x0f\xd6\xda\x53\x98\xa5\x72\x72\x4b\x30\x6e\x5a\x6a\x57\xb1\x8d\x5f\xc0\xcd\x5f\x9c\xa1\x27\x7b\x4c\x2e\x7a\x3e")
                sys.exit(1)
        else:
            print_("\x8b\xce\x22\x93\xfb\x58\x8b\xa4\x33\x7a\x02\x39\x6e\x59\x69\x59\xa2\xc3\x0c\xd2\xc4\x5c\x91\xc7")
            sys.exit(1)

    flag = open("flag.txt","rb").read()
    print_(flag,False)

if __name__ == "__main__":
    main()
