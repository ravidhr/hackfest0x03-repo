# play2win

## Solution

Terdapat dua cara untuk menyelesaikan tantangan ini.
Cara pertama :
Extract play2win.apk menggunakan 7z, setelah itu buka file classes.dex menggunakan *Dex to Java decompiler* untuk melihat source code dari program tersebut. Seperti yang terlihat pada SkorActivity terdapat kondisi jika `this.totalMatch >= this.TOTAL_MATCH` dan `this.userWin >= this.WINNER_SCORE || this.userLose <= 0` dimana TOTAL_MATCH dan WINNER_SCORE adalah 14000605. Sampai disini kita bisa mengubah TOTAL_MATCH dan WINNER_SCORE menjadi 1 menggunakan apktools.
1. Decode apk menggunakan `apktool`:
   $ apktool d play2win.apk
2. Arahkan ke directory `smali/com/example/play2win/`, lakukan perubah pada file `SkorActiviry.smail` cari nilai 14000605->0xd5a1dd ubah menjadi 1->0x1.
3. Build file apknya kembali menggunakan `apktool`:
   $ apktool b play2win -o play2win_modified.apk
4. Membuat certificate dan verify signatures menggunakan `keytool` dan `jarsigner`:
   $ keytool -genkey -v -keystore key.keystore -alias self -keyalg RSA -keysize 2048 -validity 10000
   $ jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore key.keystore play2win_modified.apk self

Cara kedua:
Seperti yang kita ketahui pada cara yang pertama, jika kita memenagkan permainan ini maka string dari flag akan tercetak, dimana string flag tersebut berada di native-lib.
1. Decode apk menggunakan `apktool`:
   $ apktool d play2win.apk
2. Arahkan ke directory `lib/x86`.
3. gunatakn perintah `rabin2` untuk mencetak string dari data section.
   $ rabin2 -z libnative-lib.so

