#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#define BLOCK_SIZE 0xff
void init(){
	setvbuf(stdout, NULL, _IONBF, 0);
}

void win(){
	puts("SIGSEGV triggered!");
	char buf[0xfff];
	FILE* f = fopen("./flag", "r");
	if (f == NULL)
	{
		puts("flag not found , ping me (arsalan) on LINE groups !!! \n");
		exit(-1);
	}
	else
	{
		fgets(buf, sizeof(buf), f);
		printf("flag: %s\n", buf);
		exit(0);
	}
}

int main(){
	init();
	signal(SIGSEGV, win);
	char bss[BLOCK_SIZE]; 
	printf("buffer[%d] >> ",BLOCK_SIZE);
	gets(bss);
	return 0;
}
