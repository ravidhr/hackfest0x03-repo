#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <seccomp.h>
#include <sys/mman.h>
//gcc chall.c -o chall -m64 -z execstack -fstack-protector-all -lseccomp -pie -g -O0 -Wl,-z,relro,-z,now
void init() {
	setvbuf(stdout, 0 , 2 , 0);
	setvbuf(stdin, 0 , 2 , 0);
	scmp_filter_ctx ctx = seccomp_init(SCMP_ACT_ALLOW);
	seccomp_rule_add(ctx,SCMP_ACT_KILL,SCMP_SYS(execve),0);
	seccomp_rule_add(ctx,SCMP_ACT_KILL,SCMP_SYS(execveat),0);
	seccomp_load(ctx);
}

int main() {
  init();
  char shellcode[0x100];
  memset(shellcode,0,0x100);
  puts("--- Give me your hellcode here ---");
  read(0,shellcode,9);
  int (*ret)() = (int(*)())shellcode;
  ret();
	exit(0);
}
