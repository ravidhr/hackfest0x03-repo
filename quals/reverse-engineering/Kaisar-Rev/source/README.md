# kaisar_rev

## Solution

Pada soal ini kita diberikan tantangan yang mana kita diminta dekripsi flagnya, dan setipa menitnya enkirpsi flag akan berubah-ubah.
Berikut ini adalah pseudocode main program tersebut.
```
[0x00001220]> pdg@main
...
...
    size = 100;
    timer = sym.imp.time(0);
    timeptr = sym.imp.localtime(&timer);
    sym.imp.strftime(&s, 0x20, "[ %d-%m-%Y | %H:%M:%S%Z ]", timeptr);           // setup waktu
    sym.imp.strftime(str, 5, 0x204c);                                           // 0x204c -> '%M' -> Menit
    iVar1 = sym.imp.atoi(str);                                                  // convert Menti ke Integer
    uVar2 = iVar1 % 6 + 3;                                                      // uVar2 = Menit % 6 + 3
    stream = sym.imp.fopen(0x2051, 0x204f);                                     // 0x2051 -> 'flag.txt', 0x204f -> 'r'; membaca file flag.txt
    if (stream == 0) {                                                          // check file flag.txt
        sym.imp.puts("[x] File flag.txt dosen\'t exist");
    // WARNING: Subroutine does not return
        sym.imp.exit(1);
    }
    sym.imp.fgets(&src, size & 0xffffffffU, stream, size & 0xffffffffU);        // memindahkan content stream ke src
    sym.imp.fclose(stream);
    var_1a8h._0_4_ = 0;
    while (*(char *)((int64_t)&src + (int64_t)(int32_t)var_1a8h) != '\0') {     // menghapus newline
        if (*(char *)((int64_t)&src + (int64_t)(int32_t)var_1a8h) == '\n') {
            *(undefined *)((int64_t)&src + (int64_t)(int32_t)var_1a8h) = 0;
        }
        var_1a8h._0_4_ = (int32_t)var_1a8h + 1;
    }
```

Pada bagian berikut ini variabel 'dest' mengcopy nilai dari 'src'. Nilai dari 'dest' dan 'uVar2' dikrim sebagai parameter pada fungsi `fcn.000014d9`.
`fcn.000014d9` berfungsi untuk menggeser posisi setipa karakter yang adalah pada 'arg1'. 

```
...
    sym.imp.strncpy(&dest, &src, 100);
    fcn.000014d9((char *)&dest, (uint64_t)uVar2);
...

[0x00001220]> pdg @fcn.000014d9
void fcn.000014d9(char *arg1, int64_t arg2)
{
    ...
    var_74h._0_4_ = sym.imp.strlen(arg1);                                                               // var_74h._0_4_ -> var_74h[4] = strlen(arg1)
    sym.imp.strncpy((int64_t)&var_74h + 4, arg1, 100);                                                  // arg1 -> var_74h
    i = 0;
    while (i < (int32_t)var_74h) {
        arg1[i] = *(char *)((int64_t)&var_74h + (int64_t)(((int32_t)arg2 + i) % (int32_t)var_74h) + 4); // arg1[i] = var_74h[(arg2 + i) % var_74h._0_4_]
        i = i + 1;
    }
    fcn.0000139d(arg1, arg2 & 0xffffffff);
    ...
}
```

Setelah melakukan pergeseran setipa karakter, terdapat pemanggilan fungsi `fcn.0000139d`, yang mana fungsi `fcn.0000139d` Ini adalah bentuk dari fungsi Caesar Cipher yang biasa kita temui dalam tantangan CTF pada umumnya. Dalam hal ini Caesar Cipher terlihat sangat 'matematis'. [**How to encrypt using Caesar cipher?**](https://www.dcode.fr/caesar-cipher#q1)

```
[0x00001220]> pdg @fcn.0000139d

int32_t fcn.0000139d(char *arg1, int64_t arg2)
{
    ...    
    var_14h._0_4_ = 0;
    while( true ) {
        uVar2 = sym.imp.strlen(arg1);
        if (uVar2 < (uint64_t)(int64_t)(int32_t)var_14h) break;
        if (('`' < arg1[(int32_t)var_14h]) && (arg1[(int32_t)var_14h] < '{')) {         // pergeseran alfabet lowercase a - z
            iVar1 = (arg1[(int32_t)var_14h] + 0x54) - (int32_t)arg2;                    // setiap karakter disubtraction dengan arg2
            arg1[(int32_t)var_14h] = (char)iVar1 + (char)(iVar1 / 0x1a) * -0x1a + 'a';
        }
        if (('@' < arg1[(int32_t)var_14h]) && (arg1[(int32_t)var_14h] < '[')) {         // pergeseran alfabet uppercase A - Z
            iVar1 = (arg1[(int32_t)var_14h] + 0x34) - (int32_t)arg2;                    // setiap karakter disubtraction dengan arg2
            arg1[(int32_t)var_14h] = (char)iVar1 + (char)(iVar1 / 0x1a) * -0x1a + 'A';
        }
        var_14h._0_4_ = (int32_t)var_14h + 3;
    }
    return (int32_t)*arg1;
}
```

Setelah melakukan pergeseran dan substitution, selanjutnya terdapat fungsi `fcn.00001309`. Setipa karakter numerik yang ada pada 'arg1' akan di subtitusi dengan arg2.

```
[0x00001220]> pdg @main
    ...
    ...
    fcn.00001309((char *)&dest, (uint64_t)uVar2);
    ...
    ...

[0x00001220]> pdg @fcn.00001309
int32_t fcn.00001309(char *arg1, int64_t arg2)
{
    int32_t iVar1;
    char *s;
    int32_t var_8h;
    undefined8 var_4h;
    
    iVar1 = sym.imp.strlen(arg1);
    var_8h = 0;
    while (var_8h < iVar1) {
        if (('/' < arg1[var_8h]) && (arg1[var_8h] < ':')) {                 // 0 - 9
            arg1[var_8h] = arg1[var_8h] + (char)arg2;                       // arg2 -> key 
        }
        var_8h = var_8h + 1;
    }
    return (int32_t)*arg1;
}
```

Selajutnya progam akan mencetak waktu dan menampilkan flag yang sudah di enkripsi sebelumnya, kemudian program akan meminta inputan dekripsi dari flag, setelahnya inputan kita akan dienkripsi sama seperti flag. 
Terdapat fungsi `fcn.0000159e` yang mana hasil enkripsi flag dan inputtan kita akan dibandingkan.

```
[0x00001220]> pdg @main
    ...
    sym.imp.puts(&s);                                                               // cetak waktu "[ %d-%m-%Y | %H:%M:%S%Z ]"
    sym.imp.printf("Encryption message : %s\n", &dest);                             // cetak flag setelah di enkripsi
    sym.imp.printf("Decryption message : ");                                        
    sym.imp.__isoc99_scanf(0x20af);                                                 // inputan kita
    var_1a8h._4_4_ = 0;
    while (*(char *)((int64_t)&var_70h + (int64_t)var_1a8h._4_4_) != '\0') {        // remove newline
        if (*(char *)((int64_t)&var_70h + (int64_t)var_1a8h._4_4_) == '\n') {
            *(undefined *)((int64_t)&var_70h + (int64_t)var_1a8h._4_4_) = 0;
        }
        var_1a8h._4_4_ = var_1a8h._4_4_ + 1;
    }
    fcn.000014d9((char *)&var_70h, (uint64_t)uVar2);
    fcn.00001309((char *)&var_70h, (uint64_t)uVar2);
    fcn.0000159e((char *)&var_70h, (char *)&dest, (char *)&src);                    // membandingkan inputtan kita dengan flag setlah di enkripsi
    ...

[0x00001220]> pdg @fcn.0000159e

void fcn.0000159e(char *arg1, char *arg2, char *arg3)
{
    int32_t iVar1;
    char *var_18h;
    char *s2;
    char *s1;
    
    iVar1 = sym.imp.strcmp(arg1, arg2, arg2);                                       // compare string
    if (iVar1 == 0) {
        sym.imp.printf("Correct!\nFLAG : TIFEST{%s}\n", arg3);
    } else {
        sym.imp.puts("Incorrect!..\n");
    }
    return;
}
```

Berikut ini adalah kode solverny
Enkripsi : shift_char(kiri) -> Caesar_cipher_Subtitution(-) -> Numerik_karakter_Subtitution(+)
Dekripsi : Numerik_karakter_Subtitution(-) -> Caesar_cipher_Subtitution(+) -> shift_char(kanan)
```
#!/usr/bin/env python3
import numpy as np

def dec3(a1, a2):
    a1 = bytearray(a1)
    v4 = len(a1)
    for i in range(v4):
        if a1[i] > 47 and a1[i] <= 66:
            a1[i] -= a2
        i += 1
    return a1

def dec2(a1, a2):
    a1 = bytearray(a1)
    i = 0
    while(i < len(a1)):
        if a1[i] > 96 and a1[i] <= 122:
            a1[i] = (a1[i] - 44 + a2) % 26 + 97
        if a1[i] > 64 and a1[i] <= 90:
            a1[i] = (a1[i] - 52 + a2) % 26 + 65
        i+=3
    return a1

dec1 = lambda s,m: ''.join(chr(i) for i in np.roll(s,m))

mnt = int(input("Menit : ")) % 6 + 3
inp = str(input("Cipher : ")).encode()
s = dec1( dec2( dec3(inp, mnt), mnt), mnt)
print(f"Decrypt Flag : {s}")

```

PoC
```
❯ ./kaisar_rev
[ 29-08-2020 | 13:47:33WIB ]
Encryption message : <;s<R_jH9w?_Xub?i?lt98n?zm;_:_H
Decryption message : 

----------------
❯ python3 solver.py
Menit : 47                             
Cipher : <;s<R_jH9w?_Xub?i?lt98n?zm;_:_H
Decrypt Flag : 7im3_2_C43s4R_sH1f7_Sub7i7ut10n

----------------
[ 29-08-2020 | 13:47:33WIB ]
Encryption message : <;s<R_jH9w?_Xub?i?lt98n?zm;_:_H
Decryption message : 7im3_2_C43s4R_sH1f7_Sub7i7ut10n
Correct!
FLAG : TIFEST{7im3_2_C43s4R_sH1f7_Sub7i7ut10n}
```