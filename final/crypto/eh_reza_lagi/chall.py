from Crypto.Util.number import *
from sympy import *
import random
import hashlib
import string
import time


class Unbuffered(object):
    def __init__(self, stream):
        self.stream = stream
    def write(self, data):
        self.stream.write(data)
        self.stream.flush()
    def writelines(self, datas):
        self.stream.writelines(datas)
        self.stream.flush()
    def __getattr__(self, attr):
        return getattr(self.stream, attr)

sys.stdout = Unbuffered(sys.stdout)


flag = 'HackFest{AyeEe_y0u_m4d3_AnD_4ut0mAtiOn_t0_sOlv3_th1s_didNt_y0u?}'

def get_random_string(length):
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str

def fermat(): #level 3
	m = get_random_string(32)
	int_m = int(m.encode('hex'), 16)
	p = getPrime(1024)
	q = nextprime(p)
	e = 65537
	n = p*q
	c = pow(int_m, e, n)
	return c, n, e, m

def low_exponent(): #level 1
	m = get_random_string(32)
	int_m = int(m.encode('hex'), 16)
	p = getPrime(2048)
	q = getPrime(2048)
	assert p != q
	e = 3
	n = p*q
	c = pow(int_m, e, n)
	return c, n, e, m

def prime_mod(): #level 2
	m = get_random_string(32)
	int_m = int(m.encode('hex'), 16)
	p = getPrime(1024)
	e = 65537
	n = p
	c = pow(int_m, e, n)
	return (c, n, e, m)

def common_factor(): #level 4
	m = get_random_string(32)
	int_m = int(m.encode('hex'), 16)
	p = getPrime(1024)
	q = getPrime(1024)
	r = getPrime(1024)
	assert p != q
	assert p != r
	assert r != q
	n1 = p*q
	n2 = q*r
	e = 65537
	c1 = pow(int_m, e, n1)
	c2 = pow(int_m, e, n2)
	return c1, c2, n1, n2, e, m

def low_private_exponent(): #level 5
	m = get_random_string(32)
	int_m = int(m.encode('hex'), 16)
	n = 109676931776753394141394564514720734236796584022842820507613945978304098920529412415619708851314423671483225500317195833435789174491417871864260375066278885574232653256425434296113773973874542733322600365156233965235292281146938652303374751525426102732530711430473466903656428846184387282528950095967567885381
	e = 49446678600051379228760906286031155509742239832659705731559249988210578539211813543612425990507831160407165259046991194935262200565953842567148786053040450198919753834397378188932524599840027093290217612285214105791999673535556558448523448336314401414644879827127064929878383237432895170442176211946286617205
	c = pow(int_m, e, n)
	return c, n, e, m

def hastad(): #level 6
	m = get_random_string(44)
	int_m = int(m.encode('hex'), 16)
	e = 3
	n1 = 1001191535967882284769094654562963158339094991366537360172618359025855097846977704928598237040115495676223744383629803332394884046043603063054821999994629411352862317941517957323746992871914047324555019615398720677218748535278252779545622933662625193622517947605928420931496443792865516592262228294965047903627
	n2 = 405864605704280029572517043538873770190562953923346989456102827133294619540434679181357855400199671537151039095796094162418263148474324455458511633891792967156338297585653540910958574924436510557629146762715107527852413979916669819333765187674010542434580990241759130158992365304284892615408513239024879592309 
	n3 = 1204664380009414697639782865058772653140636684336678901863196025928054706723976869222235722439176825580211657044153004521482757717615318907205106770256270292154250168657084197056536811063984234635803887040926920542363612936352393496049379544437329226857538524494283148837536712608224655107228808472106636903723
	c1 = pow(int_m, 3, n1)
	c2 = pow(int_m, 3, n2)
	c3 = pow(int_m, 3, n3)
	return c1, c2, c3, n1, n2, n3, e, m

def level1():
	c, n, e, m = low_exponent()
	print """
--------
LEVEL 1
--------
n : {}
e : {}
c : {}
	""".format(n, e, c)
	waktu1 = int(time.time())
	answer = raw_input('Input the m: ')
	waktu2 = int(time.time())
	if waktu2 - waktu1 > 3:
		print "Time is up !!!!"
		exit()
	elif answer == m:
		print """
Nice !!!
		"""
		level2()
	else:
		print """
WRONG !!!
		"""
		exit()

def level2():
	c, n, e, m = prime_mod()
	print """
--------
LEVEL 2
--------
n : {}
e : {}
c : {}
	""".format(n, e, c)
	waktu1 = int(time.time())
	answer = raw_input('Input the m: ')
	waktu2 = int(time.time())
	if waktu2 - waktu1 > 3:
		print "Time is up !!!!"
		exit()
	elif answer == m:
		print """
Nice !!!
		"""
		level3()
	else:
		print """
WRONG !!!
		"""
		exit()

def level3():
	c, n, e, m = fermat()
	print """
--------
LEVEL 3
--------
n : {}
e : {}
c : {}
	""".format(n, e, c)
	waktu1 = int(time.time())
	answer = raw_input('Input the m: ')
	waktu2 = int(time.time())
	if waktu2 - waktu1 > 3:
		print "Time is up !!!!"
		exit()
	if answer == m:
		print """
Nice !!!
		"""
		level4()
	else:
		print """
WRONG !!!
		"""
		exit()

def level4():
	c1, c2, n1, n2, e, m = common_factor()
	print """
--------
LEVEL 4
--------
n1 : {}
n2 : {}
e : {}
c1 : {}
c2 : {}
	""".format(n1, n2, e, c1, c2)
	waktu1 = int(time.time())
	answer = raw_input('Input the m: ')
	waktu2 = int(time.time())
	if waktu2 - waktu1 > 3:
		print "Time is up !!!!"
		exit()
	if answer == m:
		print """
Nice !!!
		"""
		level5()
	else:
		print """
WRONG !!!
		"""
		exit()

def level5():
	c, n, e, m = low_private_exponent()
	print """
--------
LEVEL 5
--------
n : {}
e : {}
c : {}
	""".format(n, e, c)
	answer = raw_input('Input the m: ')
	if answer == m:
		print """
Nice !!!
		"""
		level6()
	else:
		print """
WRONG !!!
		"""
		exit()

def level6():
	c1, c2, c3, n1, n2, n3, e, m = hastad()
	print """
--------
LEVEL 6
--------
n1 : {}
n2 : {}
n3 : {}
e : {}
c1 : {}
c2 : {}
c3 : {}
	""".format(n1, n2, n3, e, c1, c2, c3)
	waktu1 = int(time.time())
	answer = raw_input('Input the m: ')
	waktu2 = int(time.time())
	if waktu2 - waktu1 > 3:
		print "Time is up !!!!"
		exit()
	if answer == m:
		print """
Nice !!!

Here is your flag : {}
		""".format(flag)
		exit()

	else:
		print """
WRONG !!!
		"""
		exit()





def main():
	print """
-------------------------------
FINISH LEVEL 6 TO GET THE FLAG
-------------------------------

Note : - the answer is a random printable alphanumeric (don't forget to decode the m)
Note : - every level have its own time

	"""
	a = raw_input('press enter to continue.')
	level1()

if __name__ == "__main__":
    main()
