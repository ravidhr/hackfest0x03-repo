#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void init(){
    setvbuf(stdout, 0 , 2 , 0);
	setvbuf(stdin, 0 , 2 , 0);
}

void main(){
    init();
    char shellcode[121];
    char past[20];
    char future[20];
    puts("Who controls the past controls the future.");
    puts("Who controls the present controls the past.");
    puts("Take control.");
    puts("masa depan ? ");
    read(0,future,20);

    puts("masa lalu ? ");
    read(0,past,8);

    memcpy(&shellcode,past,8);
    memcpy(&shellcode[8],future,20);

    printf("%s",shellcode);

    int(*ret)() = (int(*)())shellcode;
    ret();
}
