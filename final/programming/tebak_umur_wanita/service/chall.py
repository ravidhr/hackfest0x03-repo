import random, sys
import time

class Unbuffered(object):
    def __init__(self, stream):
        self.stream = stream
    def write(self, data):
        self.stream.write(data)
        self.stream.flush()
    def writelines(self, datas):
        self.stream.writelines(datas)
        self.stream.flush()
    def __getattr__(self, attr):
        return getattr(self.stream, attr)

sys.stdout = Unbuffered(sys.stdout)

def welcome():
    print("""
    \t---[    TEBAK UMUR WANITA    ]---\n
    Pada game ini, terdapat 100 orang wanita yang umurnya harus kamu tebak.
    Sebut umur masing-masing wanita adalah variabel X.
    X adalah bilangan yang lebih besar dari sama dengan nol dan kurang dari sama dengan seratus.
    Setiap wanita memberikan kamu kesempatan sebanyak tujuh kali untuk menebak umurnya dan kamu hanya diberi waktu 5 detik pertebakannya.
    Jika kamu berhasil menebak umur seorang wanita, maka kamu akan mendapatkan cintanya.
    Jika kamu gagal menebak umurnya lebih dari tujuh kali, maka kamu tidak akan mendapatkan cintanya.
    Tugas kamu adalah dapatkanlah cinta 100 orang wanita, maka kamu akan mendapatkan harta karun rahasia
    \n
    Press [ANY] key to continue
    """)
    raw_input()

def wanita():
    umur = random.randrange(0,100)
    for x in range(7):
        time_start = int(time.time())
        try:
            tebak = raw_input('Tebak umur aku :D ? ')
            tebak = int(tebak)
        except:
            print('APAAN SIH NIH ORANG GA JELAS. PERGI SANA LO')
            exit()
        time_end = int(time.time())
        if time_end - time_start > 5:
            print('APAAN SIH NIH ORANG LEMOT BANGET MIKIRNYA. PERGI SANA LO')
            exit()
        if tebak == umur:
            print('Wahh, kamu dukun yaaa kok bisa tau umur aku :D')
            return True
        elif tebak > umur:
            print('Aku emang keliatan tua banget ya :(')
        else:
            print('Wkwkwkw, aku emang keliatan awet muda ternyata ya :D')
    return False

def main():
    welcome()
    for x in range(100):
        print('[+] WANITA KE - {}'.format(x+1))
        step = wanita()
        if step == False:
            print('APAAN SIH NIH ORANG GA JELAS. PERGI SANA LO')
            exit()
    print('\n\nSudah saatnya kami memberikan kamu harta karun. Ini dia\n')
    print('Flag : HackFest{binary_search_itu_sangat_efektif_yaa__5c3e2403}')

if __name__ == "__main__":
    main()
