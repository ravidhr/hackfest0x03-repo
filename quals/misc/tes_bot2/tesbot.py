import time
import random
import gmpy2

flag = 'HackFest{widiihhhh_kerenn_betttt}'

print('Jawab 100 soal dalam waktu kurang dari 2 detik untuk setiap soalnya')
#print(random.getrandbits(10))
for i in range(100):
	a = random.getrandbits(10)
	b = random.getrandbits(10)
	hasil = a*b
	print('Soal ke-{}'.format(i+1))
	print('{} x {} = ?'.format(a, b))
	waktu1 = int(time.time())
	jawab = input('Jawab:')
	waktu2 = int(time.time())
	if waktu2-waktu1 <= 2:
		try:
			jawab = int(jawab)
			if jawab != hasil:
				print('salah')
				exit()
			else:
				print('benar')
				continue
		except:
			print('bye bye')
			exit()
	else:
		print('terlalu lambat gan')
		exit()

print('Mantap, ini dia flagnya:', flag)