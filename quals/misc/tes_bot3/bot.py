from Crypto.Util.number import getPrime, isPrime
import time
import random


flag = 'HackFest{bilangan_prima_dudududududu_}'

print("Jawab 10 soal dalam waktu kurang dari 2 detik setiap soalnya")
print('=====================================================')
print('')
print("Di index ke berapa, yg merupakan bilangan prima?")

for i in range(10):
	print("Soal ke-", i+1)
	a = []
	prima = getPrime(100)
	a.append(prima)

	for i in range(9):
		ramdom = random.getrandbits(100)
		if isPrime(ramdom):
			ramdom = random+1
		a.append(ramdom)

	random.shuffle(a)
	indek = a.index(prima)
	print(a)
	waktu1 = int(time.time())
	jawab = input('Jawab:')
	waktu2 = int(time.time())
	if waktu2-waktu1 <= 2:
		try:
			jawab = int(jawab)
			if jawab >= 10:
				print('index out of range, bang')
				exit()
			elif jawab == indek:
				print('benar')
				#continue
			else:
				print('salah')
				exit()
		except:
			print('bye bye')
			exit()
	else:
		print('kelamaan bang jawabnya')
		exit()

print('mantapp.... ini flagnya :', flag)