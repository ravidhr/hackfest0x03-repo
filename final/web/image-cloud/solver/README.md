# Image Cloud

## Deskripsi
Mas Ravi membuat sebuah Image Cloud service yang dapat digunakan oleh peserta HackFest untuk mengupload gambar dengan temporary storage.
Gambar yang di upload akan dihapus dalam waktu 30 menit sekali.

## Solusi

Peserta mendapatkan semua file php dengan menggunakan LFI PHP filter.

    http://ccug.my.id:61901/index.php?section=php://filter/convert.base64-encode/resource=library

Setelah menganalisa libary, terlihat bahwa terdapat PHP magic functions yaitu destruct.
Fungsi ini akan selalu dipanggil setelah kelas selesai dipanggil.

Tantangan dari sini adalah peserta harus melakukan chaining LFI to RCE dengan beberapa kesulitan yaitu
1. Peserta tidak bisa secara langsung memasukan payload RCE ke image yang diupload dikarenakan terdapat
   fungsi getimagesize() yang akan mengecek validitas gambar.
2. Seandainya image size berhasil dibypass, peserta tidak bisa langsung memanggil image menggunakan LFI
   karena setiap file yang akan diupload di append .php sebagai extensi.


Solusinya adalah menggunakan PHAR wrapper yang disisipkan object dari class ImageFile dari libary.php
Untuk melakukan bypass terhadap getimagesize(), berikut ada referensi yang bisa diikuti

    https://www.nc-lp.com/blog/disguise-phar-packages-as-images

Setelah craft payload, upload ke web tersebut dengan extensi .jpg lalu akses image seperti berikut
  http://ccug.my.id:61901/index.php?section=phar://uploads/d24daeaa772613fbbe2a1313a7cd46251c8d8af3.jpg/test.php

Dari sini, peserta sudah mendapatkan RCE

    http://ccug.my.id:61901/uploads/my_backdoor.php?a=scandir&b=/
