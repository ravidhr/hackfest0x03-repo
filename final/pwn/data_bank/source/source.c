#include <stdio.h>
#include <stdlib.h>
#include <string.h>
__asm__("xor %edx, %edx\n"
        "ret"
);

int copy_to_bank(char *(buf),char *(data_bank)){
    strcpy(data_bank, buf);
    puts("data saved");
    return 0;
}

void support(){
    __asm__("xor %edx, %edx;\n"
            "xor %ecx, %ecx;\n"
            "nop\n"
    );
}

void support2(){
    __asm__("nop\n");
}

void welcome(){
    puts("kami akan menyimpan data anda!");
    puts("1. input data");
    puts("2. copy data to data bank");
    puts("3. exit");
}

int inputData(char *(buf)){
    printf(">");    
    read(0, buf, 0xfffff);
    return 0;
}

void init(){
	setvbuf(stdout, 0 , 2 , 0);
	setvbuf(stdin, 0 , 2 , 0);
}

int main(){
    init();
    char data_bank[32];
    char buf[0xfffff];
    int pil;
    while(1){
        welcome();
        printf(">> ");  
        int x[0xf];
        read(0, x, sizeof(pil));
        pil = atoi(x);
        if(pil == 1 ){
            inputData(buf);
            continue;
        }else if( pil == 2){
            copy_to_bank(buf, data_bank);
        }else if(pil == 3){
            break;
        }else{
            puts("invalid command");
        }
    }
    return 0;
}