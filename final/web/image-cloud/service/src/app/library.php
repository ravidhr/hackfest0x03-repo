<?php
/*
In the future, i will use this class to handle upload file
*/
class ImageFile {
  protected $contents;
  protected $filename;

  public function __construct($filename,$contents) {
    if(substr($filename,-4) === '.jpg' || substr($filename,-4) === '.png') {
      $ext = substr($filename,-4);
      $this->filename = 'uploads/'.sha1($filename.time()).'.'.$ext;
      $this->contents = $contents;
    } else {
      exit;
    }
  }
  public function __destruct() {
    file_put_contents($this->filename,$this->contents);
  }
}
?>
