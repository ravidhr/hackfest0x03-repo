from __future__ import print_function
import random
import time
import sys
# ide soal ke peserta
# peserta diberikan matrix A dan matrix B dan matrix D
# matrix C valuenya harus dicari peserta
# soal : (matrix A + matrix B) * (matrix C) = matrix D

class Unbuffered(object):
    def __init__(self, stream):
        self.stream = stream
    def write(self, data):
        self.stream.write(data)
        self.stream.flush()
    def writelines(self, datas):
        self.stream.writelines(datas)
        self.stream.flush()
    def __getattr__(self, attr):
        return getattr(self.stream, attr)

sys.stdout = Unbuffered(sys.stdout)


def generate_matrix(num):
    # bentuk matrix NxN
    res = []
    for x in range(num):
        tmp = []
        for y in range(num):
            tmp.append(random.randrange(10,50))
        res.append(tmp)
    return res

def generate_x_matrix(num):
    # bentuk matrix Nx1 matrix ini adalah matrix xi-xn yang akan dicari oleh peserta
    res = []
    for x in range(num):
        res.append(random.randrange(2,15))
    return res

def matrix_addition(m1,m2):
    # ordo matrix m1 dan m2 harus sama
    m3 = []
    for x in range(len(m1)):
        tmp = []
        for y in range(len(m2)):
            num = m1[x][y] + m2[x][y]
            tmp.append(num)
        m3.append(tmp)
    return m3

def transpose_matrix(m1):
    res = []
    for x in range(len(m1)):
        tmp = []
        for y in range(len(m1)):
            tmp.append(m1[y][x])
        res.append(tmp)
    return res

def mulmatrix_with_xm(m1,mx):
    # matrix NxN dikali sama Nx1
    res = []
    for x in range(len(m1)):
        tsum = 0
        for y in range(len(m1)):
            num = m1[x][y] * mx[y]
            tsum += num
        res.append(tsum)
    return res

def generate_soal(num):
    # soalnya adalah transpose(A + B) * C = D
    A = generate_matrix(num) # A
    B = generate_matrix(num) # B
    AB = matrix_addition(A,B) # A+B
    TAB = transpose_matrix(AB) # transpose
    C = generate_x_matrix(num) # matrix yang dicari peserta
    D = mulmatrix_with_xm(TAB,C) # transpose(A+B) * C = D
    return A,B,C,D

def matrix_non_single_to_str(matrix):
    res = "\t"
    for line in matrix:
        tmp = []
        for num in line:
            tmp.append(str(num))
        res += ' '.join(tmp)
        res += "\n\t"
        #res.append(tmp)
    return res
    #print(tmp)
def matrix_single_to_str(matrix):
    res = ""
    tmp = []
    for num in matrix:
        tmp.append(str(num))
    res = "\t    " + '\n\t    '.join(tmp)
    res += "\n"
    return res

def welcome():
    print("""
    \t\t\t\t---[ OPERASI MATRIX ]---\n
    Pada tantangan ini, kamu akan diberikan 3 buah matrix. Sebut saja matrix A, B dan D.
    Diketahui bahwa A dan B adalah matrix berordo NxN dan D adalah matrix berordo Nx1. N akan bernilai random.\n
    D = T(A+B) * C\n
    T adalah fungsi transpose matrix. T(A+B) memiliki arti bahwa pertambahan matrix A dan B kemudian hasilnya di transpose.
    Untuk mendapatkan flag, kamu harus menghitung matrix C untuk setiap soalnya. Total jumlah soal adalah 100
    Sebagai contoh matrix C yang didapat adalah [1,2,3,4] maka submit 1 2 3 4 untuk setiap nilai matrixnya dengan dipisahkan oleh spasi.
    Kamu diberikan waktu maksimal 8 detik untuk menyelesaikan per 1 soal\n
    Press [ANY] key to continue
    """)
    raw_input()

def main():
    welcome()
    num_choice = [3,4]
    for x in range(100):
        num = random.choice(num_choice)
        print("\t---[ Level {} ]---\n".format(x+1))
        A,B,C,D = generate_soal(num)
        line = '-' * (15)
        print("\t   MATRIX A\n\t{}\n{}".format(line,matrix_non_single_to_str(A)))
        print("\t   MATRIX B\n\t{}\n{}".format(line,matrix_non_single_to_str(B)))
        print("\t   MATRIX D\n\t{}\n{}".format(line,matrix_single_to_str(D)))
        time_start = int(time.time())
        try:
            input_peserta = raw_input('[?] MATRIX C = ').split(' ')
            time_end = int(time.time())
            if time_end - time_start > 8:
                print("[x] Waktu habis")
                exit()
            if len(input_peserta) == num:
                for x in range(num):
                    if input_peserta[x].isdigit() == True:
                        if int(input_peserta[x]) == int(C[x]):
                            continue
                        else:
                            print("[x] Jawaban Salah")
                            exit()
                    else:
                        print("[x] Jawaban Salah")
                        exit()

            else:
                print("[x] Jawaban Salah")
                exit()
        except:
            exit()
    print("[+] Selamat, ini flag buat kamu. Flag : HackFest{___kamu_nyari_matrix_C_nya_pake_Z3_apa_manual_:D___}\n")

if __name__ == "__main__":
    main()
