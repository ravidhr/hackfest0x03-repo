# eksor tidak jomblo

	c'mon... you know the key, so it should be easy

## Solusi
Ini hanya pengenalan ke multibyte xor jadi cukup jalankan saja programnya dengan key yang sudah ada

	r = open('cipher.txt', 'r')
	cip = r.read()
	r.close

	key = 'TheQuickBrownFox'

	flag = ''
	for i in range(len(cip)):
		flag += chr(ord(cip[i]) ^ ord(key[i % len(key)]))

	print flag

FLAG = HackFest{They_called_this_MultiByteXoR}
