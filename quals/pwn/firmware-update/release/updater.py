import zlib
from os import system
import random

def SanityChecks(data):
	magic_header = [69, 76, 70]
	if "ELF" in data:
		print("[+] Sanity Check : passed")
		return data
	else:
		print("[!] Sanity Check : not passed")
		exit(-1)

def UnpackingUpdate(data):
	unhex = data.decode('hex')
	unzlib = zlib.decompress(unhex)
	return unzlib

def WriteToSandbox(binary):
	randInt = [x for x in range(0xff)]
	name  = random.choice(randInt)
	path = "/tmp/tripoloskiOS" + str(name)
	f = open(path , 'wb')
	f.write(binary)
	f.close()
	system("chmod 777 " + path)
	return path


def WelcomeMsg():
	print("	     _.-;;-._")
	print("      '-..-'|   ||   |")
	print("      '-..-'|_.-;;-._|")
	print("      '-..-'|   ||   |")
	print("      '-..-'|_.-''-._|")
	print("     	 arsalan OS")
	print("")
	print("this is arsalan file system update mechanism")
	print("created by arsalan@2019 all the update will ")
	print("stored inside /tmp/<n> ")

	Update = raw_input("waiting for update.. ")
	unpack = UnpackingUpdate(Update)
	Sanity  = SanityChecks(unpack)
	print("UpData : " + Sanity)
	wtbox = WriteToSandbox(Sanity)
	print("applying update... ")
	system(wtbox)

def main():
	WelcomeMsg()



if __name__ == '__main__':
	main()