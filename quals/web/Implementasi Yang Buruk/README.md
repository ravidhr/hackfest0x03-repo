# Implementasi Yang Buruk

# Deskripsi

    tim ccug berhasil membuat sebuah system untuk kebutuhan mengamankan pesan dengan cara implementasi kami sendiri menggunakan bahasa pemograman, bisakah kamu menguji system kami?

# Solusi

Chall ini berisi client side attack karena function bisa diatur di sisi pengguna, tujuannya adalah peserta diminta untuk melakukan RCE dengan mengedit nilai di bagian select menggunakan function pemanggilan sistem seperti exec(), system(), shell_exec(), passthru() dll

flag : hackfest{mungkinkah_ada_yang_seperti_ini_didunia_nyata?_mana_saya_tau_saya_kan_anak_tiktok}