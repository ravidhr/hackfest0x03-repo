from Crypto import Random
key = '\xd0\xe3\x7f\xb3\xa8=\xf9\xcdR\x16"W\x1b4\x0b<'
def encrypt(data):
    res = ""
    for x in range(len(data)):
        tmp = hex(ord(data[x]) ^ ord(key[x % len(key)]))[2:]
        if len(str(tmp)) == 2:
            res += "\\x" + str(tmp)
        else:
            res += "\\x0" + str(tmp)
    return res

def main():
    print(encrypt('[-] Serial number salah\n'))

if __name__ == "__main__":
    main()
