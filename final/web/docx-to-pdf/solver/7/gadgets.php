<?php

namespace Illuminate\Broadcasting
{
    class PendingBroadcast
    {
        protected $pending_events;
        protected $pending_event;

        function __construct($events, $cmd)
        {
            $this->pending_events = $events;
            $this->pending_event = $cmd;
        }
    }
}


namespace Faker
{
    class Generator
    {
        protected $formatters;

        function __construct($function)
        {
            $this->formatters = ['dispatch' => $function];
        }
    }
}
