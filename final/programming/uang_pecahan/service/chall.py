import random, sys
import time

class Unbuffered(object):
    def __init__(self, stream):
        self.stream = stream
    def write(self, data):
        self.stream.write(data)
        self.stream.flush()
    def writelines(self, datas):
        self.stream.writelines(datas)
        self.stream.flush()
    def __getattr__(self, attr):
        return getattr(self.stream, attr)

sys.stdout = Unbuffered(sys.stdout)

def welcome():
    print("""
    \t\t---[ PECAHAN DAN PENYEBUTAN UANG RUPIAH ]---\n
    Di Republik Indonesia ini terdapat beberapa lembar uang yaitu
    100000, 75000, 50000, 20000, 10000, 5000, 2000, 1000
    Saat ini Anda dimintai tolong oleh CEO CCUG Mart untuk membantu kasir CCUG mart dalam melaksanakan tugasnya\n
    Contoh Soal:
    Temukan A dan B dari 76000
    Contoh Jawaban:
    A = 0 1 0 0 0 0 0 1
    B = Tujuh Puluh Enam Ribu Rupiah

    Keterangan :
    -> A adalah variabel yang menampung pecahan uang. Sebagai contoh, uang Rp 76.000 diperlukan uang 1 lembar Rp 75.000 dan 1 lembar uang Rp 1.000
    Setiap jumlah lembar dipisah menggunakan spasi.
    -> B adalah variabel yang menampung penyebutan uang. Sebagai contoh, uang Rp 76.000 dibaca dalam Bahasa Indonesia menjadi Tujuh Puluh Enam Ribu Rupiah.\n
    Untuk mendapatkan flag, kamu harus menemukan A dan B sebanyak 100 kali dalam waktu kurang dari 5 detik untuk setiap levelnya.

    Press [ANY] key to continue
    """)
    raw_input()

def fungsi_sebut_uang(uang):
    res = []
    kata = ['satu','dua','tiga','empat','lima','enam','tujuh','delapan','sembilan']
    jut = 0
    if uang >= 1000000:
        jut = uang / 1000000
        res.append(kata[jut-1])
        res.append('juta')
        uang = uang % 1000000
    ratus = 0
    if uang >= 100000:
        ratus = uang / 100000
        if ratus == 1:
            res.append('seratus')
        else:
            res.append(kata[ratus-1])
            res.append('ratus')
        uang = uang % 100000
    puluh = 0
    if uang >= 10000:
        puluh = uang / 10000
        if puluh == 1:
            res.append('sepuluh')
        else:
            res.append(kata[puluh-1])
            res.append('puluh')
        uang = uang % 10000
    ribu = 0
    if uang >= 1000:
        ribu = uang / 1000
        if puluh == 1 and ribu == 1:
            res.pop()
            res.append('sebelas')
        elif puluh == 1 and ribu > 1:
            res.pop()
            res.append(kata[ribu-1])
            res.append('belas')
        elif ribu == 1 and puluh > 1:
            res.append(kata[ribu-1])
        elif ribu == 1 and puluh == 0:
            res.append('seribu')
        else:
            res.append(kata[ribu-1])
        uang = uang % 1000
    if ratus > 0 or puluh > 0 or ribu > 0 and ribu != 1:
        res.append('ribu')
    ratus_ = 0
    if uang >= 100:
        ratus_ = uang / 100
        if ratus_ == 1:
            res.append('seratus')
        else:
            res.append(kata[ratus_ - 1])
            res.append('ratus')
        uang = uang % 100
    if jut != 0 or ratus != 0 or puluh != 0 or ribu != 0 or ratus_ != 0:
        res.append('rupiah')
        res = ' '.join(res).title()
        return res
    else:
        return None

def fungsi_pecah_uang(uang):
    uang_copy = uang
    seratus_ribu = 0
    tujuh_lima_ribu = 0
    lima_puluh_ribu = 0
    dua_puluh_ribu = 0
    sepuluh_ribu = 0
    lima_ribu = 0
    dua_ribu = 0
    seribu = 0
    if uang_copy >= 100000:
        seratus_ribu = uang_copy/100000
        uang_copy = uang_copy % 100000
    if uang_copy >= 75000:
        tujuh_lima_ribu = uang_copy/75000
        uang_copy = uang_copy % 75000
    if uang_copy >= 50000:
        lima_puluh_ribu = uang_copy/50000
        uang_copy = uang_copy % 50000
    if uang_copy >= 20000:
        dua_puluh_ribu = uang_copy/20000
        uang_copy = uang_copy % 20000
    if uang_copy >= 10000:
        sepuluh_ribu = uang_copy/10000
        uang_copy = uang_copy%10000
    if uang_copy >= 5000:
        lima_ribu = uang_copy/5000
        uang_copy = uang_copy%5000
    if uang_copy >= 2000:
        dua_ribu = uang_copy/2000
        uang_copy = uang_copy%2000
    if uang_copy >= 1000:
        seribu = uang_copy/1000
        uang_copy = uang_copy % 1000
    pecahan = [seratus_ribu,tujuh_lima_ribu,lima_puluh_ribu,dua_puluh_ribu,sepuluh_ribu,lima_ribu,dua_ribu,seribu]
    if uang_copy == 0:
        return pecahan
    else:
        return False

def main():
    welcome()
    for x in range(100):
        time_start = int(time.time())
        print('\t---[ Level {} ]---'.format(x+1))
        uang = random.randrange(5000,9999000)
        uang =  uang / 1000
        uang = uang * 1000
        print('Temukan A dan B dari {}'.format(uang))
        pecah_uang = fungsi_pecah_uang(uang)
        sebut_uang = fungsi_sebut_uang(uang)
        input_peserta = raw_input('[?] A = ')
        sebut_peserta = raw_input('[?] B = ')
        time_end = int(time.time())
        if time_end - time_start > 5:
            print('[x] Waktu habis')
            exit()
        if sebut_peserta != sebut_uang:
            print('[x] Jawaban salah. Silahkan coba lagi\n')
            exit()
        try:
            pecahan_peserta = input_peserta.split(' ')
            if len(pecahan_peserta) == 8:
                for num in pecahan_peserta:
                    if num.isdigit() == False:
                        print('[x] Format input tidak sesuai\n')
                        exit()
                for y in range(len(pecahan_peserta)):
                    if int(pecahan_peserta[y]) != int(pecah_uang[y]):
                        print('[x] Jawaban salah. Silahkan coba lagi\n')
                        exit()
                print('[+] Jawaban benar\n')
            else:
                print('[x] Format input tidak sesuai\n')
                exit()
        except:
            exit()
    print('Selamat, ini hadiah untuk Anda')
    print('[+] Flag : HackFest{kenapa_kok_uang_75_ribu_baru_angka_0_nya_kecil_dac93fcdb9}')

if __name__ == "__main__":
    main()
