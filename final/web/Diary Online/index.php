<?php

    require 'controller/db.php';
    $id = $_SESSION['id'];
    if(empty($id) && !array_key_exists('id', $_SESSION)) {
        header('Location: login.html');
        exit;
    }

    $nama = $conn->query("SELECT nama FROM tbl_users WHERE id = {$id}");
    $diary = $conn->query("SELECT alasan, diary FROM tbl_diary WHERE id_peserta = {$id}");

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Dashboard Diary</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body>

	<div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100 p-t-50 p-b-90" style="text-align:center;">
                <p><a href="logout.php" style="width:100%;" class="login100-form-btn">Logout</a></p>
					<span class="login100-form-title p-b-51">Wellcome <?= $nama->fetch_assoc()['nama']; ?></span>
                    
                    <?php if($diary->num_rows > 0): ?>
                        <p style="font-size: 16px;text-align:center;"><b>Your Diary</b></p>
                        <?php while($isi = $diary->fetch_assoc()): ?>
                        <div class="wrap-input100 validate-input m-b-16">
                            <h6><?= $isi['alasan']; ?></h6>
                            <hr>
                            <p><?= $isi['diary']; ?></p>
					    </div>
                        <?php endwhile; ?>
                    <?php else: ?>
                        <p style="font-size: 16px;text-align:center;"><b>Your diary  doesn't exist yet, Please fill the input below!</b></p>
                    <?php endif; ?>
                    <hr>
                    <form action="controller/tambah.php" method="post">
                        <label for="alasan">Title</label>
                        <div class="wrap-input100 validate-input m-b-16" data-validate="Alasan is required">
						    <input style="padding: 10px;font-size:12;width:100%;background:transparent;color:#403866;" type="text" name="alasan" placeholder="Galau">
                        </div>
                        <label for="alasan">Diary</label>
                        <textarea name="diary" placeholder="Saya galau banget hari ini pengen flag:(" class="wrap-input100 m-b-16" cols="30" rows="10"></textarea>
                        <div class="container-login100-form-btn m-t-17">
                            <button name="submit" class="login100-form-btn">
                                Submit
                            </button>
                        </div>
                    </form>
			</div>
		</div>
	</div>
<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

</body>
</html>