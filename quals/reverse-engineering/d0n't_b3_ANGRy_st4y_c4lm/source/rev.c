// gcc rev.c -o rev -fno-stack-protector -no-pie -m64

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int func_0(char *a1){
    return (*(a1+0) < *(a1+123) && (*(a1+0) - 7) + 0x27 == 0x65);
}
int func_1(char *a1){
    return ((*(a1+1) + 0x2d) - 15 == 0x8c && *(a1+1) > *(a1+92));
}
int func_2(char *a1){
    return (*(a1+2) < *(a1+76) && (*(a1+2) - 5) + 0x19 == 0x88);
}
int func_3(char *a1){
    return ((*(a1+143) * 0x1f) % 17 == 0xf && *(a1+3) == *(a1+143));
}
int func_4(char *a1){
    return ((*(a1+4) + 0x2e) - 18 == 0x84 && *(a1+4) > *(a1+15));
}
int func_5(char *a1){
    return (*(a1+5) < *(a1+45) && (*(a1+5) - 13) + 0x19 == 0x2c);
}
int func_6(char *a1){
    return ((*(a1+6) + 0x26) - 17 == 0x77 && *(a1+6) > *(a1+0));
}
int func_7(char *a1){
    return (*(a1+7) < *(a1+40) && (*(a1+7) - 9) + 0x24 == 0x7c);
}
int func_8(char *a1){
    return ((*(a1+8) + 0x1d) - 23 == 0x6d && *(a1+8) > *(a1+36));
}
int func_9(char *a1){
    return (*(a1+9) < *(a1+102) && (*(a1+9) - 5) + 0x19 == 0x75);
}
int func_10(char *a1){
    return ((*(a1+10) + 0x33) - 23 == 0x85 && *(a1+10) > *(a1+117));
}
int func_11(char *a1){
    return ((*(a1+11) + 0x1e) - 22 == 0x75 && *(a1+11) > *(a1+57));
}
int func_12(char *a1){
    return ((*(a1+49) * 0x1f) % 20 == 0x7 && *(a1+12) == *(a1+49));
}
int func_13(char *a1){
    return ((*(a1+13) + 0x33) - 25 == 0x88 && *(a1+13) > *(a1+6));
}
int func_14(char *a1){
    return ((*(a1+14) + 0x34) - 18 == 0x83 && *(a1+14) > *(a1+109));
}
int func_15(char *a1){
    return (*(a1+15) < *(a1+161) && (*(a1+15) - 15) + 0x2e == 0x3f);
}
int func_16(char *a1){
    return ((*(a1+16) + 0x30) - 15 == 0x82 && *(a1+16) > *(a1+61));
}
int func_17(char *a1){
    return (*(a1+17) < *(a1+139) && (*(a1+17) - 12) + 0x2b == 0x8d);
}
int func_18(char *a1){
    return ((*(a1+18) + 0x2e) - 17 == 0x81 && *(a1+18) > *(a1+120));
}
int func_19(char *a1){
    return ((*(a1+19) + 0x29) - 19 == 0x77 && *(a1+19) > *(a1+37));
}
int func_20(char *a1){
    return (*(a1+20) < *(a1+114) && (*(a1+20) - 15) + 0x25 == 0x36);
}
int func_21(char *a1){
    return ((*(a1+21) + 0x30) - 25 == 0x84 && *(a1+21) > *(a1+55));
}
int func_22(char *a1){
    return (*(a1+22) < *(a1+121) && (*(a1+22) - 13) + 0x24 == 0x7c);
}
int func_23(char *a1){
    return ((*(a1+134) * 0x2c) % 20 == 0x0 && *(a1+23) == *(a1+134));
}
int func_24(char *a1){
    return ((*(a1+24) + 0x19) - 20 == 0x7e && *(a1+24) > *(a1+162));
}
int func_25(char *a1){
    return ((*(a1+25) + 0x22) - 20 == 0x73 && *(a1+25) > *(a1+92));
}
int func_26(char *a1){
    return ((*(a1+26) + 0x26) - 19 == 0x7f && *(a1+26) > *(a1+25));
}
int func_27(char *a1){
    return (*(a1+27) < *(a1+102) && (*(a1+27) - 13) + 0x21 == 0x79);
}
int func_28(char *a1){
    return ((*(a1+28) + 0x1a) - 16 == 0x7d && *(a1+28) > *(a1+53));
}
int func_29(char *a1){
    return ((*(a1+113) * 0x1d) % 20 == 0xd && *(a1+29) == *(a1+113));
}
int func_30(char *a1){
    return ((*(a1+30) + 0x19) - 21 == 0x6d && *(a1+30) > *(a1+138));
}
int func_31(char *a1){
    return ((*(a1+31) + 0x2d) - 17 == 0x87 && *(a1+31) > *(a1+61));
}
int func_32(char *a1){
    return (*(a1+32) < *(a1+17) && (*(a1+32) - 12) + 0x22 == 0x77);
}
int func_33(char *a1){
    return ((*(a1+33) + 0x36) - 21 == 0x8f && *(a1+33) > *(a1+47));
}
int func_34(char *a1){
    return ((*(a1+34) + 0x2f) - 21 == 0x88 && *(a1+34) > *(a1+97));
}
int func_35(char *a1){
    return ((*(a1+35) + 0x25) - 22 == 0x88 && *(a1+35) > *(a1+100));
}
int func_36(char *a1){
    return (*(a1+36) < *(a1+111) && (*(a1+36) - 10) + 0x34 == 0x8b);
}
int func_37(char *a1){
    return (*(a1+37) < *(a1+74) && (*(a1+37) - 14) + 0x2b == 0x3d);
}
int func_38(char *a1){
    return ((*(a1+38) + 0x34) - 18 == 0x96 && *(a1+38) > *(a1+157));
}
int func_39(char *a1){
    return (*(a1+39) < *(a1+35) && (*(a1+39) - 7) + 0x2b == 0x85);
}
int func_40(char *a1){
    return (*(a1+40) < *(a1+67) && (*(a1+40) - 15) + 0x23 == 0x84);
}
int func_41(char *a1){
    return ((*(a1+41) + 0x22) - 24 == 0x73 && *(a1+41) > *(a1+6));
}
int func_42(char *a1){
    return (*(a1+42) < *(a1+103) && (*(a1+42) - 15) + 0x35 == 0x46);
}
int func_43(char *a1){
    return ((*(a1+43) + 0x24) - 24 == 0x75 && *(a1+43) > *(a1+27));
}
int func_44(char *a1){
    return ((*(a1+44) + 0x33) - 24 == 0x89 && *(a1+44) > *(a1+27));
}
int func_45(char *a1){
    return ((*(a1+45) + 0x32) - 15 == 0x8c && *(a1+45) > *(a1+3));
}
int func_46(char *a1){
    return (*(a1+46) < *(a1+8) && (*(a1+46) - 5) + 0x36 == 0x51);
}
int func_47(char *a1){
    return ((*(a1+47) + 0x19) - 15 == 0x6b && *(a1+47) > *(a1+37));
}
int func_48(char *a1){
    return (*(a1+48) < *(a1+10) && (*(a1+48) - 13) + 0x32 == 0x89);
}
int func_49(char *a1){
    return ((*(a1+49) + 0x31) - 16 == 0x82 && *(a1+49) > *(a1+155));
}
int func_50(char *a1){
    return ((*(a1+50) + 0x32) - 25 == 0x85 && *(a1+50) > *(a1+57));
}
int func_51(char *a1){
    return (*(a1+51) < *(a1+77) && (*(a1+51) - 10) + 0x36 == 0x8d);
}
int func_52(char *a1){
    return ((*(a1+52) + 0x1b) - 17 == 0x72 && *(a1+52) > *(a1+87));
}
int func_53(char *a1){
    return (*(a1+53) < *(a1+84) && (*(a1+53) - 11) + 0x25 == 0x3a);
}
int func_54(char *a1){
    return (*(a1+54) < *(a1+133) && (*(a1+54) - 7) + 0x35 == 0x74);
}
int func_55(char *a1){
    return (*(a1+55) < *(a1+149) && (*(a1+55) - 12) + 0x29 == 0x69);
}
int func_56(char *a1){
    return (*(a1+56) < *(a1+154) && (*(a1+56) - 15) + 0x37 == 0x5c);
}
int func_57(char *a1){
    return (*(a1+57) < *(a1+139) && (*(a1+57) - 7) + 0x1f == 0x5f);
}
int func_58(char *a1){
    return (*(a1+58) < *(a1+27) && (*(a1+58) - 15) + 0x26 == 0x51);
}
int func_59(char *a1){
    return (*(a1+59) < *(a1+104) && (*(a1+59) - 11) + 0x2a == 0x3f);
}
int func_60(char *a1){
    return ((*(a1+60) + 0x25) - 25 == 0x33 && *(a1+60) > *(a1+131));
}
int func_61(char *a1){
    return (*(a1+61) < *(a1+107) && (*(a1+61) - 14) + 0x33 == 0x79);
}
int func_62(char *a1){
    return (*(a1+62) < *(a1+52) && (*(a1+62) - 9) + 0x28 == 0x68);
}
int func_63(char *a1){
    return (*(a1+63) < *(a1+113) && (*(a1+63) - 12) + 0x2c == 0x66);
}
int func_64(char *a1){
    return (*(a1+64) < *(a1+132) && (*(a1+64) - 8) + 0x30 == 0x6d);
}
int func_65(char *a1){
    return (*(a1+65) < *(a1+139) && (*(a1+65) - 8) + 0x23 == 0x6e);
}
int func_66(char *a1){
    return (*(a1+66) < *(a1+10) && (*(a1+66) - 10) + 0x1e == 0x68);
}
int func_67(char *a1){
    return ((*(a1+67) + 0x26) - 22 == 0x8b && *(a1+67) > *(a1+101));
}
int func_68(char *a1){
    return (*(a1+68) < *(a1+149) && (*(a1+68) - 8) + 0x1c == 0x48);
}
int func_69(char *a1){
    return ((*(a1+69) + 0x27) - 17 == 0x84 && *(a1+69) > *(a1+143));
}
int func_70(char *a1){
    return (*(a1+70) < *(a1+76) && (*(a1+70) - 6) + 0x30 == 0x91);
}
int func_71(char *a1){
    return ((*(a1+71) + 0x1f) - 22 == 0x7b && *(a1+71) > *(a1+129));
}
int func_72(char *a1){
    return (*(a1+72) < *(a1+29) && (*(a1+72) - 7) + 0x1c == 0x74);
}
int func_73(char *a1){
    return ((*(a1+73) + 0x32) - 23 == 0x8e && *(a1+73) > *(a1+144));
}
int func_74(char *a1){
    return (*(a1+74) < *(a1+117) && (*(a1+74) - 8) + 0x33 == 0x5c);
}
int func_75(char *a1){
    return (*(a1+75) < *(a1+28) && (*(a1+75) - 8) + 0x22 == 0x87);
}
int func_76(char *a1){
    return ((*(a1+76) + 0x21) - 19 == 0x83 && *(a1+76) > *(a1+48));
}
int func_77(char *a1){
    return ((*(a1+77) + 0x31) - 19 == 0x8a && *(a1+77) > *(a1+78));
}
int func_78(char *a1){
    return ((*(a1+78) + 0x25) - 25 == 0x40 && *(a1+78) > *(a1+60));
}
int func_79(char *a1){
    return (*(a1+79) < *(a1+22) && (*(a1+79) - 5) + 0x2a == 0x5c);
}
int func_80(char *a1){
    return (*(a1+80) < *(a1+6) && (*(a1+80) - 14) + 0x21 == 0x44);
}
int func_81(char *a1){
    return (*(a1+81) < *(a1+88) && (*(a1+81) - 15) + 0x1f == 0x40);
}
int func_82(char *a1){
    return (*(a1+82) < *(a1+38) && (*(a1+82) - 10) + 0x1c == 0x80);
}
int func_83(char *a1){
    return ((*(a1+83) + 0x1d) - 16 == 0x6c && *(a1+83) > *(a1+84));
}
int func_84(char *a1){
    return (*(a1+84) < *(a1+129) && (*(a1+84) - 7) + 0x25 == 0x6b);
}
int func_85(char *a1){
    return (*(a1+85) < *(a1+129) && (*(a1+85) - 14) + 0x31 == 0x57);
}
int func_86(char *a1){
    return ((*(a1+86) + 0x30) - 25 == 0x85 && *(a1+86) > *(a1+143));
}
int func_87(char *a1){
    return (*(a1+87) < *(a1+118) && (*(a1+87) - 7) + 0x2b == 0x58);
}
int func_88(char *a1){
    return ((*(a1+88) + 0x33) - 25 == 0x81 && *(a1+88) > *(a1+125));
}
int func_89(char *a1){
    return (*(a1+89) < *(a1+73) && (*(a1+89) - 7) + 0x2c == 0x58);
}
int func_90(char *a1){
    return ((*(a1+90) + 0x21) - 17 == 0x82 && *(a1+90) > *(a1+160));
}
int func_91(char *a1){
    return ((*(a1+91) + 0x36) - 17 == 0x98 && *(a1+91) > *(a1+157));
}
int func_92(char *a1){
    return ((*(a1+92) + 0x2b) - 25 == 0x71 && *(a1+92) > *(a1+42));
}
int func_93(char *a1){
    return ((*(a1+93) + 0x2c) - 18 == 0x8d && *(a1+93) > *(a1+20));
}
int func_94(char *a1){
    return ((*(a1+94) + 0x20) - 15 == 0x45 && *(a1+94) > *(a1+5));
}
int func_95(char *a1){
    return (*(a1+95) < *(a1+139) && (*(a1+95) - 11) + 0x2f == 0x92);
}
int func_96(char *a1){
    return ((*(a1+96) + 0x1c) - 23 == 0x6c && *(a1+96) > *(a1+94));
}
int func_97(char *a1){
    return (*(a1+97) < *(a1+133) && (*(a1+97) - 14) + 0x2b == 0x51);
}
int func_98(char *a1){
    return ((*(a1+98) + 0x2e) - 15 == 0x93 && *(a1+98) > *(a1+106));
}
int func_99(char *a1){
    return (*(a1+99) < *(a1+149) && (*(a1+99) - 9) + 0x32 == 0x88);
}
int func_100(char *a1){
    return ((*(a1+100) + 0x31) - 24 == 0x7b && *(a1+100) > *(a1+109));
}
int func_101(char *a1){
    return (*(a1+101) < *(a1+24) && (*(a1+101) - 8) + 0x27 == 0x52);
}
int func_102(char *a1){
    return ((*(a1+102) + 0x24) - 22 == 0x80 && *(a1+102) > *(a1+44));
}
int func_103(char *a1){
    return (*(a1+103) < *(a1+151) && (*(a1+103) - 6) + 0x26 == 0x87);
}
int func_104(char *a1){
    return ((*(a1+104) + 0x32) - 16 == 0x97 && *(a1+104) > *(a1+69));
}
int func_105(char *a1){
    return ((*(a1+105) + 0x1f) - 24 == 0x75 && *(a1+105) > *(a1+106));
}
int func_106(char *a1){
    return (*(a1+106) < *(a1+41) && (*(a1+106) - 5) + 0x1c == 0x4b);
}
int func_107(char *a1){
    return ((*(a1+107) + 0x22) - 21 == 0x8a && *(a1+107) > *(a1+84));
}
int func_108(char *a1){
    return (*(a1+108) < *(a1+17) && (*(a1+108) - 12) + 0x2f == 0x4a);
}
int func_109(char *a1){
    return (*(a1+109) < *(a1+141) && (*(a1+109) - 7) + 0x33 == 0x5a);
}
int func_110(char *a1){
    return (*(a1+110) < *(a1+119) && (*(a1+110) - 7) + 0x1c == 0x35);
}
int func_111(char *a1){
    return ((*(a1+111) + 0x2a) - 19 == 0x79 && *(a1+111) > *(a1+19));
}
int func_112(char *a1){
    return ((*(a1+112) + 0x2c) - 16 == 0x91 && *(a1+112) > *(a1+105));
}
int func_113(char *a1){
    return ((*(a1+113) + 0x2b) - 22 == 0x76 && *(a1+113) > *(a1+63));
}
int func_114(char *a1){
    return ((*(a1+114) + 0x2f) - 16 == 0x93 && *(a1+114) > *(a1+10));
}
int func_115(char *a1){
    return (*(a1+115) < *(a1+140) && (*(a1+115) - 14) + 0x36 == 0x48);
}
int func_116(char *a1){
    return ((*(a1+116) + 0x1e) - 21 == 0x74 && *(a1+116) > *(a1+101));
}
int func_117(char *a1){
    return (*(a1+117) < *(a1+26) && (*(a1+117) - 7) + 0x22 == 0x7c);
}
int func_118(char *a1){
    return ((*(a1+118) + 0x23) - 21 == 0x7b && *(a1+118) > *(a1+30));
}
int func_119(char *a1){
    return (*(a1+119) < *(a1+35) && (*(a1+119) - 5) + 0x28 == 0x98);
}
int func_120(char *a1){
    return (*(a1+120) < *(a1+83) && (*(a1+120) - 5) + 0x31 == 0x4c);
}
int func_121(char *a1){
    return ((*(a1+121) + 0x34) - 17 == 0x9c && *(a1+121) > *(a1+108));
}
int func_122(char *a1){
    return ((*(a1+47) * 0x2b) % 20 == 0xb && *(a1+122) == *(a1+47));
}
int func_123(char *a1){
    return ((*(a1+123) + 0x30) - 16 == 0x8e && *(a1+123) > *(a1+140));
}
int func_124(char *a1){
    return (*(a1+124) < *(a1+44) && (*(a1+124) - 11) + 0x33 == 0x8f);
}
int func_125(char *a1){
    return (*(a1+125) < *(a1+61) && (*(a1+125) - 14) + 0x1b == 0x2d);
}
int func_126(char *a1){
    return ((*(a1+126) + 0x23) - 23 == 0x7f && *(a1+126) > *(a1+53));
}
int func_127(char *a1){
    return ((*(a1+127) + 0x35) - 16 == 0x9a && *(a1+127) > *(a1+15));
}
int func_128(char *a1){
    return (*(a1+128) < *(a1+31) && (*(a1+128) - 8) + 0x32 == 0x8e);
}
int func_129(char *a1){
    return (*(a1+129) < *(a1+116) && (*(a1+129) - 13) + 0x26 == 0x7a);
}
int func_130(char *a1){
    return ((*(a1+130) + 0x31) - 19 == 0x86 && *(a1+130) > *(a1+125));
}
int func_131(char *a1){
    return (*(a1+131) < *(a1+45) && (*(a1+131) - 10) + 0x1d == 0x33);
}
int func_132(char *a1){
    return ((*(a1+132) + 0x2a) - 24 == 0x7f && *(a1+132) > *(a1+54));
}
int func_133(char *a1){
    return ((*(a1+133) + 0x1e) - 25 == 0x6a && *(a1+133) > *(a1+20));
}
int func_134(char *a1){
    return ((*(a1+23) * 0x1f) % 18 == 0x8 && *(a1+134) == *(a1+23));
}
int func_135(char *a1){
    return ((*(a1+135) + 0x32) - 21 == 0x96 && *(a1+135) > *(a1+144));
}
int func_136(char *a1){
    return (*(a1+136) < *(a1+71) && (*(a1+136) - 14) + 0x37 == 0x8e);
}
int func_137(char *a1){
    return (*(a1+137) < *(a1+90) && (*(a1+137) - 12) + 0x25 == 0x85);
}
int func_138(char *a1){
    return ((*(a1+138) + 0x24) - 21 == 0x74 && *(a1+138) > *(a1+42));
}
int func_139(char *a1){
    return ((*(a1+139) + 0x19) - 19 == 0x79 && *(a1+139) > *(a1+20));
}
int func_140(char *a1){
    return (*(a1+140) < *(a1+38) && (*(a1+140) - 13) + 0x35 == 0x89);
}
int func_141(char *a1){
    return ((*(a1+141) + 0x1b) - 19 == 0x71 && *(a1+141) > *(a1+81));
}
int func_142(char *a1){
    return ((*(a1+142) + 0x2a) - 21 == 0x80 && *(a1+142) > *(a1+81));
}
int func_143(char *a1){
    return ((*(a1+143) + 0x22) - 25 == 0x6a && *(a1+143) > *(a1+161));
}
int func_144(char *a1){
    return ((*(a1+69) * 0x31) % 20 == 0xa && *(a1+144) == *(a1+69));
}
int func_145(char *a1){
    return ((*(a1+131) * 0x19) % 17 == 0x1 && *(a1+145) == *(a1+131));
}
int func_146(char *a1){
    return ((*(a1+146) + 0x29) - 21 == 0x88 && *(a1+146) > *(a1+14));
}
int func_147(char *a1){
    return (*(a1+147) < *(a1+102) && (*(a1+147) - 5) + 0x2a == 0x86);
}
int func_148(char *a1){
    return ((*(a1+148) + 0x19) - 23 == 0x70 && *(a1+148) > *(a1+162));
}
int func_149(char *a1){
    return ((*(a1+149) + 0x2d) - 15 == 0x92 && *(a1+149) > *(a1+50));
}
int func_150(char *a1){
    return (*(a1+150) < *(a1+8) && (*(a1+150) - 10) + 0x2e == 0x85);
}
int func_151(char *a1){
    return ((*(a1+151) + 0x1f) - 25 == 0x74 && *(a1+151) > *(a1+113));
}
int func_152(char *a1){
    return (*(a1+152) < *(a1+151) && (*(a1+152) - 6) + 0x31 == 0x92);
}
int func_153(char *a1){
    return (*(a1+153) < *(a1+50) && (*(a1+153) - 12) + 0x25 == 0x7a);
}
int func_154(char *a1){
    return ((*(a1+154) + 0x2d) - 17 == 0x8a && *(a1+154) > *(a1+70));
}
int func_155(char *a1){
    return (*(a1+155) < *(a1+23) && (*(a1+155) - 11) + 0x34 == 0x49);
}
int func_156(char *a1){
    return (*(a1+156) < *(a1+28) && (*(a1+156) - 14) + 0x2f == 0x8a);
}
int func_157(char *a1){
    return ((*(a1+105) * 0x19) % 16 == 0xe && *(a1+157) == *(a1+105));
}
int func_158(char *a1){
    return (*(a1+158) < *(a1+151) && (*(a1+158) - 9) + 0x19 == 0x79);
}
int func_159(char *a1){
    return (*(a1+159) < *(a1+35) && (*(a1+159) - 12) + 0x24 == 0x38);
}
int func_160(char *a1){
    return (*(a1+160) < *(a1+154) && (*(a1+160) - 5) + 0x36 == 0x6b);
}
int func_161(char *a1){
    return (*(a1+161) < *(a1+3) && (*(a1+161) - 7) + 0x26 == 0x48);
}
int func_162(char *a1){
    return (*(a1+162) < *(a1+128) && (*(a1+162) - 12) + 0x23 == 0x45);
}


int main(){
    char s[0xff];

    printf("[?] Password: ");
    fgets(s, 0xff, stdin);

    if (func_0(s) && func_1(s) && func_2(s) && func_3(s) && func_4(s) && func_5(s) && func_6(s) && func_7(s) && func_8(s) && func_9(s) && func_10(s) && func_11(s) && func_12(s) && func_13(s) && func_14(s) && func_15(s) && func_16(s) && func_17(s) && func_18(s) && func_19(s) && func_20(s) && func_21(s) && func_22(s) && func_23(s) && func_24(s) && func_25(s) && func_26(s) && func_27(s) && func_28(s) && func_29(s) && func_30(s) && func_31(s) && func_32(s) && func_33(s) && func_34(s) && func_35(s) && func_36(s) && func_37(s) && func_38(s) && func_39(s) && func_40(s) && func_41(s) && func_42(s) && func_43(s) && func_44(s) && func_45(s) && func_46(s) && func_47(s) && func_48(s) && func_49(s) && func_50(s) && func_51(s) && func_52(s) && func_53(s) && func_54(s) && func_55(s) && func_56(s) && func_57(s) && func_58(s) && func_59(s) && func_60(s) && func_61(s) && func_62(s) && func_63(s) && func_64(s) && func_65(s) && func_66(s) && func_67(s) && func_68(s) && func_69(s) && func_70(s) && func_71(s) && func_72(s) && func_73(s) && func_74(s) && func_75(s) && func_76(s) && func_77(s) && func_78(s) && func_79(s) && func_80(s) && func_81(s) && func_82(s) && func_83(s) && func_84(s) && func_85(s) && func_86(s) && func_87(s) && func_88(s) && func_89(s) && func_90(s) && func_91(s) && func_92(s) && func_93(s) && func_94(s) && func_95(s) && func_96(s) && func_97(s) && func_98(s) && func_99(s) && func_100(s) && func_101(s) && func_102(s) && func_103(s) && func_104(s) && func_105(s) && func_106(s) && func_107(s) && func_108(s) && func_109(s) && func_110(s) && func_111(s) && func_112(s) && func_113(s) && func_114(s) && func_115(s) && func_116(s) && func_117(s) && func_118(s) && func_119(s) && func_120(s) && func_121(s) && func_122(s) && func_123(s) && func_124(s) && func_125(s) && func_126(s) && func_127(s) && func_128(s) && func_129(s) && func_130(s) && func_131(s) && func_132(s) && func_133(s) && func_134(s) && func_135(s) && func_136(s) && func_137(s) && func_138(s) && func_139(s) && func_140(s) && func_141(s) && func_142(s) && func_143(s) && func_144(s) && func_145(s) && func_146(s) && func_147(s) && func_148(s) && func_149(s) && func_150(s) && func_151(s) && func_152(s) && func_153(s) && func_154(s) && func_155(s) && func_156(s) && func_157(s) && func_158(s) && func_159(s) && func_160(s) && func_161(s) && func_162(s)){
        puts("Correct!...");
    } else {
        puts("Incorrect!...");
    }
    return 0;
}
