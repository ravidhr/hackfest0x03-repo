# Kage function no jutsu

    jurus seribu function !!!!

# Solusi

terdapat 1k function di dalam binary ini, dapat dilihat menggunakan tool RE seperti
IDA, Binary Ninja, Hopper, Cutter, Ghidra, bisa juga menggunakan perintah

    nm -C ./a.out

untuk menyelesaikan challenge ini, karna flag sudah ada pada binary ini, maka kita hanya perlu
mencari function yang menyimpan flag tersebut, kita dapat menggunakan gdb secara manual (kalau niat :v), bisa juga menggunakan automasi dengan python, berikut solvernya: 


    from pwn import *
    import subprocess
    elf = ELF("./a.out")
    for i, j in elf.symbols.iteritems():
        x = subprocess.Popen(['gdb','-q','./a.out','-ex','start','-ex','jump %s' % i, '-ex', 'continue',"-ex","quit"], stdout=PIPE).communicate()[0]
        print "iter -" + str(i)
        if "hackfest{" in x:
            print x
            break

FLAG: hackfest{yamerooooooooo:(}
