<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Heker Berkelas</title>
</head>
<body>

    <h2><a href="index.php">CCUG News</a></h2>
    <form action="" method="GET">
        <label for="search">Search</label>
        <input type="text" name="search" id="search">
        <button type="submit">Search</button>
    </form>
    <hr>

    <?php

        require_once 'conn.php';
        if(array_key_exists('search', $_GET)) {
            $search = $_GET['search'];
            $data = $conn->query("SELECT id, judul, tanggal, penulis FROM tbl_artikel WHERE judul LIKE '%$search%'") OR die($conn->error);
        } elseif(array_key_exists('id', $_GET)) {
            $id = $_GET['id'];
            $data = $conn->query("SELECT judul, artikel FROM tbl_artikel WHERE id = '$id'") OR die($conn->error);
        } else {
            $data = $conn->query('SELECT id, judul, tanggal, penulis FROM tbl_artikel');
        }

        while($baris = $data->fetch_assoc()) {
            if(array_key_exists('id', $_GET)) {
                echo '<h2>' . $baris['judul'] . '</h2>';
                echo '<p>' . $baris['artikel'] . '</p>';
            } else {
                echo '<h2><a href="index.php?id=' . $baris['id'] . '">' . $baris['judul'] . '</a></h2>';
                echo '<pre>Ditulis pada ' . $baris['tanggal'] . ' oleh ' . $baris['penulis'] . '</pre>';
            }
        }

    ?>

</body>
</html>