# strugle 2.0

	dude, this is the real strugle

## Solusi

Gunakan karakteristik dari base64 dan base32 sebagai filternya, jika semua dalam uppercase maka decode dengan base32 karena base32 tidak ada lowercase

	import base64

	r = open('chall.txt', 'r')
	c = r.read()
	r.close()

	while True:
		if c.isupper():
			try:
				c = base64.b32decode(c)
				print c
			except:
				print c
				exit()
		else:
			try:
				c = base64.b64decode(c)
				print c
			except:
				print c
				exit()

FLAG = HackFest{if_4ll_0f_th3_ch4r_iS_UPPERCASE_th3n_iT_is_base32}
