<?php

    if(array_key_exists('lapor', $_POST)) {
        $nama = addslashes($_POST['nama']);
        $email = addslashes($_POST['email']);
        $keluhan = addslashes($_POST['keluhan']);
        // Meriksa apakan payload yang dikirim berbentuk tag script atau tag biasa yang berisi event handler
        if(preg_match('/on*/i', $keluhan) || preg_match('/<\/?script>/i', $keluhan)) {
            $conn = new mysqli('localhost', '', '', 'xss_ctf_ccug');
            if(!($conn->query("INSERT INTO payload_list VALUES('', '{$keluhan}')") && $conn->query("INSERT INTO payload_backup VALUES('', '{$ip}', '{$keluhan}')"))) {
                echo '<script>alert("Keluhan Gagal Dikiriman");history.back();</script>';
                exit;
            }
        }
        echo '<script>alert("Keluhan Berhasil Dikiriman! Admin Akan Segera Membalas Keluhanmu");history.back();</script>';
    } else {
        header('Location: /');
    }

?>