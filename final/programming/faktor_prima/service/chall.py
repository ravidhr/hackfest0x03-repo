from Crypto.Util.number import *
import random
import time

class Unbuffered(object):
    def __init__(self, stream):
        self.stream = stream
    def write(self, data):
        self.stream.write(data)
        self.stream.flush()
    def writelines(self, datas):
        self.stream.writelines(datas)
        self.stream.flush()
    def __getattr__(self, attr):
        return getattr(self.stream, attr)

sys.stdout = Unbuffered(sys.stdout)
prime_list = []
for x in range(10000):
    if isPrime(x):
        prime_list.append(x)

def generate_soal():
    global prime_list
    loop = random.randrange(15,50)
    factor_list = []
    for x in range(loop):
        while True:
            fact = random.choice(prime_list)
            if fact not in factor_list:
                factor_list.append(fact)
                break
    res_mul = 1
    for num in factor_list:
        res_mul *= num
    return res_mul,factor_list

def welcome():
    contoh = generate_soal()
    contoh_soal = contoh[0]
    contoh_jawaban = ' '.join([str(x) for x in contoh[1]])
    print("""
    \t\t\t---[ FAKTOR-FAKTOR PRIMA DARI N ]---\n
    Pada tantangan ini, Anda akan diberikan sebuah bilangan. Sebut saja bilangan itu adalah N.
    N adalah hasil perkalian beberapa bilangan prima.
    Bilangan prima yang menjadi faktor dari N adalah 0 <= P <= 10000.
    Tugas Anda adalah mencari faktor dari N yang berupa beberapa nilai P yang dikalikan menghasilkan N.\n

    Berikut ini merupakan contoh soal dan contoh jawaban\n
    Contoh Soal
    {}
    Contoh Jawaban
    {}

    Keterangan : Jawaban merupakan kumpulan nilai P yang dipisahkan oleh spasi.
    Untuk mendapatkan flag, Anda harus menyelesaikan tantangan ini sebanyak 100 kali dan memasukan jawaban setiap levelnya dalam waktu kurang dari 5 detik.


    Press [ANY] key to continue
    """.format(contoh_soal,contoh_jawaban))
    raw_input()

def main():
    welcome()
    for x in range(100):
        time_start = int(time.time())
        print('\t\t---[ Soal ke - {} ]---\n'.format(x+1))
        soal = generate_soal()
        print('N = {}'.format(soal[0]))
        input_peserta = raw_input('P = ')
        time_end = int(time.time())
        if time_end - time_start > 5:
            print('[x] Waktu habis')
            exit()
        try:
            p_peserta = input_peserta.split(' ')
            m_peserta = 1
            for num in p_peserta:
                if num.isdigit() == False:
                    print('[x] Format input tidak sesuai')
                    exit()
                elif isPrime(int(num)) == False:
                    print('[x] Jawaban salah')
                    exit()
                elif isPrime(int(num)) == True:
                    m_peserta *= int(num)
            if m_peserta == soal[0]:
                print('[+] Jawaban benar')
            else:
                print('[x] Jawaban salah')
                exit()
        except:
            exit()
    print('Selamat, ini hadiah untuk Anda')
    print('[+] Flag : HackFest{ide_awal_soal_ini_karena_kepikiran_attack_pada_RSA_hehe_4730a4e3db}')

if __name__ == "__main__":
    main()
